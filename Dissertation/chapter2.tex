%Critical
%Own opinion
%reconcile discrepant results
%analysis and synthesis

\chapter{State of the Art}

This chapter reviews the state of the art in the area of Simulation Level of Detail and also looks at studies that applied machine learning to predicting battle outcomes, as this is  central to the approach taken for the LOD system proposed in this dissertation.\\
%The LOD System proposed in this dissertation will use machine learning to predict the outcome of NPC interactions.

The first section looks at various approaches towards Simulation LOD in the literature and in commercial games. The second section reviews machine learning use-cases similar to the proposed system. The chapter ends with a short summary.

\section{Simulation LOD}

%Brom
Simulation LOD is concerned with what occurs when the NPCs are out of frustum \cite{Brom2007}. The LOD system described by Brom et al. \cite{Brom2007} can support areas without an observer having a high LOD, if they are particularly important areas. It supports gradual degradation of the simulation, as opposed to an online - offline approach. It differs from other methods in that it attempts to simplify the space as well as the behaviours.\\ 

The space is represented in a hierarchical manner, with agents using a more coarse representation of the space for path finding if the simulation LOD is lower (See Figure \ref{hier}). There are four levels of detail and they are applied to the action selection and environment representation \cite{Wissner:2010:LDA:1948395.1948423}. Brom et al. also discuss the difficulty of retaining information about areas with lower simulation LOD such that they can be reconstructed back to full LOD, as hierarchical LOD can be likened to lossy compression. The Membrane metaphor is used to describe what is currently simulated: nothing below the membrane line is simulated in Figure \ref{hier}. \\

%Typically: places -> parts of rooms -> rooms -> houses -> suburbs -> villages\\
%Each layer is associated with one simulation detail.\\
%When far away, only perform basic behaviour. Closer, role is taken on with a domain specici behaviour. path finding outside fustrum simplified. can't perform role only partially. full, medium, nothing.\\
%Partial detail may result in different outcome, but believability is maintained, mostly.\\

\begin{figure}[]
  \centering
  \includegraphics[scale=.8]{Images/lodplaces.png}
  \caption{Hierarchical Representation of Places and Objects \cite{Brom2007}. The wavy line is called the \textit{LOD Membrane}. Nothing below the membrane is simulated.}
  \label{hier}
\end{figure}

\iffalse
\begin{figure}[t]
  \centering
  \includegraphics[scale=.5]{Images/slod.png}
  \caption{Graphics LOD and Simulation LOD}
  \label{detectfig}
\end{figure}
\fi

An example scenario of a miner having a drink in a pub is shown in Figure \ref{pub}. At each level of detail the square boxes show the agent's behaviour. If the player is far away from the pub then the miner will just be enjoying himself in the pub, with no further detail about what is happening. At \textit{full-2} the miner is sitting at a table. At \textit{full-3} the miner is drinking a beer. At the full detail the miner is sipping their beer. While this approach is efficient it requires a lot of work to specify the logic at different levels of detail. This dissertation aims to create a more automated approach.\\

\begin{figure}[]
  \centering
  \includegraphics[scale=.85]{Images/pub.png}
  \caption{The behaviour of a miner drinking in a pub at different levels of detail. \cite{Brom2007}}
  \label{pub}
\end{figure}

%Wissner et al.
The Simulation LOD system described by Wissner et al. \cite{Wissner:2010:LDA:1948395.1948423} is based on both distance and visibility. There are eight levels of detail in their system. The LOD system is applied to action execution, updating movement, collision avoidance and navigation. For example, characters with lower levels of detail can walk through each other. Animations are omitted and characters only lip sync their dialogue at high levels of detail. As the detail decreases path planning is simplified. Instead of using the shortest possible route, they use any route that will get them to their target. The agent update frequency is also adjusted based on their LOD level.\\

%Chenny
Chenney et al. \cite{chenney2001proxy} developed a traffic simulation in which the space of the environment is partitioned and a discrete event simulation is used to track when a car crosses to a new area (enters a new road or intersection). In this way, the intervening travel is not simulated. When entering a cell, a prediction is made on when it will leave the cell, based on the acceleration profile of the car and the traffic congestion. The authors define three requirements for the proxy simulation:

\begin{enumerate}
\item The proxy must provide a reasonable stream of view entry events.
\item The proxy must provide reasonable state for objects when they re-enter the view.
\item The proxy must be significantly cheaper to compute than the accurate simulation.
\end{enumerate}

In another paper by the same authors \cite{arikan2001efficient} they also apply LOD to path planning. If an agent is out of view, then it only performs global path-finding and not local path-finding i.e. obstacle avoidance. They do not just ignore local interactions however, as an agent moving through a crowd would be expected to reach their destination slower than if they were unimpeded. They sample the full simulation using an Expectation Maximization (EM) algorithm to obtain a probability mixture distribution which can predict how long the delay should be when out-of-view parties interact. The goal is that the player could infer what happened off-screen. To further optimise path planning for off-screen agents, the shortest paths between every pair of vertices are computed and stored in a table. \\

%Grouped Hierarchical Level of Detail
Osborne and Dickinson \cite{OS:2010} describe what they call Grouped Hierarchical Level of Detail. It takes a top down approach, where the group is a single entity, but groups can be composed of subgroups and the leaf nodes are individual agents. If a lower level of detail is required, the subgroups are not simulated. There are increasing levels of abstraction coinciding with decreasing levels of detail. How many nodes are expanded depends on the proximity of the player. Attributes can be maintained for nodes relative to their parents, resulting in a more persistent simulation, but requires memory for the nodes are maintained down to the leaf node. The approach is similar to Brom et al. except the abstraction is applied to agents instead of locations. The simulation is shown in Figure \ref{groups}. The connected points are fully simulated child nodes. Child nodes are collapsed to the parent as the groups move further from the centre of the simulation.\\

\begin{figure}[]
  \centering
  \includegraphics[scale=.6]{Images/osborne.png}
  \caption{Grouped Hierarchical LOD \cite{OS:2010}}
  \label{groups}
\end{figure}

%PPA
MacNamee and Cunningham \cite{mac2001proposal} introduce the idea of Proactive Persistent Agents (PPS). To introduce the concept they contrast it to the presiding attitude in game development by quoting a code comment in the Half Life SDK: \textit{"don't let monsters outside of the player's PVS} [Potentially Visible Set] \textit{act up, or most of the interesting things will happen before the player gets there"}\cite{halfl20:online}. MacNamee and Cunningham go on to say that there are a range of games where this is not the case, in particular RPG games, \textit{"in which a virtual world, populated by agents which act autonomously - irrespective of the player's location - would greatly add to the sense of immersion."}.\\

The system they developed had four major modules:

\begin{itemize}
\item Behaviour system (Reactive) 
\item Social system (Reactive)
\item Goal based planner (Deliberative)
\item Schedule
\end{itemize}

The Behaviour system is handled by a technique called Role-Passing \cite{CAROL:2002} \cite{MACNAMEE:2002}. Certain roles require less computational resources than others. Every agent is a base agent object, and then can be assigned roles in the simulation, which can be added, removed and layered to change the agent's behaviour. This modular approach allows the agent to be no more complex than it needs to be, and as such can speed up AI processing. For example, if the NPC is not in view of the player, roles involved in making the agent appear visually realistic can be removed. The Schedule system is a cheap way to place agents where they should be at a given time of day according to a schedule. Games such as Skyrim \cite{TheEl21:online} use a system like this to seem as though the agents are simulated off-screen. The Social system is handled by a module called $\mu$-Sic \cite{macnamee2003creating} which will be discussed later.\\

%LOD Trader
Sunshine-Hill \cite{sunshine2013managing} describes a system that eschews simple distance based LOD called the LOD Trader. He argues that there is no natural threshold for AI LOD like there is for graphics LOD, and he sets out to create a better heuristic for AI LOD. The idea centres around optimal LOD selection. The LOD Trader tries to maximise realism, so a metric for realism is needed. It uses a stock trader metaphor - the trader has a set amount of resources [CPU and RAM] to spend, and must choose the stocks [LODs] that maximise return [realism].\\

An event in which the player notices a problem with the simulation is called a Break in Realism (BIR). The LOD trader attempts to reduce the likelihood of a BIR occurring. The LOD Trader uses logarithmic probability instead of linear probability  as it makes operations such as adding and comparing different probabilities easier. The trader attempts to minimise the total probability of a BIR occurring. The Trader will not decrease detail until it becomes necessary for performance\\

The BIR categories are:

\begin{itemize}
\item \textbf{Unrealistic State} These events are immediately noticeable to the player, such as an agent walking in place or disappearing.
\item \textbf{Fundamental Discontinuity} The NPCs current state is incompatible with the player's memory of the agent. These events are not unrealistic when taken out of context, but are unrealistic because the player knows they can't be true, such as a character who is known to be dead suddenly appearing alive and well.
\item \textbf{Unrealistic long-term behaviour} If the NPC is observed for an extended period, unrealistic behaviour is discovered.
\end{itemize}

Instead of using fixed transition rules, the LOD Trader tries to figure out the entire space of detail levels and finds the LOD for each agent that minimises the probability of above BIRs. It respects performance constraints and handles clustering of agents around the player gracefully. It is one of the more sophisticated LOD selection methods in the literature, however it does not describe how the actual LOD levels are implemented, which is the main focus of this dissertation.\\

\subsection{Simulation/AI LOD in Commercial Games}

LOD systems have been applied to commercial games, although these approaches are usually not publicly documented. Developer interviews are the main (and often only) source of information on LOD systems used in commercial games..

%\subsubsection{Republic: The Revolution}
%The level-of-detail AI in Republic: The Revolution simulated thousands of agents. When popping up a detail level the agents would become groups with a distribution describing what kind of agents are in that group. This allowed the simulation to reconstruction the more detailed simulation from a probability distribution. [ref]

\subsubsection{Assassin's Creed Unity (2014)}
 
Assassin's Creed Unity \cite{Ubiso41:online} applied a LOD system \cite{GDCVa68:online} to simulate the large city crowds in Paris. The legacy constraints of the older Assassin's Creed games were a CPU limit of one hundred NPCs, and a limit of twenty civilians. There were no systemic crowds. For Assassin's Creed Unity they implemented three levels of detail: Autonomous, Puppet and Low Resolution. The distance threshold for each LOD can be seen in Figure \ref{ACULOD}. The puppet has the same appearance as autonomous but with all the components turned off. The low resolution agent uses a custom fast animation system with a much simpler animation rig. There can be 40 autonomous agents which are less than 12m from the player. There can be thousands of lower detail agents.\\

\begin{figure}[]
  \centering
  \includegraphics[scale=.5]{Images/ACUThreshold.jpg}
  \caption{Assassin's Creed Unity LOD threshold distances \cite{GDCVa68:online}}
  \label{ACULOD}
\end{figure}

The low resolution and puppet agents don't use the Havok physics engine for collisions. Instead they use a 2D partition map for queries. All agents have a 'shepherd' which is placed by level designers and can be scripted. The shepherds contain all the AI data and control the agents. One type of shepherd is the wandering crowd, which uses randomly generated closed paths. Object pooling is used for the meshes. Object Pooling is an optimisation pattern in which a set amount of objects are loaded into memory once and are then re-used throughout the lifetime of the program, as opposed to allocating and freeing them continuously \cite{Objec62:online}. When an agent needs their LOD changed, the best matching entity in the object pool is selected, and props such as hats are spawned.\\% The animation is then mapped on to the new model.\\

\subsubsection{S.T.A.L.K.E.R.: Shadow of Chernobyl (2007)}

%, and was a major inspiration for this dissertation, as its agents made the world very immersive.
%Intro
S.T.A.L.K.E.R. \cite{S.T.A62:online} is a first person shooter survival horror game developed by GSC Game World. The game is built in GSC Game World's proprietary game engine, X-ray. A key feature of S.T.A.L.K.E.R. is the A-Life (Artificial Life) system, which is to date one of the best examples of A-Life in a commercial game. In an A-Life system the NPCs continue to exist even outside the player's field of view. Generally in open world games NPCs will be de-spawned (deleted) when the player leaves the area, to save processor resources. The AI developers of S.T.A.L.K.E.R. decided to continuously simulate the NPCs, albeit at a lower level of detail.\\

%Number of Detail Levels & %Detail Switching
In S.T.A.L.K.E.R. the NPCs have two levels of detail: Online and Offline \cite{A-Lif80:online}. They have very different behaviour implementations. When in offline mode, the NPC will not play animations or sounds, will not manage their inventory actively and does not use detailed, smoothed paths. It builds coarse paths, on the global navigation graph. The online mode has the full level of detail.\\

S.T.A.L.K.E.R. has a few separately instanced areas. All NPCs in other areas are offline. Some NPCs in the player's current area are also offline, as the areas are quite large and contain a large population of NPCs. When the NPC crosses a predefined radius from the player, it switches to full detail. The radius is typically 150m but varies in different areas.\\

%Navigation
Each area has its own detailed navigation graph, which is used by online characters. Each area's detailed graph has a less detailed version, which is connected up with the less detailed graphs from all the other areas. This global graph is used by offline characters.  When switching areas, an online NPC uses the detailed graph to calculate a path to the nearest node in the global graph. The agent then uses the global graph to move to the next area. The area transition points for NPCs are further than they are for the player, somewhere "around the corner" so that they do not just vanish when transferring to the next area.\\

%It is also used by online characters for strategic manoeuvres, and for moving to a location in a different area.

%NPC Agenda & Offline Mode behaviour & Online Mode Behaviour
The goal that all the NPC characters shared is to uncover the mystery of the 'Zone', the world of the game. 
%The NPC has a list of dealers who provide them with missions to retrieve artefacts. 
In offline mode, the NPC tries to find a goal. If it can't find one it wanders aimlessly. When it has a goal, it moves to the goal location using the global graph. While on the mission, the NPC could encounter other characters and fight or trade, run away from, or ignore each other. To resolve encounters in offline mode, a turn based system was used. It consisted of a formula and a random factor. The outcome of the events would be in the news for the player to read, if another character was there to witness them. 

%In online mode, the NPC has 3 senses - visual, sound and damage. These allow the character to respond to enemy threats, or objects that can be picked up.\\

%Character decision making is done with GOAP (Goal-Orientated Action Planning).\\
%There were four iterations of decision making:
%-stack based FSM
%-hierarchical FSM
%-Switched to GOAP for action selection, and motivational graphs for goal selection.
%Goal-oriented action planning is very good for debugging

\subsubsection{Shadow of Mordor (2014)}

The Nemesis System in Middle Earth Shadow of Mordor \cite{Middl25:online} is a recent example of a game trying to make more interesting NPCs. The Orc captains in the game are shown in a menu (See Figure \ref{URUK}). If you have not met them before they are just a silhouette. The player can discover an Orc, revealing one of the silhouettes, by encountering that Orc in the wild, or by interrogating a regular Orc.\\

%Expand
While the Orc characters are not continuously simulated, their names and traits are persistent. When the player goes to their location on the map the Orc is spawned. The Orcs have a procedurally generated name, appearance and personality.\\

\begin{figure}[]
  \centering
  \includegraphics[scale=.85]{Images/nemesis.jpg}
  \caption{The Nemesis System in Shadow of Mordor \cite{Middl25:online}. The Orc captains are displayed according to their place in the Uruk faction hierarchy.}
  \label{URUK}
\end{figure}

The version of the Nemesis System seen in the released version of the game was significantly scaled back from the original goals \cite{Gamas70:online}: \begin{displayquote}For example at one point we had multiple Uruk Factions with separate bars for Morale and Discipline, each Captain influenced these Bars and their state determined the behavior of the Orcs in combat as well as emergent missions. At this point, their Hierarchy UI looked somewhat like a Christmas tree.\end{displayquote} However, the Nemesis system was very well received by critics and fans, which shows that there is a strong desire for more interesting NPCs and emergent gameplay.\\

%Bithell discusses the appeal of the system, as well as its limitations \cite{Imake15:online}:

%\textit{"Nothing makes you believe in a game character as much the implication that they do stuff when you’re not looking. This is of course not the case.. these are straightforward generated side quests that only occur if you arrive, but there are intentionally just a few too many of them, meaning that those you don’t get to are fought between Orcs and result in outcomes you didn’t interact with."}

%"The stats behind these encounters between Orcs are obfuscated, deliberately.. I suspect to cover the fact that this system usually favours Orcs the player has had some interaction with, be it failed mission attempts, directly being killed, or whatever. 
%The game designers know that a random shadow killing the Orc that has bested you three times doesn’t serve the story you’re telling yourself, and never forget, they don’t want to simulate a real world, they’re simulating a world about you."
%"If the lesson is 'make players care about locations, characters and events through randomisation, persistence, reaction to player actions and implied actions off screen’ then one can see similar stat based meta stories occurring far more diversely."

\section{Machine Learning}

\subsection{Combat Outcome Prediction}

%Short intro to the field
There has been work done recently in trying to predict the outcome in MOBA (Multiplayer Online Battle Arena) and RTS (Real-time Strategy) games. StarCraft (1998) in particular is popular in AI research due to the Brood War API (BWAPI) \cite{bwapi0:online} which allows the programming of custom StarCraft AIs. The research has been mainly to develop strategies to assist either human or AI players. Two common modelling goals are to find the best way to spend resources, or the best way to use individual units to win combats \cite{yang2014identifying}.\\

Yang et al. \cite{yang2014identifying} present a data-driven approach for identifying patterns in combat that lead to success in MOBA games - specifically Dota 2 (2013). Dota 2 is a 5v5 MOBA game. Yang et al. use game logs from pro game competitions to build their model. They model combat as a sequence of graphs that represent the ten players' interactions at different times. The interactions modelled are dealing damage and healing. There are eleven nodes in a graph (See Figure \ref{DOTA}), each node in the graph being a character role for each team, plus an extra one for death. A unidirectional edge is made between two agents if one is attacking or healing the other. An edge is drawn to the death node if the agent died in that stage of the game.\\

\begin{figure}[]
  \centering
  \includegraphics[scale=.45]{Images/graphsyang.png}
  \caption{A sequence of graphs describing a game in Dota 2. The game stages are around 9 minutes apart. $Tx\_ Ty =$ Team X Role Y. \cite{yang2014identifying}}
  \label{DOTA}
\end{figure}

There are a number of graph metrics that are recorded, such as how many in and out connections a node has. These are used as features to train a decision tree. Different features are tried until a tree with a good classification accuracy can be trained. Combat rules are then generated from the tree, simply by tracing a path from the root node to the leaf nodes. However these rules are written in terms of the graph metrics used to train the tree. They are difficult to relate to real in-game actions. Therefore they use the combat rules to find subgraphs which show good combat tactics. This study shows that it is possible to predict in-game outcomes using machine learning, and also that it is possible to extract useful information about the game in the process of doing so.\\

S\' anchez-Ruiz \cite{sanchez2015predicting} uses machine learning techniques to predict small battles in StarCraft. Being able to predict the outcome of a battle would allow an AI to adapt their strategy. The paper uses Linear and Quadratic Discriminate Analysis, Support Vector Machines, and k-Nearest Neighbours. The scenario used was a battle between two groups of 6 units. The author collected data from 200 games. Samples were taken six times per second. At the end of a game, all the samples were labelled either one or zero for win or lose. The features recorded were the number of units, their total amount of life and the area of the minimum axis aligned rectangle that could enclose the agents, for both teams. A seventh feature was the distance between the two teams rectangles. The average game length was $\sim 19$ seconds.\\

The Support Vector Machine had a global accuracy of 76.27\% while the KNN and KKNN (Weighted K-Nearest) algorithms had a very high global accuracy of 84.3\% and 95.7\% respectively. The accuracy of each model increases as the game evolves, but the nearest neighbours can predict who will win very early in the game, before the agents even begin fighting. The author does mention a potential reason for the high accuracy of the case based approaches (KNN and KKNN) is that the dataset is biased due to the AI playing against itself and not using enough variety of attack strategies. The author planned future work is to test with human players and also with more complex battles, and when predicting to consider the evolution of the game rather than just a snapshot.

\subsection{The $\mu$-SIC System}

An Artificial Neural Network (ANN) can be used to resolve character interactions and events. The $\mu $-Sic system \cite{macnamee2003creating} uses an ANN (See Figure \ref{ANN}) that has been trained to take a set of values, and determines the action that a character should take. The inputs are the personality of the agent, the agent's relationship with the other agent, and the agent's mood. The ANN was trained with 300 hand-crafted example scenarios (as there were no databases for how people interact), and then used this to solve for the general space of possibilities in real-time. The output layer contains all the possible actions the character can take. The largest output is the interaction that is used.\\

This model was a component in the PPA architecture \cite{mac2001proposal} mentioned earlier in this chapter. Neural Networks are very fast once they've been trained. This allows the $\mu $-Sic system to generate believable interactions with minimal runtime cost. This cheap way to resolve encounters was a major influence on the direction of this dissertation. A big advantage of using an ANN for the LOD system described in this dissertation is that there is plentiful data available to train with since the samples are taken from the game itself.\\

\begin{figure}[]
\centering
\includegraphics[scale=.7]{Images/uSicANN.jpg}
\caption{The ANN used by $\mu$-SIC \cite{macnamee2003creating}}
\label{ANN}
\end{figure}

%It is a common misconception that Neural Networks are slow. It may take some time to train, but when trained a neural network is very fast, as it is just a query.\\ 

\subsection{Black and White (2001)}

One of the most famous examples of Machine Learning being applied in a commercial video game is Black and White \cite{bwls}, a god game by Lionhead Studios. In the game, there is a creature that the player owns, and it responds to feedback from the player. The player is able to slap or pet the creature. The creature has a decision tree which attempts to predict the player's feedback to potential actions by the creature. Whenever the player gives feedback, the decision tree is updated. In this way the player is able to teach the creature how to behave.

%\subsection{Machine Learning in Commercial Games}

\section{Summary}

%Here be the summary.
This chapter reviewed the AI/Simulation LOD approaches that have been taken in academia and in the games industry. Unlike graphical LOD no single method has emerged, likely because there is no obvious method, and it can be very domain-specific. The purpose of this review was to inform the design of the system put forth in this dissertation. %There is also scope for future work in which elements of other systems are implemented. This will be discussed in Chapter 6.\\

Papers about Machine Learning applied to the prediction of encounter outcomes were also examined. Firstly papers predicting combat outcomes were discussed, and then the $\mu$-Sic system which 'predicts' an appropriate character reaction given a situation. The papers discussed show that games are a great test bed for machine learning. Due to the ease of data creation in a virtual environment, very accurate models can be built.


