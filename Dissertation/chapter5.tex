\chapter{Evaluation}

This section will evaluate the LOD system under three criteria: accuracy, processing and memory.

\section{Model Accuracy}

%We can measure accuracy, yay!
Since the goal of the system is to simulate agents realistically when out of view, a measurement for proxy simulation accuracy would be useful. In most cases simulation LOD is only evaluated on the criteria of runtime performance, and sometimes a survey with real users to evaluate believability \cite{Wissner:2010:LDA:1948395.1948423}\cite{sunshine2013managing}. With the approach that has been taken, accuracy of the lower level of detail can be measured precisely. This is because we have a test set with labelled feature vectors to test the predictions against.\\

%How it is evaluated
The dataset that has been collected by the agent sampler component is split into the training and test sets, to train and evaluate the model. Most of the algorithms can only produce models which predict one target variable. Neural Networks are the exception in that they can produce a vector output. When collecting samples, the Spawner was set to spawn between 2 and 20 agents. The Spawner created a new instance whenever a team had won or there had been a period of no encounters.\\ 

%naming convention
The naming convention for models is \texttt{\textunderscore featureVectorVersion\textunderscore (binary flags for won,duration,xpGain,hpLoss)}. For example, the model which uses version 2 of the feature vector and outputs a predicted duration is named \texttt{\textunderscore 2\textunderscore 0100.}\\

\begin{table}[]
\centering
\caption{Model \textunderscore 2\textunderscore 1000. Training set size: 5000. Test set size: 5000. This model predicts whether the agent will survive the encounter.}
\begin{adjustbox}{width=1\textwidth}
\label{_2_1000}
\begin{tabular}{|l|l|l|l|l|}
\hline
Model    & Accuracy & Avg. predict time(ms)  & Model parameters    & Training time (ms) \\ 
\hline
Base     & 0.551    &                        &                     & N/A                \\ \hline
ANN      & 0.8414   & 0.0031                 & Activation: Sigmoid & 1471               \\ \hline
Bayes    & 0.7846   & 0.0026                 &                     & 3                  \\ \hline
Boost    & 0.8446   & 0.0272                 &                     & 2377               \\ \hline
DTree    & 0.8282   & 0.0062                 &                     & 24                 \\ \hline
ERTree   & 0.4324   & 0.0015                 &                     & 117                \\ \hline
GBTree   & 0.4308   & 0.0012                 &                     & 4                  \\ \hline
KNN      & 0.7366   & 0.221                  &                     & 0                  \\ \hline
RTrees   & 0.8444   & 0.0032                 & 10 Trees            & 246                \\ \hline
RTrees   & 0.8532   & 0.0254                 & 100 Trees           & 2517               \\ \hline
SVM      & 0.6272   & 0.1188                 &                     & 919                \\ \hline
Ensemble & 0.8774   & 0.1135                 &                     & N/A                \\ \hline
\end{tabular}
\end{adjustbox}
\end{table}

\begin{table}[]
\centering
\caption{Model \_2\_0100 - Training set=5000 Test set=5000. This model predicts the duration (in seconds) of a battle}
\begin{adjustbox}{width=1\textwidth}
\label{_2_0100}
\begin{tabular}{|l|l|l|l|l|l|}
\hline
Model    & RMSE   & MAE    & Avg. Predict Time (ms) & Parameters & Training Time \\ \hline
ANN      & 5.6539 & 4.0904 & 0.00287                &            & 519           \\ \hline
DTrees   & 5.4629 & 3.5009 & 0.00231                &            & 34            \\ \hline
ERTrees  & 5.6910 & 4.1188 & 0.23790                & 500 Trees  & 25918         \\ \hline
KNN      & 8.7234 & 6.3291 & 0.22489                &            & 0             \\ \hline
SVM      & 6.9322 & 5.2214 & 0.05935                &            & 523           \\ \hline
RTrees   & 5.2001 & 3.4992 & 0.00327                & 10 Trees   & 386           \\ \hline
Rtrees   & 5.1344 & 3.4684 & 0.39396                & 500 Trees  & 17477         \\ \hline
Ensemble & 5.9819 & 3.9778 & 0.01522                & Mean       & N/A           \\ \hline
Ensemble & 5.1373 & 3.4237 & 0.03176                & Median     & N/A           \\ \hline
\end{tabular}
\end{adjustbox}
\end{table}

%Target Variables
The four target variables of interest are \texttt{didSurvive}, \texttt{duration}, \texttt{xpGain} and \texttt{hpLoss}. \texttt{didSurvive} is a binary classification problem. \texttt{duration}, \texttt{xpGain} and \texttt{hpLoss} are continuous variable regression problems. The Bayes and Boost algorithms can only do classification problems, so they are only used for the \texttt{didSurvive} model. In Tables \ref{_2_1000} and \ref{_2_0100} results are shown for models predicting \texttt{didSurvive} and \texttt{duration}. In Table \ref{vectorResponseTable} results are shown for a Neural Network predicting all four target variables.\\

In Tables \ref{_2_1000} and \ref{_2_0100}, the models shown are trained to predict one target variable. For continuous target variables, the Root Mean Squared Error (RMSE) and Mean Average Error(MAE) are calculated. Both measure the average error in the same scale as the target variable being predicted, with RMSE lending extra weight to larger errors.  For discrete target variables, or classes, the accuracy is simply a percentage of correct classifications. The classification results are compared against a base classifier which just chooses the class that is most frequent.\\

%combined _2_1000 and _2_0100 results discussion
In Table \ref{_2_1000}, the results for the \texttt{\textunderscore 2\textunderscore 1000} model are shown. The main considerations when choosing an algorithm are the accuracy and the prediction time. The prediction time must be quick so the model can be consulted numerous times within in a single frame without slowing down the game. The speed of the Random Tree and Extremely Random Tree algorithms only become acceptable when using a small number of trees. The accuracy does decrease somewhat when the number of trees is reduced for the Random Tree algorithm, but it still has very good accuracy. The Neural Net using the Sigmoid activation function also performs very well. Table \ref{_2_0100} shows the results for the duration model. Neural Networks and Random Trees again perform well. Algorithms that performed poorly for classification, such as the Extremely Random Tree algorithm, perform well here.\\

\begin{figure}
\begin{tikzpicture}
\centering
\begin{semilogxaxis}[ylabel = {Accuracy}, xlabel = {Average Prediction Time(ms)}, width=12cm]
\addplot [scatter, only marks, scatter src=explicit symbolic, nodes near coords,
visualization depends on=\thisrow{alignment} \as \alignment,
every node near coord/.style={anchor=\alignment}]
table [meta=Model,y=Accuracy, x=Avg. predict time (ms), col sep=comma] {Data/ML/ml2_scatter.csv};
%\caption{A scatter plot showing prediction speed and accuracy of the various algorithms. A logarithmic scale is used to due the wide variation in prediction speeds.}
\end{semilogxaxis}
\end{tikzpicture}
\caption{The accuracy and prediction time of the various algorithms. Multiple RTrees are shown as different forest sizes produce different results. Using a small forest will result in a speedier, but less accurate prediction, as shown with the furthest left RTrees.}
\label{accxspeed}
\end{figure}

%Further discussion
The accuracy and prediction time columns from Table \ref{_2_1000} are plotted in Figure \ref{accxspeed}. Algorithms in the placed on the left of the plot are the most suitable for a real-time system due to their high prediction speed. Algorithms close to the top have the highest accuracy. Neural Network and Random Trees both perform very well, placing in the top left of the plot. Since K-Nearest Neighbours is lazily evaluated, all the computation takes place at run-time, making it quite slow. This can be seen in Figures \ref{speedapp1} and \ref{speedapp2} included in the appendix which plot prediction time for each algorithm against number of training samples. KNN prediction time increases linearly with training samples as opposed to the other algorithms which remain constant. Since speed is a major consideration for the LOD system - this rules out the use of KNN. The speed of the Random Trees algorithm increases linearly with the amount of trees, so there is a trade-off between accuracy and prediction speed.\\ 

%The use of multiple algorithms
An advantage of having a few different algorithms at hand is that different problems may be more suited to varying approaches. This allows the best approach to be selected for solving the problem. A potential feature of the framework is the automatic selection of the best performing model for use in the LOD system. Another application of using multiple algorithms is they can be combined into an ensemble. The ensemble meta-algorithm implemented uses a majority vote for classification problems, or the mean or median for regression problems. While the ensemble produces the highest accuracy, it comes at the cost of prediction speed at runtime.\\

\begin{figure}
\centering
\begin{tikzpicture}
\begin{axis}[xlabel={Training Samples}, 
			ylabel={Accuracy}, 
			axis lines = left, 
			width=15cm, 
			legend style={font=\small}, 
			legend pos=south east]
\addplot[color=red, mark=none, smooth] table [x=trainingSet, y=accuracy0, col sep=comma, select coords between index={193}{291}] {../Results/_2_1111/_2_1111_ANN.csv};
\addlegendentry{\_2\_1111\_ANN (Gaussian)}
\addplot[color=green, mark=square, smooth] table [x=trainingSet, y=accuracy0, col sep=comma, select coords between index={12}{111}] {../Results/_2_1000/_2_1000_Bayes.csv};
\addlegendentry{\_2\_1000\_Bayes}
\addplot[color=orange, mark=diamond, smooth] table [x=trainingSet, y=accuracy0, col sep=comma, select coords between index={12}{111}] {../Results/_2_1000/_2_1000_Boost.csv};
\addlegendentry{\_2\_1000\_Boost}
\addplot[color=black, mark=x, smooth] table [x=trainingSet, y=accuracy0, col sep=comma, select coords between index={15}{114}] {../Results/_2_1000/_2_1000_DTree.csv};
\addlegendentry{\_2\_1000\_DTree}
\addplot[color=red, mark=o, smooth] table [x=trainingSet, y=accuracy0, col sep=comma, select coords between index={12}{111}] {../Results/_2_1000/_2_1000_KNN.csv};
\addlegendentry{\_2\_1000\_KNN}
\addplot[color=blue, mark=none, smooth] table [x=trainingSet, y=accuracy0, col sep=comma, select coords between index={12}{111}] {../Results/_2_1000/_2_1000_RTrees.csv};
\addlegendentry{\_2\_1000\_RTrees (50 Trees)}
\addplot[color=blue, mark=square, smooth] table [x=trainingSet, y=accuracy0, col sep=comma, select coords between index={162}{261}] {../Results/_2_1000/_2_1000_RTrees.csv};
\addlegendentry{\_2\_1000\_RTrees (15 Trees)}
%\addplot[color=brown, mark=none, smooth] table [x=trainingSet, y=accuracy, col sep=comma, select coords between index={0}{100}] {../Results/_2_1000/_2_1000_SVM.csv};
%\addlegendentry{\_2\_1000\_SVM}
\end{axis}
\end{tikzpicture}
\caption{The convergence of the various algorithms predicting \texttt{didSurvive}. A test set size of 1000 was used.}
\label{convergeSurvive}
\end{figure}

\begin{figure}
\centering
\begin{tikzpicture}
\begin{axis}[xlabel={Training Samples}, 
			ylabel={Duration RMSE (s)}, 
			axis lines = left, 
			width=15cm, 
			legend style={font=\small}]
\addplot[color=red, mark=none, smooth] table [x=trainingSet, y=RMSE1, col sep=comma, select coords between index={193}{291}] {../Results/_2_1111/_2_1111_ANN.csv};
\addlegendentry{\_2\_1111\_ANN (Gaussian)}
\addplot[color=black, mark=x, smooth] table [x=trainingSet, y=RMSE0, col sep=comma, select coords between index={0}{99}] {../Results/_2_0100/_2_0100_DTree.csv};
\addlegendentry{\_2\_0100\_DTree}
%\addplot[color=green, mark=none, smooth] table [x=trainingSet, y=RMSE0, col sep=comma, select coords between index={162}{261}] {../Results/_2_0100/_2_0100_ERTrees.csv};
%\addlegendentry{\_2\_0100\_ERTrees}
\addplot[color=red, mark=o, smooth] table [x=trainingSet, y=RMSE0, col sep=comma, select coords between index={0}{99}] {../Results/_2_0100/_2_0100_KNN.csv};
\addlegendentry{\_2\_0100\_KNN}
\addplot[color=blue, mark=o, smooth] table [x=trainingSet, y=RMSE0, col sep=comma, select coords between index={0}{99}] {../Results/_2_0100/_2_0100_RTrees.csv};
\addlegendentry{\_2\_0100\_RTrees (15 Trees)}
\addplot[color=blue, mark=none, smooth] table [x=trainingSet, y=RMSE0, col sep=comma, select coords between index={151}{250}] {../Results/_2_0100/_2_0100_RTrees.csv};
\addlegendentry{\_2\_0100\_RTrees (50 Trees)}
\addplot[color=brown, mark=none, smooth] table [x=trainingSet, y=RMSE0, col sep=comma, select coords between index={0}{99}] {../Results/_2_0100/_2_0100_SVM.csv};
\addlegendentry{\_2\_0100\_SVM}
\end{axis}
\end{tikzpicture}
\caption{The convergence of the various algorithms predicting \texttt{duration}. A test set size of 1000 was used.}
\label{convergeDuration}
\end{figure}

%Convergence
Figure \ref{convergeSurvive} shows how the accuracy changes over various training set sizes. The same test set is used for all of these tests, which comes from the front of the dataset.  
%To evenly distribute the samples a shuffle was implemented, as some samples were taken under different spawner settings, which affects the size of encounters.
The general trend seen in Figure \ref{convergeSurvive} is upward the more training samples that were used. However good results can be achieved with quite a small sample set. Sometimes the Neural Network Backpropagation algorithm does not converge which accounts for the occasional dip in accuracy for \texttt{\textunderscore 2\textunderscore 1111\textunderscore ANN}. In these cases a very long training time was observed. Random Trees tended to be more accurate than the Neural Networks. Figure \ref{convergeDuration} shows a convergence plot for the prediction of duration. Further convergence plots are included in the appendix.\\

The results in Tables \ref{_2_1000} and \ref{_2_0100} are for a Neural Network with a scalar output, but similarly good results are obtained with a Neural Network outputting a vector response, as can be seen in Table \ref{vectorResponseTable}, which shows results of training a neural network with $fvectorv1$ and $fvectorv2$. The second feature vector improves the prediction quality, without the extra features having any negative impact on the prediction time. The Gaussian activation function worked better than Sigmoid for Neural Networks with a vector output.\\

\begin{table}[]
\centering
\caption{Training results for Neural Nets using the Gaussian activation function trained with feature vectors version 1 (\_1\_1111) and version 2 (\_2\_1111). Training Set: 7000. Test Set: 1000.}
\label{vectorResponseTable}
\begin{tabular}{l|l|l|}
\cline{2-3}
                                           & \_1\_1111 & \_2\_1111 \\ \hline
\multicolumn{1}{|l|}{Avg. Prediction Time} & 0.002326  & 0.002301  \\ \hline
\multicolumn{1}{|l|}{didSurvive (\%)}      & 80.44     & 85.2      \\ \hline
\multicolumn{1}{|l|}{duration (RMSE)}      & 6.6738    & 6.2054    \\ \hline
\multicolumn{1}{|l|}{duration (MAE)}       & 4.6487    & 4.2610    \\ \hline
\multicolumn{1}{|l|}{xpGain (RMSE)}        & 32.0619   & 31.3898   \\ \hline
\multicolumn{1}{|l|}{xpGain (MAE)}         & 24.6001   & 24.0823  \\ \hline
\multicolumn{1}{|l|}{hpLoss (RMSE)}        & 63.3510   & 60.6519  \\ \hline
\multicolumn{1}{|l|}{hpLoss (MAE)}         & 50.6910   & 44.9081  \\ \hline
\end{tabular}
\end{table}

%Feature Vectors
The two feature vectors used are shown below:

\begin{equation} \label{fveq}
fvectorv1^\mathsf{T} =
\left(\begin{smallmatrix}
    characterClass \\ 
    unitLevel \\
    unitHp \\ 
    alliesNumberOfAgents \\ 
    alliesAverageHealth \\ 
    alliesAverageLevel \\ 
    enemuesNumberOfAgents \\ 
    enemiesAverageHealth \\ 
    enemiesAverageLevel
\end{smallmatrix}\right)
fvectorv2^\mathsf{T} =
\left(\begin{smallmatrix}
    characterClass \\ 
    unitLevel \\ 
    unitHp \\ 
    premptiveStrike \\ 
    alliesNumberOfAgents \\ 
    alliesAverageHealth \\ 
    allieTeamComposition \\ 
    alliesCritical \\ 
    enemiesNumberOfAgents \\ 	
    enemiesAverageHealth \\ 
    enemiesAverageLevel \\ 
    enemiesTeamComposition \\ 
    enemiesCritical
\end{smallmatrix}\right)
\end{equation}\\

where $alliesTeamComposition$ and $enemiesTeamComposition$ are the vector

\begin{equation}
\begin{bmatrix}
    fractionFighters & fractionGunners & fractionArchers & fractionMages 
\end{bmatrix}
\end{equation}

%Feature Engineering
The feature vectors need to expose the underlying structure of the problem to the algorithms so they can make good predictions. Anything in the simulation that has an impact on the resulting target variable in question should be considered when selecting features. As the features are domain specific, anyone using this framework will need to do feature engineering. Choosing too many features could lead to overfitting.\\

\pgfplotstableread[col sep=comma]{Data/ML/varImportance.csv}\loadedtable

\begin{figure}
\centering
\begin{tikzpicture}[scale=1]
\centering
\begin{axis}[
	xlabel={Relative Importance}, 
    %height=50cm, %the height of the plot
    width=10cm,%the width of the plot
    bar width=2pt,%the width of the bars
    xbar=1pt,%the width of the bars
    y=0.8cm,%the space between the features
    %enlargelimits=0.3,
    ytick=data,
    yticklabels from table={\loadedtable}{Feature},
    xticklabels={,,},
    %y tick label style={anchor=north,font=\small}
    major y tick style = transparent,
    ]
\foreach \i in {didSurvive,duration,xpGain,hpLoss}
{
	\addplot table [y expr=\coordindex, x=\i] {\loadedtable}; 
}
\legend{didSurvive,duration,xpGain,hpLoss};
\end{axis}
\end{tikzpicture}
\caption{The relative importance of each variable in making a prediction for the four target variables. This data was obtained from the Random Trees algorithm.} \label{fig:M1}
\label{varimportfig}
\end{figure}


%Feature Vectors
The first feature vector built, $fvectorv1$, had nine features. It produced good results of over 80\% accuracy, but it was missing some important information, such as the character classes of a unit's allies and enemies. For $fvectorv2$ the team composition for both sides is taken in to account. This adds eight features, as there are four character classes. The boolean \textit{preemtiveStrike} was also added. This is true if the agent is not in combat at the time of the prediction. This means they are getting the 'jump' on their opponent in fights involving more than 2 agents. In $fvectorv1$, since the average health for the allies and enemies was just an average, it could obscure the fact that there may be some agents in critical condition, so the fraction of agents in critical condition on the allied and enemy teams are used as features. \\
%The implementation allows for different feature vectors to be used from the same dataset. %Last sentence a bit out of place?

%Variable importance
Another benefit of having a suite of algorithms is that some of them have extra uses outside of prediction. Whilst Neural Networks are very powerful, they are a black box, and can't expose much about how they arrived at their prediction. This is not the case with tree algorithms, which can tell you which features best split the data. The Random Forest implementation provides a function which returns a vector of the same dimensions as the feature vector containing scores for the relative importance of each feature. How important each variable is in predicting the four target variables is shown in Figure \ref{varimportfig}.\\

%Discuss Vector Response Results, and training results

%Convergence discussion
%Random Trees tended to be more accurate than the Neural Networks. %Convergence plots in Figure \ref{convergeSurvive} and Figure \ref{convergeDuration} show show how the accuracy increases when the training set size is increased. 
%Neural Networks are more difficult to train, as can be seen in the high variance in accuracy. 
%They seem to be very sensitive to outliers as their accuracy decreased when some larger battles were included in the dataset, whereas Random Forests maintained their high accuracy. 
%Further convergence plots are included in the appendix.\\

%TODO: Accuracy vs. number of agents in encounter
%TODO: Accuracy vs. difference in forces size

%Parameter tuning?

%Misclassifications
The best models trained can predict with $\sim 85\%$ accuracy if an agent will survive an encounter, and to within $\sim 4$ seconds the duration of the encounter. From visual inspection, the misclassifications usually occur in cases where the two sides are very evenly matched, and it comes down to chance, such as whose attacks are registered first or who is targeting who in larger fights. In these cases where it could go either way, a misclassification will have no discernible impact on the believability of the simulation.\\

\section{Runtime Performance}

The goal of the project is to simulate off-screen agents in as cheap a manner as possible while still maintaining the integrity of the simulation. The previous section has shown how the approach can produce accurate predictions when given an input vector. The other important consideration is how efficient the system is. Modern games can only allocate a very small window of the frame time processing to off-screen agents. Another important consideration is how much memory is being used by the approach, as an aim of the system is to simulate a large number of agents. 

\subsection{Processing}

Unity 5 provides a CPU and memory profiler, however it doesn't export usable data so it is only really useful for visual inspection and to aid optimisation. The results in Figure \ref{updatetimefig} were acquired by averaging the frame update time over many frames. All tests were performed on a Windows 10 PC with a AMD FX-4170 4.20GHz Quad Core processor, 8 GB of DDR3 RAM and the GTX 570 graphics card. Version 5.3.4f1 of Unity3d was used.\\

The system was able to simulate 1000 agents at 588 frames / second on average, or 1.7 ms / frame for the full update. The budget for a frame is typically 16ms to meet the need for 60 frames per second, so this leaves plenty of time left over. Also, not all of the 1.7ms was attributable to the LOD system, as it was the full game update being measured. The 'Cheaper Full Simulation' in Figure \ref{updatetimefig} has the full game logic but with no audioControllers, rendering turned off, and the navmesh agents collision avoidance turned off. This allowed a more fair assessment of the savings made by the LOD system.\\

\begin{figure}
\centering
\begin{tikzpicture}
\begin{axis}[xlabel={Number of Agents}, ylabel={Average Update Time (ms)}, axis lines = left, legend pos=north west]
\addplot[color=red, mark=o] table [x=noOfAgents, y=avgUpdateMs, col sep=comma, select coords between index={0}{5}] {Data/Processing/update_time.csv};
\addlegendentry{Full Simulation}
\addplot [color=blue, mark=o] table [x=noOfAgents, y=avgUpdateMs, col sep=comma, select coords between index={6}{11}] {Data/Processing/update_time.csv};
\addlegendentry{Cheaper Full Simulation}
\addplot [color=green, mark=o] table [x=noOfAgents, y=avgUpdateMs, col sep=comma, select coords between index={12}{27}] {Data/Processing/update_time.csv};
\addlegendentry{Proxy Simulation}
\end{axis}
\end{tikzpicture}
\caption{The average update time taken with different amounts of agents in the scene. The profiling was done in an optimised build of the game.}
\label{updatetimefig}
\end{figure}

Some sections of code vital to the LOD system were also benchmarked. These were \texttt{SetLOD()} which performs the level of detail switch. This function is quite involved as it has to add or remove a number of components from the object based on which level of detail the agent is to be simulated at. \texttt{SetLOD()} averaged 0.27ms per call over 1314 iterations. This could be optimising using Object Pooling, which will be discussed in the Memory section of this chapter.\\ 

When an encounter begins, the details for each faction in the encounter are computed. This computation is shared for each NPC's feature vector, so the agents only have to compute parts of the feature vector specific to them. The \texttt{Predict()} function calls \texttt{GetFeatureVector()} and then consults the model (Neural Network tested). \texttt{GetFeatureVector()} computes the feature vector for a particular agent when it needs a prediction. \texttt{GetFeatureVector()} averaged 0.005768ms over 31286 iterations. Building the feature vector is slightly more expensive than consulting the neural network itself. The full prediction function which builds the feature vector and consults the model averaged 0.013602 ms per call. If we were to budget 1ms / frame for predictions, we could perform 73 predictions. \\ 

\begin{figure}[]
\centering
\includegraphics[scale=.5]{Images/unityprofiler.jpg}
\caption{CPU utilisation in the Unity Profiler. Orange = Physics Engine. Light purple = NavMeshManager. Green = Rendering. Dark purple = MonoBehaviours i.e. scripts}
\label{unityprofiler}
\end{figure}

Figure \ref{unityprofiler} shows the CPU utilisation in the Unity Engine when 1000 agents are being simulated at the proxy level of detail. A big chunk of the processing is used by the physics engine and the NavMeshManager. The physics engine is managing all the trigger colliders which are used to trigger encounters. Spheres are very cheap collision shapes, and with broadphase collision detection optimisations in the PhysX engine in use by Unity, thousands of sphere colliders can be easily handled. However, there is definitely scope to find an even cheaper way to track off-screen agent encounters.\\ 

An even bigger chunk of the processing is used by the NavMeshManager, even without the collision avoidance and with a relatively coarse navigation mesh used. The Unity Engine does not have any LOD features for navigation meshes. There is scope to improve this aspect of the project, and work has been done on level of detail for navigation \cite{Brom2007}. The spike near the beginning of \ref{unityprofiler} sometimes occurs when a very large encounter is initialised, requiring many new predictions. It is registered as physics under the profiler because it is called from a physics callback function. Spreading prediction queries over several frames is a possible solution.
%(TODO: Study STALKER source code to see how they did it).

\subsection{Memory}

\begin{table}[]
\centering
\caption{The size of the models in memory}
\label{modelmem}
\begin{tabular}{|l|l|l|l|l|l|l|}
\hline
Model        & ANN & SVM & KNN & Bayes & Decision Tree & Random Trees \\ \hline
Size (Bytes) & 176 & 128 & 28  & 52    & 24            & 48           \\ \hline
\end{tabular}
\end{table}

The other runtime performance consideration is how much memory is used by the system. As discussed in Chapter 4, the NPC object expands and contracts based on what level of detail it is at. This is done to save memory resources when an agent is off-screen, as there is a lot of state at the full LOD which is no longer necessary at the proxy LOD. The trade-off is that the \texttt{SetLOD()} function is a bit more intensive as it has to add and destroy components rather than just enabling and disabling them. The observer radius for switching to back to proxy LOD has a small spillover to avoid unnecessary extra calls to the \texttt{SetLOD()} function. \\

%Object pooling
There are allocations and deallocations occurring as components are added and removed from the application. Instantiating and destroying objects on a regular basis is inefficient. Using Object Pooling, instead of objects being destroyed, they are placed in a buffer and de-activated. They are then taken from the buffer and re-activated when needed. There is just one initial memory allocation at the beginning of the program. This technique was used for the large crowds in Assassin's Creed Unity[ref] as mentioned in Chapter 2. However, this would have a larger memory footprint than the current system, so it is a trade-off between memory and CPU usage. It would still be cheaper in memory than maintaining full state for every agent, as the object pool only needs to have enough components to assign to the number of NPCs that are expected to be close to the observer.\\

Figure \ref{memfig} shows the memory usage of the application at the different levels of detail. As can be seen, the amount of state needed for the full agents is considerably larger. A breakdown of each component's memory usage as reported by the Unity Memory Profiler can be found in the appendix.\\

\begin{figure}
\centering
\begin{tikzpicture}
\begin{axis}[xlabel={Number of Agents}, ylabel={Memory (Mb)}, axis lines = left, legend pos=north west]
\addplot[color=red, mark=o] table [x=noOfAgents, y=totalMem, col sep=comma, select coords between index={19}{24}] {Data/Memory/mem_profile.csv};
\addlegendentry{Full Simulation}
\addplot[color=blue, mark=o] table [x=noOfAgents, y=totalMem, col sep=comma, select coords between index={25}{30}] {Data/Memory/mem_profile.csv};
\addlegendentry{Cheaper Full Simulation}
\addplot[color=green, mark=o] table [x=noOfAgents, y=totalMem, col sep=comma, select coords between index={31}{36}] {Data/Memory/mem_profile.csv};
\addlegendentry{Proxy Simulation}
\end{axis}
\end{tikzpicture}
\caption{Memory usage of the application when simulating different numbers of agents. The results are from snapshots of the memory usage taken from the Unity Profiler.}
\label{memfig}
\end{figure}

Another consideration is the memory footprint of the model itself. To test how much space each model takes up in memory, the C++ version of OpenCV was used. This is because the unmanaged memory usage could not be assessed in C\#. The results are shown in Table \ref{modelmem}. Other objects in memory attributable to the LOD system are the encounter spheres and \textit{EncounterManager} which use 136 bytes (each) and 168 bytes respectively.


%From chapter 2...
%Object pooling is used for the meshes. Object Pooling is an optimisation pattern in which a set amount of objects are loaded into memory once and are then re-used throughout the lifetime of the program, as opposed to allocating and freeing them continuously \cite{Objec62:online}. When an agent needs their LOD changed, the best matching entity in the object pool is selected, and props such as hats are spawned.

%and Figure \ref{memfig2} shows the memory resources required by an individual agent at full and proxy levels. 

\section{Summary}

Overall the system made significant gains in performance. There was a 68x speed-up between \textit{Proxy} and \textit{Cheaper Full} when simulating 1000 agents. There is reason to believe even more savings could be made which will be discussed in Future Work. There was a 3.94x reduction in memory usage by the \textit{Proxy} simulation vs. the \textit{Cheaper Full} simulation when simulating 1000 agents.  

%xN speed up
%xN memory






