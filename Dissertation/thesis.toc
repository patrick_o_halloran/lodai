\contentsline {chapter}{Acknowledgments}{iv}{chapter*.3}
\contentsline {chapter}{Abstract}{v}{chapter*.4}
\contentsline {chapter}{List of Tables}{viii}{chapter*.6}
\contentsline {chapter}{List of Figures}{ix}{chapter*.7}
\contentsline {chapter}{Chapter{} \numberline {1}Introduction}{1}{chapter.1}
\contentsline {section}{\numberline {1.1}Motivation}{2}{section.1.1}
\contentsline {section}{\numberline {1.2}Objectives}{2}{section.1.2}
\contentsline {section}{\numberline {1.3}Dissertation Roadmap}{3}{section.1.3}
\contentsline {chapter}{Chapter{} \numberline {2}State of the Art}{4}{chapter.2}
\contentsline {section}{\numberline {2.1}Simulation LOD}{4}{section.2.1}
\contentsline {subsection}{\numberline {2.1.1}Simulation/AI LOD in Commercial Games}{10}{subsection.2.1.1}
\contentsline {subsubsection}{Assassin's Creed Unity (2014)}{10}{section*.11}
\contentsline {subsubsection}{S.T.A.L.K.E.R.: Shadow of Chernobyl (2007)}{11}{section*.13}
\contentsline {subsubsection}{Shadow of Mordor (2014)}{13}{section*.14}
\contentsline {section}{\numberline {2.2}Machine Learning}{14}{section.2.2}
\contentsline {subsection}{\numberline {2.2.1}Combat Outcome Prediction}{14}{subsection.2.2.1}
\contentsline {subsection}{\numberline {2.2.2}The $\mu $-SIC System}{16}{subsection.2.2.2}
\contentsline {subsection}{\numberline {2.2.3}Black and White (2001)}{16}{subsection.2.2.3}
\contentsline {section}{\numberline {2.3}Summary}{17}{section.2.3}
\contentsline {chapter}{Chapter{} \numberline {3}Design}{18}{chapter.3}
\contentsline {section}{\numberline {3.1}Introduction}{18}{section.3.1}
\contentsline {section}{\numberline {3.2}Architecture Overview}{20}{section.3.2}
\contentsline {subsection}{\numberline {3.2.1}LOD system}{20}{subsection.3.2.1}
\contentsline {subsection}{\numberline {3.2.2}Machine Learning}{24}{subsection.3.2.2}
\contentsline {subsubsection}{Sampling}{24}{section*.20}
\contentsline {subsubsection}{Feature Selection}{25}{section*.22}
\contentsline {subsubsection}{Target Variables}{25}{section*.23}
\contentsline {subsubsection}{Preparing the data}{26}{section*.24}
\contentsline {subsubsection}{Learning}{27}{section*.25}
\contentsline {section}{\numberline {3.3}Summary}{27}{section.3.3}
\contentsline {chapter}{Chapter{} \numberline {4}Implementation}{28}{chapter.4}
\contentsline {section}{\numberline {4.1}Platform Selection}{28}{section.4.1}
\contentsline {section}{\numberline {4.2}Implementing the System}{31}{section.4.2}
\contentsline {subsection}{\numberline {4.2.1}Overview}{31}{subsection.4.2.1}
\contentsline {subsection}{\numberline {4.2.2}Spawning the agents}{31}{subsection.4.2.2}
\contentsline {subsection}{\numberline {4.2.3}Building the Feature Vector}{31}{subsection.4.2.3}
\contentsline {subsection}{\numberline {4.2.4}Encounter System}{32}{subsection.4.2.4}
\contentsline {subsection}{\numberline {4.2.5}LOD Switching}{35}{subsection.4.2.5}
\contentsline {subsection}{\numberline {4.2.6}Machine Learning}{36}{subsection.4.2.6}
\contentsline {subsubsection}{Artificial Neural Network}{37}{section*.33}
\contentsline {subsubsection}{K-Nearest Neighbours}{38}{section*.34}
\contentsline {subsubsection}{Naive Bayes Classifier}{38}{section*.35}
\contentsline {subsubsection}{Decision Tree}{39}{section*.36}
\contentsline {subsubsection}{Boosting}{39}{section*.37}
\contentsline {subsubsection}{Random Trees}{40}{section*.38}
\contentsline {chapter}{Chapter{} \numberline {5}Evaluation}{41}{chapter.5}
\contentsline {section}{\numberline {5.1}Model Accuracy}{41}{section.5.1}
\contentsline {section}{\numberline {5.2}Runtime Performance}{50}{section.5.2}
\contentsline {subsection}{\numberline {5.2.1}Processing}{50}{subsection.5.2.1}
\contentsline {subsection}{\numberline {5.2.2}Memory}{53}{subsection.5.2.2}
\contentsline {section}{\numberline {5.3}Summary}{54}{section.5.3}
\contentsline {chapter}{Chapter{} \numberline {6}Conclusion}{56}{chapter.6}
\contentsline {section}{\numberline {6.1}Main Contributions}{56}{section.6.1}
\contentsline {section}{\numberline {6.2}Future Work}{57}{section.6.2}
\contentsline {section}{\numberline {6.3}Final Thoughts}{59}{section.6.3}
\contentsline {chapter}{Appendices}{60}{section.6.3}
\contentsline {chapter}{Bibliography}{70}{appendix*.61}
