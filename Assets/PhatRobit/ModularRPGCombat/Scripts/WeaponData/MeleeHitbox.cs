﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class MeleeHitbox : MonoBehaviour
{
	public int damage = 0;
	public float activeTime = 1;
	public PlayerCommon playerCommon;

	private List<Collider> _hits = new List<Collider>();

	void OnEnable()
	{
		StartCoroutine(Deactivate());
	}

	private IEnumerator Deactivate()
	{
		yield return new WaitForSeconds(activeTime);
		gameObject.SetActive(false);
		_hits.Clear();
	}

    void OnTriggerEnter(Collider other)
	{
		if(!_hits.Contains(other))
		{
			_hits.Add(other);

			if(!other.isTrigger && other.CompareTag(Tags.Enemy))
			{
				EnemyStatController enemyStats = other.GetComponent<EnemyStatController>();

				if(enemyStats && enemyStats.Alive && (enemyStats.team == Team.None || enemyStats.team != playerCommon.StatController.team))
				{
					enemyStats.ModifyHealth(-damage, playerCommon.gameObject);
				}
			}
		}
	}
}