﻿using UnityEngine;
using System.Collections;

public class PlayerSpell : MonoBehaviour
{
	public float lifeSpan = 5;
	public int manaAmount = 10;								// How much Mana does the spell use?
	
	private float _life = 0;
	private PlayerCommon _playerCommon;		// Reference to the player common which is used to get weapon info
	private GameObject _player;								// Reference to the Player.
	
	private Transform _t;
	private Rigidbody _r;

	public PlayerCommon PlayerCommon
	{
		get { return _playerCommon; }
		set { _playerCommon = value; }
	}

	void Start()
	{
		_t = transform;
		_r = GetComponent<Rigidbody>();

		_life = lifeSpan;

		if(_playerCommon.StatController.Mana < manaAmount)	// Check if player got enough Mana to cast spell
		{
			Destroy(gameObject);
		}

		else
		{
			_playerCommon.StatController.ModifyMana(-manaAmount); // Use Mana
		}
	}
	
	void Update()
	{
		if(_life <= 0)
		{
			Destroy(gameObject);
		}
		else
		{
			_life -= Time.deltaTime;
		}
	}
	
	void FixedUpdate()
	{
		if(_playerCommon)
		{
			_r.velocity = _t.forward * _playerCommon.CombatController.spellSpeed;
		}
	}
	
	void OnTriggerEnter(Collider other)
	{
		if(_playerCommon)
		{
			if(!other.isTrigger && other.CompareTag(Tags.Enemy))
			{
				EnemyStatController enemyStats = other.GetComponent<EnemyStatController>();
				
				if(enemyStats && enemyStats.Alive && (enemyStats.team == Team.None || enemyStats.team != _playerCommon.StatController.team))
				{
					enemyStats.ModifyHealth(-_playerCommon.CombatController.WeaponDamage, _playerCommon.gameObject);
					Destroy(gameObject);
				}
			}
		}
	}
}