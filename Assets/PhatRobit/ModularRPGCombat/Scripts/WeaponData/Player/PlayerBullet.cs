﻿using UnityEngine;
using System.Collections;

public class PlayerBullet : MonoBehaviour
{
	public float lifeSpan = 5;

	private PlayerCommon _playerCommon;			// Reference to the player common script

	private Transform _t;
	private Rigidbody _r;

	public PlayerCommon PlayerCommon
	{
		get { return _playerCommon; }
		set { _playerCommon = value; }
	}

	void Start()
	{
		_t = transform;
		_r = GetComponent<Rigidbody>();

		StartCoroutine(DeathTimer());
	}

	void FixedUpdate()
	{
		if(_playerCommon)
		{
			_r.velocity = _t.forward * _playerCommon.CombatController.rangedSpeed;
		}
	}

    void OnTriggerEnter(Collider other)
	{
		if(_playerCommon)
		{
			if(!other.isTrigger && other.CompareTag(Tags.Enemy))
			{
				EnemyStatController enemyStats = other.GetComponent<EnemyStatController>();

				if(enemyStats && enemyStats.Alive && (enemyStats.team == Team.None || enemyStats.team != _playerCommon.StatController.team))
				{
					enemyStats.ModifyHealth(-_playerCommon.CombatController.WeaponDamage, _playerCommon.gameObject);
					Destroy(gameObject);
				}
			}
		}
	}

	private IEnumerator DeathTimer()
	{
		yield return new WaitForSeconds(lifeSpan);
		Destroy(gameObject);
	}
}