﻿using UnityEngine;
using System.Collections;

public class PlayerAoe : MonoBehaviour
{

	public float lifeSpan = 15;								// How long the arrow lasts (in seconds) before being destroyed
	public float damageInterval = 3;						// How often the damage is inflicted within the AOE;
	
	private float _life = 0;								// Timer for AOE before being destroyed
	
	private PlayerCommon _playerCommon;		// Reference to the player's combat information

	private float _timer = 0;

	public PlayerCommon PlayerCommon
	{
		get { return _playerCommon; }
		set { _playerCommon = value; }
	}
	
	void Start()
	{
		// Set up our life timer
		_life = lifeSpan;
	}
	
	void Update()
	{
		// Destory object when life runs out
		if(_life <= 0)
		{
			Destroy(gameObject);
		}
		else
		{
			_life -= Time.deltaTime;
		}
	}

    void OnTriggerStay(Collider other)
	{
		// Deal damage to enemies if colliding with them and destroy gameobject
		if(_playerCommon)
		{
			if(!other.isTrigger && other.CompareTag(Tags.Enemy))
			{
				EnemyStatController enemyStats = other.GetComponent<EnemyStatController>();

				if(enemyStats && enemyStats.Alive && (enemyStats.team == Team.None || enemyStats.team != _playerCommon.StatController.team))
				{
					if(_timer > damageInterval)
					{
						enemyStats.ModifyHealth(-_playerCommon.CombatController.WeaponDamage, _playerCommon.gameObject);
						_timer = 0;
					}
					else
					{
						_timer += Time.deltaTime;
					}
				}
			}
		}
	}
}