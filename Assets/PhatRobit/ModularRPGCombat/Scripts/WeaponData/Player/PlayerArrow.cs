﻿using UnityEngine;
using System.Collections;

public class PlayerArrow : MonoBehaviour
{
	public float forwardForce = 50;							// How much force to be applied to the forward direction of the arrow
	public float upForce = 10;								// How much force to be applied to the up direction of the arrow
	public float lifeSpan = 5;								// How long the arrow lasts (in seconds) before being destroyed

	private float _life = 0;								// Timer for arrow before being destroyed

	private PlayerCommon _playerCommon;		// Reference to the player's combat information

	private Transform _t;									// Reference to gameobject's own transform
	private Rigidbody _r;

	public PlayerCommon PlayerCommon
	{
		get { return _playerCommon; }
		set { _playerCommon = value; }
	}

	void Start()
	{
		_t = transform;
		_r = GetComponent<Rigidbody>();

		// Set up our life timer
		_life = lifeSpan;

		// Apply initial force
		_r.AddForce(forwardForce * _t.forward);
		_r.AddForce(upForce * _t.up);
	}

	void Update()
	{
		// Destory object when life runs out
		if(_life <= 0)
		{
			Destroy(gameObject);
		}
		else
		{
			_life -= Time.deltaTime;
		}
	}

	void FixedUpdate()
	{
		// Rotate the arrow so it is always facing the direction it is moving
		if(_r.velocity != Vector3.zero)
		{
			_r.rotation = Quaternion.LookRotation(_r.velocity);
		}
	}

    void OnTriggerEnter(Collider other)
	{
		// Deal damage to enemies if colliding with them and destroy gameobject
		if(_playerCommon)
		{
			if(!other.isTrigger && other.CompareTag(Tags.Enemy))
			{
				EnemyStatController enemyStats = other.GetComponent<EnemyStatController>();

				if(enemyStats && enemyStats.Alive && (enemyStats.team == Team.None || enemyStats.team != _playerCommon.StatController.team))
				{
					enemyStats.ModifyHealth(-_playerCommon.CombatController.WeaponDamage, _playerCommon.CombatController.gameObject);
					Destroy(gameObject);
				}
			}
		}
	}
}