﻿using UnityEngine;
using System.Collections;

public class EnemyBullet : MonoBehaviour
{
	public float lifeSpan = 5;

	private EnemyCommon _enemyCommon;			// Reference to the enemy common script

	private Transform _t;
	private Rigidbody _r;

	public EnemyCommon EnemyCommon
	{
		get { return _enemyCommon; }
		set { _enemyCommon = value; }
	}

	void Start()
	{
		_t = transform;
		_r = GetComponent<Rigidbody>();

		StartCoroutine(DeathTimer());
	}

	void FixedUpdate()
	{
		if(_enemyCommon)
		{
			if(_enemyCommon.CombatController)
			{
				_r.velocity = _t.forward * _enemyCommon.CombatController.rangedSpeed;
			}
			else if(_enemyCommon.TowerController)
			{
				_r.velocity = _t.forward * _enemyCommon.TowerController.projectileSpeed;
			}
		}
	}

    void OnTriggerEnter(Collider other)
	{
		if(_enemyCommon)
		{
			if(!other.isTrigger && (other.CompareTag(Tags.Enemy) || other.CompareTag(Tags.Player)))
			{
				StatController enemyStats = other.GetComponent<StatController>();

				if(enemyStats && enemyStats != _enemyCommon.StatController && enemyStats.Alive && (enemyStats.team == Team.None || enemyStats.team != _enemyCommon.StatController.team))
				{
					int damage = 0;

					if(_enemyCommon.CombatController)
					{
						damage = _enemyCommon.CombatController.weaponDamage;
					}
					else if(_enemyCommon.TowerController)
					{
						damage = _enemyCommon.TowerController.projectileDamage;
					}

					enemyStats.ModifyHealth(-damage, _enemyCommon.gameObject);
					Destroy(gameObject);
				}
			}
		}
	}

	private IEnumerator DeathTimer()
	{
		yield return new WaitForSeconds(lifeSpan);
		Destroy(gameObject);
	}
}