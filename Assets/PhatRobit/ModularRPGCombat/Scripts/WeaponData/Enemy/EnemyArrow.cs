﻿using UnityEngine;
using System.Collections;

public class EnemyArrow : MonoBehaviour
{
	public float forwardForce = 50;							// How much force to be applied to the forward direction of the arrow
	public float upForce = 10;								// How much force to be applied to the up direction of the arrow
	public float lifeSpan = 5;								// How long the arrow lasts (in seconds) before being destroyed

	private float _life = 0;								// Timer for arrow before being destroyed

	private EnemyCommon _enemyCommon;		// Reference to the player's combat information

	private Transform _t;									// Reference to gameobject's own transform
	private Rigidbody _r;									// Reference to gameobject's own rigidbody

	public EnemyCommon EnemyCommon
	{
		get { return _enemyCommon; }
		set { _enemyCommon = value; }
	}

	void Start()
	{
		_t = transform;
		_r = GetComponent<Rigidbody>();

		// Set up our life timer
		_life = lifeSpan;

		// Apply initial force
		_r.AddForce(forwardForce * _t.forward);
		_r.AddForce(upForce * _t.up);
	}

	void Update()
	{
		// Destory object when life runs out
		if(_life <= 0)
		{
			Destroy(gameObject);
		}
		else
		{
			_life -= Time.deltaTime;
		}
	}

	void FixedUpdate()
	{
		// Rotate the arrow so it is always facing the direction it is moving
		if(_r.velocity != Vector3.zero)
		{
			_r.rotation = Quaternion.LookRotation(_r.velocity);
		}
	}

    void OnTriggerEnter(Collider other)
	{
		// Deal damage to enemies if colliding with them and destroy gameobject
		if(_enemyCommon)
		{
			if(!other.isTrigger && (other.CompareTag(Tags.Enemy) || other.CompareTag(Tags.Player)))
			{
				StatController enemyStats = other.GetComponent<StatController>();

				if(enemyStats && enemyStats != _enemyCommon.StatController && enemyStats.Alive && (enemyStats.team == Team.None || enemyStats.team != _enemyCommon.StatController.team))
				{
					enemyStats.ModifyHealth(-_enemyCommon.CombatController.weaponDamage, _enemyCommon.CombatController.gameObject);
					Destroy(gameObject);
				}
			}
		}
	}
}