﻿using UnityEngine;
using System.Collections;

public class EnemyAoeSpell : MonoBehaviour
{
	public float forwardForce = 50;							// How much force to be applied to the forward direction of the spell
	public float upForce = 10;								// How much force to be applied to the up direction of the spell
	public float lifeSpan = 5;								// How long the spell lasts (in seconds) before being destroyed
	public int manaAmount = 20;								// How much Mana does the spell use?
	public GameObject aoe;									// The AOE Game Object
	
	private float _life = 0;								// Timer for spell before being destroyed

	private EnemyCommon _enemyCommon;						// Reference to the enemy's combat information
	private GameObject _player;								// Reference to the Player.
	
	private Transform _t;									// Reference to gameobject's own transform
	private Rigidbody _r;									// Reference to gameobject's own rigidbody

	public EnemyCommon EnemyCommon
	{
		get { return _enemyCommon; }
		set { _enemyCommon = value; }
	}

	void Start()
	{
		_t = transform;
		_r = GetComponent<Rigidbody>();
		
		// Set up our life timer
		_life = lifeSpan;

		if(_enemyCommon.StatController.Mana < manaAmount)	// Check if player got enough Mana to cast spell
		{
			Destroy(gameObject);
		}
		else
		{
			_enemyCommon.StatController.ModifyMana(-manaAmount); // Retract Mana
		}
		
		// Apply initial force
		_r.AddForce(forwardForce * _t.forward);
		_r.AddForce(upForce * _t.up);
	}
	
	void Update()
	{
		// Destory object when life runs out
		if(_life <= 0)
		{
			Destroy(gameObject);
		}
		else
		{
			_life -= Time.deltaTime;
		}
	}
	
	void FixedUpdate()
	{
		// Rotate the spell so it is always facing the direction it is moving
		if(_r.velocity != Vector3.zero)
		{
			_r.rotation = Quaternion.LookRotation(_r.velocity);
		}
	}

    void OnTriggerEnter(Collider other)
	{
		// Deal damage to enemies if colliding with them and destroy gameobject
		if(_enemyCommon)
		{
			if(!other.isTrigger && other.CompareTag("Terrain"))
			{
				GameObject go = (GameObject)Instantiate(aoe, gameObject.transform.position, Quaternion.identity);

                if(Settings.Get().fullCheapMode)
                    Destroy(go.transform.FindChild("Fire1").gameObject);
                EnemyAoe goAOE = go.GetComponent<EnemyAoe>();
				
				if(goAOE)
				{
					goAOE.EnemyCommon = _enemyCommon;
				}
			}
		}
	}
}
