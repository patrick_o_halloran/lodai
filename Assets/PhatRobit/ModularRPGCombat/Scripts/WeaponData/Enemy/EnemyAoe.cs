﻿using UnityEngine;
using System.Collections;

public class EnemyAoe : MonoBehaviour
{

	public float lifeSpan = 15;								// How long the arrow lasts (in seconds) before being destroyed
	public float damageInterval = 3;						// How often the damage is inflicted within the AOE;
	
	private float _life = 0;								// Timer for AOE before being destroyed
	
	private EnemyCommon _enemyCommon;		// Reference to the enemy's combat information

	private float _timer = 0;

	public EnemyCommon EnemyCommon
	{
		get { return _enemyCommon; }
		set { _enemyCommon = value; }
	}
	
	void Start()
	{
		// Set up our life timer
		_life = lifeSpan;
	}
	
	void Update()
	{
		// Destory object when life runs out
		if(_life <= 0)
		{
			Destroy(gameObject);
		}
		else
		{
			_life -= Time.deltaTime;
		}
	}

    void OnTriggerStay(Collider other)
	{
		// Deal damage to enemies if colliding with them and destroy gameobject
		if(_enemyCommon)
		{
			if(!other.isTrigger && (other.CompareTag(Tags.Enemy) || other.CompareTag(Tags.Player)))
			{
				StatController enemyStats = other.GetComponent<StatController>();

				if(enemyStats && enemyStats != _enemyCommon.StatController && enemyStats.Alive && (enemyStats.team == Team.None || enemyStats.team != _enemyCommon.StatController.team))
				{
					if(_timer > damageInterval)
					{
						enemyStats.ModifyHealth(-_enemyCommon.CombatController.weaponDamage, _enemyCommon.gameObject);
						_timer = 0;
					}
					else
					{
						_timer += Time.deltaTime;
					}
				}
			}
		}
	}
}