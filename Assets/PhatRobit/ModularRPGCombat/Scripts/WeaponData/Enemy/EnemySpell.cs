﻿using UnityEngine;
using System.Collections;

public class EnemySpell : MonoBehaviour
{
	public float lifeSpan = 5;
	public int manaAmount = 10;								// How much Mana does the spell use?
	
	private float _life = 0;
	private EnemyCommon _enemyCommon;						// Reference to the enemy common which is used to get weapon info
	private GameObject _player;								// Reference to the Player.
	
	private Transform _t;
	private Rigidbody _r;

	public EnemyCommon EnemyCommon
	{
		get { return _enemyCommon; }
		set { _enemyCommon = value; }
	}

	void Start()
	{
		_t = transform;
		_r = GetComponent<Rigidbody>();
		_life = lifeSpan;

		if(_enemyCommon.StatController.Mana < manaAmount)	// Check if player got enough Mana to cast spell
		{
			Destroy(gameObject);
		}
		else
		{
			_enemyCommon.StatController.ModifyMana(-manaAmount); // Use Mana
		}
	}
	
	void Update()
	{
		if(_life <= 0)
		{
			Destroy(gameObject);
		}
		else
		{
			_life -= Time.deltaTime;
		}
	}
	
	void FixedUpdate()
	{
		if(_enemyCommon)
		{
			_r.velocity = _t.forward * _enemyCommon.CombatController.spellSpeed;
		}
	}

    void OnTriggerEnter(Collider other)
	{
		if(_enemyCommon)
		{
			if(!other.isTrigger && (other.CompareTag(Tags.Enemy) || other.CompareTag(Tags.Player)))
			{
				StatController enemyStats = other.GetComponent<StatController>();

				if(enemyStats && enemyStats != _enemyCommon.StatController && enemyStats.Alive && (enemyStats.team == Team.None || enemyStats.team != _enemyCommon.StatController.team))
				{
					enemyStats.ModifyHealth(-_enemyCommon.CombatController.weaponDamage, _enemyCommon.gameObject);
					Destroy(gameObject);
				}
			}
		}
	}
}