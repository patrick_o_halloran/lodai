﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(NavMesh))]
public class EnemyMovementController : EnemyCommon
{
	public EnemyLogicStyle logicStyle = EnemyLogicStyle.Idle;
	public bool patrolBacktrack = false;		// Should the enemy backtrack his patrol path once completed or not?
	public float speed = 4;
	public float stoppingDistance = 0.5f;
	public bool returnEnabled = true;			// Whether or not the enemy returns to the point of engagement after targeting something
	public float returnSpeed = 8;
	public float waitTime = 1f;					// How long does the enemy wait once reaching his target?
	public Transform[] patrolWayPoints;			// What are the enemy's patrol points?
	public float roamRadius = 5f;			// If Free Roaming, how far away from his origin position can he move?

	/*COMBATKITEDIT*/public float _waitTimer;					// A wait timer used to see when it's time to move again
	/*COMBATKITEDIT*/public bool _freeRoamDone = true;			// A bool used to set if enemy has reached his free roam target.
	//COMBATKITEDIT//private Vector3 _startPosition;				// This is the start position of the enemy
	private int _currentWaypoint = 0;			// Which waypoint is the enemy at right now?
	private bool _backingUp = false;			// Is the enemy currently backtracking?
	private bool _returning = false;
	private Locomotion _locomotion;

    public bool Returning
	{
		get { return _returning; }
		set { _returning = value; }
	}

	void Start()
	{
        //if(!LODAISettings.Get().fullCheapMode)//COMBATKITEDIT
		    _locomotion = new Locomotion(_animator);

        //COMBATKITEDIT //Reset();

        //COMBATKITEDIT
        Vector3 vel = GetComponent<NavMeshAgent>().velocity.normalized;
        if(vel != Vector3.zero)
            transform.forward = vel;
    }

	void Update()
	{
		if(_navAgent.enabled)
		{
			if(!_targetController.Target)
			{
				_navAgent.speed = speed;
				_navAgent.stoppingDistance = stoppingDistance;

				if(_returning)
				{
					_navAgent.speed = returnSpeed;
					_returning = returnEnabled;

					if(AgentDone())
					{
						_returning = false;
					}

					_navAgent.destination = _targetController.ReturnTarget;
				}
				else
				{
					switch(logicStyle)
					{
						case EnemyLogicStyle.Idle:
							break;
						case EnemyLogicStyle.FreeRoam:
							FreeRoam();
							break;
						case EnemyLogicStyle.Patrol:
							Patrol();
							break;
					}
				}
			}

            SetupAgentLocomotion();
		}
	}

	public void Reset()
	{
		_currentWaypoint = 0;
		_navAgent.destination = _t.position;
		_returning = false;
	}

	private void Patrol()
	{
		if(patrolWayPoints.Length > 0)
		{
			// If near the next waypoint or there is no destination...
			if(AgentDone())
			{
				// ... increment the timer.
				_waitTimer += Time.deltaTime;

				// If the timer exceeds the wait time...
				if(_waitTimer >= waitTime)
				{
					// Reset the timer.
					_waitTimer = 0;

					// Increment / Decrement current waypoint depending on patrol direction
					if(_backingUp)
					{
						_currentWaypoint--;
					}
					else
					{
						_currentWaypoint++;
					}

					// Done backtracking
					if(_currentWaypoint < 0)
					{
						_backingUp = false;
						_currentWaypoint = 1;
					}

					// Restart at first waypoint, or start backtracking
					if(_currentWaypoint >= patrolWayPoints.Length)
					{
						if(patrolBacktrack)
						{
							_backingUp = true;
							_currentWaypoint -= 2;
						}
						else
						{
							_currentWaypoint = 0;
						}
					}
				}
			}
			else
			{
				// If not near a destination, reset the timer.
				_waitTimer = 0;
			}

			// Set the destination to the patrolWayPoint.
			_navAgent.destination = patrolWayPoints[_currentWaypoint].position;
		}
	}

	private void FreeRoam()
	{
		if(_freeRoamDone == true && (_waitTimer >= waitTime))
		{	// Move the enemy towards the next random position within the radius range you have specified.
			Vector3 randomDirection = Random.insideUnitSphere * roamRadius;
			randomDirection += transform.position; //COMBATKITEDIT
			NavMeshHit hit;
			NavMesh.SamplePosition(randomDirection, out hit, roamRadius, 1);
			Vector3 finalPosition = hit.position;
			_navAgent.destination = finalPosition;
			_freeRoamDone = false;
			_waitTimer = 0;
		}
		else
		{	// This is what's used to calculate if the enemy has reached his destination point. If he has - move again once the waiting time has been reached.
			float dist = _navAgent.remainingDistance;
			if(dist != Mathf.Infinity && AgentDone())
			{
				_waitTimer += Time.deltaTime;
				_freeRoamDone = true;
			}
		}
	}

	private void SetupAgentLocomotion()
	{
        #region COMBATKITEDIT
        //Moved out of else
        Vector3 velocity = Quaternion.Inverse(transform.rotation) * _navAgent.desiredVelocity;
        float angle = Mathf.Atan2(velocity.x, velocity.z) * 180.0f / 3.14159f;

        if (_targetController.Target && _targetController.TargetInRange())
        {
            angle = FindAngle(_targetController.Target.position);
        }
        #endregion

        if (AgentDone())
        {
            _locomotion.Do(0, angle); //COMBATKITEDIT
        }
        else
        {
            float speed = _navAgent.desiredVelocity.magnitude;
            _locomotion.Do(speed, angle);
        }
    }

    void OnAnimatorMove()
	{
		_navAgent.velocity = _animator.deltaPosition / Time.deltaTime;
		_t.rotation = _animator.rootRotation;
	}

    public bool AgentDone()
    {
        return !_navAgent.pathPending && AgentStopping();
    }

    protected bool AgentStopping()
    {
        return _navAgent.remainingDistance <= _navAgent.stoppingDistance;
    }

	private float FindAngle(Vector3 target)
	{
		Vector3 forwardA = _t.forward;
		Vector3 forwardB = target - _t.position;

		float angleA = Mathf.Atan2(forwardA.x, forwardA.z) * Mathf.Rad2Deg;
		float angleB = Mathf.Atan2(forwardB.x, forwardB.z) * Mathf.Rad2Deg;

		return Mathf.DeltaAngle(angleA, angleB);
	}
}