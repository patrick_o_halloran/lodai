﻿using UnityEngine;
using System.Collections;

public class EnemyCommon : MonoBehaviour
{
    //COMBATKITEDIT: public
    [HideInInspector]
    public EnemyCombatController _combatController;
    [HideInInspector]
    public EnemyMovementController _movementController;   
    [HideInInspector]
    public AudioController _audioController;
    [HideInInspector]
    public Animator _animator;

    protected EnemyStatController _statController;
    protected EnemyTargetController _targetController;
    protected EnemyTowerController _towerController;
    protected NavMeshAgent _navAgent;
	protected Transform _t;

	public EnemyCombatController CombatController { get { return _combatController; } }
	public EnemyMovementController MovementController { get { return _movementController; } }
	public EnemyStatController StatController { get { return _statController; } }
	public EnemyTargetController TargetController { get { return _targetController; } }
	public EnemyTowerController TowerController { get { return _towerController; } }
	public AudioController AudioController { get { return _audioController; } }

	public Animator Animator { get { return _animator; } }
	public NavMeshAgent NavAgent { get { return _navAgent; } }

	public virtual void Awake()
	{
		_combatController = GetComponent<EnemyCombatController>();
		_movementController = GetComponent<EnemyMovementController>();
		_statController = GetComponent<EnemyStatController>();
		_targetController = GetComponent<EnemyTargetController>();
		_towerController = GetComponent<EnemyTowerController>();
		_audioController = GetComponent<AudioController>();

		_animator = GetComponent<Animator>();
		_navAgent = GetComponent<NavMeshAgent>();

		// Making sure the rotation is controlled by Mecanim.
		_navAgent.updateRotation = false;

		_t = transform;
	}
}