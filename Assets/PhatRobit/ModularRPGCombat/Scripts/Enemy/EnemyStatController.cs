﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class EnemyStatController : StatController
{
    [HideInInspector]
	public bool _dead = false; //COMBATKITEDIT

	private List<StatController> _xpList = new List<StatController>();

    private EnemyCommon _common;

	public override void Start()
	{
		base.Start();

		_common = GetComponent<EnemyCommon>();
	}

	public override void Update()
	{
		base.Update();

		if(!Alive && !_dead)
		{
			_dead = true;

            if (!Settings.Get().fullCheapMode) //COMBATKITEDIT
                _common.AudioController.Play("Death");

			foreach(StatController stat in _xpList)
			{
				stat.AddXp(level);
			}

			_xpList.Clear();

			_common.TargetController.DropTarget();

			if(_common.MovementController)
			{
				_common.MovementController.Reset();
			}

            //COMBATKITEDIT
            //if(respawnTime > 0)
            //{
            //	Respawn();
            //	_common.NavAgent.enabled = false;
            //}
            //else
            //{
            //  // Destory gameObject in 3 seconds
            //  //Destroy(gameObject, 3); //COMBATKITEDIT
            //}

            GetComponent<NPC>().ShutDown();//COMBATKITEDIT
        }

        #region COMBATKITEDIT
        //else if (Alive)
		//{
			//if(_dead)
			//{
			//	_dead = false;
			//	_common.NavAgent.enabled = true;
			//}

            
            //if (!_common.TargetController.Target)
			//{
                
                //if(_health < _maxHealth)
                //{

                //_health++;

                // }
                
            //}
           
        //}
        #endregion
    }

    public override void ModifyHealth(int amount, GameObject go)
	{
        // Decrement this GameObject's health by amount inflected by the other GameObject.
        #region COMBATKITEDIT
        //if(!_common.MovementController || !_common.MovementController.Returning)
        //{
        #endregion

        base.ModifyHealth(amount, go);

		if(go)
		{
			StatController stats = go.GetComponent<StatController>();

			if(stats && !_xpList.Contains(stats))
			{
				_xpList.Add(stats);
			}

            try {

                if (Alive && !_common.TargetController.Target)
                {
                    _common.TargetController.Target = go.transform;
                    _common.TargetController.TargetStats = go.GetComponent<StatController>();
                    _common.TargetController.ReturnTarget = transform.position;
                }
            }
            catch
            {
                Debug.Log("Exception caught");
            }
		}

        #region COMBATKITEDIT
        //}
        //else
        //{
        //	Debug.Log(gameObject.tag + " is returning to position before engaging and is immortal.");
        //}
        #endregion
    }
}