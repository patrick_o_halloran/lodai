﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class EnemyTowerController : EnemyCommon
{
	public float attackDelay = 5;

	public bool aimAtTarget = true;
	public bool smoothAim = true;
	public float rotationSpeed = 5;

	public Transform turret;
	public GameObject projectile;

	public float projectileSpeed = 30;
	public int projectileDamage = 15;

	public Vector3 targetOffset = new Vector3();

	private bool _ready = true;

	void Update()
	{
		if(_statController.Alive)
		{
			if(_targetController.Target)
			{
				if(turret)
				{
					if(aimAtTarget)
					{
						if(smoothAim)
						{
							Quaternion rotation = Quaternion.LookRotation(_targetController.Target.position + targetOffset - turret.position);
							turret.rotation = Quaternion.Slerp(turret.rotation, rotation, Time.deltaTime * rotationSpeed);
						}
						else
						{
							turret.LookAt(_targetController.Target.position + targetOffset);
						}
					}
				}

				if(_ready)
				{
					Attack();
					StartCoroutine(Cooldown());
				}

				if(Vector3.Distance(_t.position, _targetController.Target.position) > _targetController.aggroRange)
				{
					_targetController.DropTarget();
				}
			}
		}
	}

	private void Attack()
	{
		GameObject go = (GameObject)Instantiate(projectile, turret.position, turret.rotation);

		EnemyBullet bullet = go.GetComponent<EnemyBullet>();

		if(bullet)
		{
			bullet.EnemyCommon = this;
		}

		_audioController.Play("Attack");
	}

	private IEnumerator Cooldown()
	{
		_ready = false;
		yield return new WaitForSeconds(attackDelay);
		_ready = true;
	}
}