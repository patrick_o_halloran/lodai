﻿using UnityEngine;
using System.Collections;

public class EnemyTargetController : EnemyCommon
{
	public bool aggressive = false;
	public AggroDetection aggroDetection = AggroDetection.Rays;

	public float fieldOfView = 120;
	public int rays = 12;
	public float aggroRange = 10;

	public bool debugRaycasts = true;

	private Quaternion _startingAngle;
	private Quaternion _stepAngle;

    public Transform _target; //COMBATKITEDIT
    private StatController _targetStats;

	private Vector3 _returnTarget = new Vector3();

    public Transform Target
	{
		get { return _target; }
		set { _target = value; }
	}

	public StatController TargetStats
	{
		get { return _targetStats; }
		set { _targetStats = value; }
	}

	public Vector3 ReturnTarget
	{
		get { return _returnTarget; }
		set { _returnTarget = value; }
	}

	void Start()
	{
		_startingAngle = Quaternion.AngleAxis(-fieldOfView / 2f, Vector3.up);
		_stepAngle = Quaternion.AngleAxis(fieldOfView / (float)rays, Vector3.up);
    }

	void Update()
	{
		if(_statController.Alive)
		{
			if(_target)
			{
                if (!_targetStats.Alive)
				{
					DropTarget();
				}
			}
			else if(aggroDetection == AggroDetection.Rays && aggressive && (!_movementController || !_movementController.Returning))
			{
				RayDetection();
			}
		}
    }

    public bool TargetInRange()
	{
		return Vector3.Distance(_t.position, _target.position) < _combatController.weaponRange;
	}

	public void DropTarget()
	{
		_target = null;
		_targetStats = null;

		if(_movementController)
		{
			_movementController.Returning = true;
		}
	}

	private void RayDetection()
	{
		RaycastHit hit;
		Quaternion angle = _t.rotation * _startingAngle;

		Vector3 direction = angle * Vector3.forward;

		for(int i = 0; i < rays; i++)
		{
			if(debugRaycasts)
			{
				Debug.DrawRay(_t.position + new Vector3(0, 1, 0), direction * aggroRange);
			}

            if (Physics.Raycast(_t.position + new Vector3(0, 1, 0), direction, out hit, aggroRange))
            {
                _targetStats = hit.collider.GetComponent<StatController>();

                if (_targetStats && _targetStats.Alive)
                {
                    if (_targetStats.team == Team.None || _targetStats.team != _statController.team)
                    {
                        _target = hit.transform;
                        _returnTarget = _t.position;

                        break;
                    }
                }
            }

            direction = _stepAngle * direction;
		}
	}

	private void TriggerDetection(Collider other)
	{
		_targetStats = other.GetComponent<StatController>();

		if(_targetStats && _targetStats.Alive)
		{
			if(_targetStats.team == Team.None || _targetStats.team != _statController.team)
			{
				_target = other.transform;
				_returnTarget = _t.position;
            }
		}
	}

	void OnTriggerEnter(Collider other)
	{
		if(other.CompareTag(Tags.Enemy) &&
            !_target && aggressive && 
            aggroDetection == AggroDetection.Collider
            && enabled) //COMBITKITEDIT
		{
			TriggerDetection(other);
		}
    }

    void OnTriggerStay(Collider other)
	{
        //UnityEngine.Profiler.BeginSample("OnTriggerStay_EnemyTargetController");
        if (other.CompareTag(Tags.Enemy) &&
            !_target && aggressive && aggroDetection == AggroDetection.Collider
            && enabled) //COMBITKITEDIT
        {
			TriggerDetection(other);
		}
        //UnityEngine.Profiler.EndSample();
    }
}