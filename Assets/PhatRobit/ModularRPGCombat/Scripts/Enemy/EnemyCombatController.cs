﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(Animator))]
[RequireComponent(typeof(Rigidbody))]
[RequireComponent(typeof(CapsuleCollider))]
[RequireComponent(typeof(NavMeshAgent))]
//[RequireComponent(typeof(AudioSource))] //COMBATKITEDIT
//[RequireComponent(typeof(LineRenderer))] //COMBATKITEDIT
public class EnemyCombatController : EnemyCommon
{
	public float chaseSpeed = 4;
	public float chaseWaitTime = 5f;						// The amount of time to wait when the last sighting is reached.
	public float chaseStoppingDistance = 2;
	public float chaseRange = 50;							// How far the enemy will chase the player before disengaging

	public Transform weaponMount;							// Where the weapon is mounted for the enemy
	public WeaponType weaponStyle = WeaponType.Melee; 		// What combat style the enemy has
	public RangedType rangedStyle = RangedType.Gun;
	public float weaponRange = 3;							// The attacking range of the enemy
	public bool laserGun;									// If the gun should be a lasergun or not
	public float bulletFadeTime = 0.1f;						// Fading time for the bullets
	public float bulletRange = 100;							// The range the bullets can reach
	public Color bulletTrailColor = Color.green;			// Color of the bullets
	public float rangedSpeed = 30;							// How fast the bullet flies
	public float spellSpeed = 5;

	public int weaponDamage = 16;
	public int weaponSpeed = 4;

	public GameObject swordModel;							// Sword prefab
	public GameObject gunModel;								// Gun prefab
	public GameObject bowModel;								// Bow prefab
	public Material swordMaterial;							// Material for the sword
	public Material gunMaterial;							// Material for the gun
	public Material bowMaterial;							// Material for the bow

	public GameObject bullet;								// The bullet prefab used by gun
	public GameObject spell;								// The spell prefab used
	public GameObject aoeSpell;								// The area of effect spell prefab used
	public GameObject arrow;								// The arrow prefab used by bow

	public float fieldOfViewAngle = 160;					// Number of degrees, centred on forward, for the enemy see.

	private GameObject _sword;								// Reference to currently equipped sword object
	private GameObject _gun;								// Reference to currently equipped gun object
	private GameObject _bow;								// Reference to currently equipped bow object

	private float _weaponCD = 0;							// Amount of time left before able to attack again

	private bool _attacked = false;							// A check to see if we have attacked after pressing the attack key

	private WeaponData _weaponData;							// Class that holds our weapon information (type, speed, damage)
	private LineRenderer _bulletTrail;						// Reference to the LineRenderer for the bullet trail

	private AnimatorStateInfo _state;						// Animation state information used to check if we are in the attack animation states

	// Hash IDs for attack animations
	private int _meleeID = 0;
	private int _rangedID = 0;

	private float _chaseTimer;								// A timer for the chaseWaitTime.

	void Start()
	{
		_bulletTrail = GetComponent<LineRenderer>();

		_meleeID = Animator.StringToHash("Base Layer.Locomotion.MeleeAttack");
		_rangedID = Animator.StringToHash("Base Layer.Locomotion.RangedAttack");

		// We initialize our weapon data and give it some parameters for our initial weapon
		EquipWeapon(weaponStyle, weaponDamage, weaponSpeed, rangedStyle);
	}

	void Update()
	{
		if(_statController.Alive)
		{
            _state = _animator.GetCurrentAnimatorStateInfo(0);

			// While engaged, chase the player!
			if(_targetController.Target)
			{
				Chasing();
			}

			// If we are attacking but have not attacked (in code) yet, attack!
			if(Attacking() && !_attacked)
			{
				if(_weaponData.WeaponType == WeaponType.Ranged && Shooting() ||
				_weaponData.WeaponType == WeaponType.Melee)
				{
					_attacked = true;
					Attack();
				}
			}

			// Done attacking
			else if(!Attacking() && _attacked)
			{
				_attacked = false;
			}
		}
		else
		{
			_attacked = false;
		}

		// Fade the bullet trail out
		if(_bulletTrail)
		{
			_bulletTrail.material.color = Color.Lerp(_bulletTrail.material.color, Color.clear, Time.deltaTime / bulletFadeTime);
		}
	}

	private void PreAttack()
	{
		Vector3 direction = _targetController.Target.position - _t.position;
		float distance = Vector3.Distance(_targetController.Target.position, _t.position);

		if(_weaponCD <= 0)
		{
			if(Vector3.Dot(direction.normalized, _t.forward) > 0)
			{
				_weaponCD = _weaponData.WeaponSpeed;

                if (distance <= weaponRange)
                {
                    if (_weaponData.WeaponType == WeaponType.Melee)
                    {
                        _animator.SetTrigger(CP.MeleeAttack);
                    }
                    else if (_weaponData.WeaponType == WeaponType.Ranged)
                    {
                        _animator.SetTrigger(CP.RangedAttack);

                        if (!Settings.Get().fullCheapMode)//COMBATKITEDIT
                            _audioController.Play("GunReady");
                    }
                }         
			}
		}
		else
		{
			_weaponCD -= Time.deltaTime;
		}
	}

	private void Attack()
	{
		// If we're using a melee weapon, do this!
		if(_weaponData.WeaponType == WeaponType.Melee)
		{
            if (!Settings.Get().fullCheapMode) //COMBATKITEDIT
                _audioController.Play("SwordAttack");

			if(_targetController.TargetStats)
			{
				_targetController.TargetStats.ModifyHealth(-_weaponData.WeaponDamage, gameObject);
			}
		}
		// Otherwise if were using a ranged weapon, do this!
		else if(_weaponData.WeaponType == WeaponType.Ranged)
		{
			if(_weaponData.RangedType == RangedType.Gun)
			{
				if(laserGun)
				{
					Ray ray = new Ray(_t.position + new Vector3(0, 1, 0), _t.forward);
					RaycastHit[] hits = Physics.RaycastAll(ray, bulletRange);

					if(_bulletTrail)
					{
						_bulletTrail.material.color = bulletTrailColor;
						_bulletTrail.SetPosition(0, weaponMount.position);
						_bulletTrail.SetPosition(1, _t.position + (ray.direction * bulletRange));
					}

					foreach(RaycastHit hit in hits)
					{
						if(!hit.collider.isTrigger && hit.transform == _targetController.Target)
						{
							_targetController.TargetStats.ModifyHealth(-_weaponData.WeaponDamage, gameObject);

							if(_bulletTrail)
							{
								_bulletTrail.SetPosition(1, hit.point);
							}
						}
					}
				}
				else
				{
					Bullet();
				}
			}
			else if(_weaponData.RangedType == RangedType.Spell)
			{
				Spell();
			}
			else if(_weaponData.RangedType == RangedType.AOESpell)
			{
				AOESpell();
			}
			else if(_weaponData.RangedType == RangedType.Bow)
			{
				Arrow();
			}

            if (!Settings.Get().fullCheapMode) //COMBATKITEDIT
                _audioController.Play("GunFire");
		}
	}

	private bool Attacking()
	{
		return _state.fullPathHash == _meleeID ||
			   _state.fullPathHash == _rangedID;
	}

	private bool Shooting()
	{
		return _state.fullPathHash == _rangedID;
	}

	// Here is where the weapons are equipped / unequipped
	public void EquipWeapon(WeaponType weaponType, int weaponDamage, int weaponSpeed, RangedType rangedType = RangedType.None)
	{
		_weaponData = new WeaponData(weaponType, weaponDamage, weaponSpeed, rangedType);

		// Remove the gun
		if(_gun)
		{
			Destroy(_gun);
			_gun = null;
		}

		// Remove the bow
		if(_bow)
		{
			Destroy(_bow);
			_bow = null;
		}

		// Remove the sword
		if(_sword)
		{
			Destroy(_sword);
			_sword = null;
		}

		if(weaponType == WeaponType.Melee)
		{
			if(swordModel)
			{
				if(!_sword)
				{
					// Instantiate the sword model and place it on our weapon mount
					_sword = (GameObject)Instantiate(swordModel, Vector3.zero, Quaternion.identity);
					_sword.transform.parent = weaponMount;
					_sword.transform.localPosition = Vector3.zero;
					_sword.transform.localRotation = Quaternion.Euler(180, 0, 0);

					if(swordMaterial)
					{
						_sword.GetComponent<Renderer>().material = swordMaterial;
					}
				}
			}
		}
		else if(weaponType == WeaponType.Ranged)
		{
			if(rangedType == RangedType.Gun)
			{
				if(gunModel)
				{
					if(!_gun)
					{
						// Instantiate the gun model and place it on our weapon mount
						_gun = (GameObject)Instantiate(gunModel, Vector3.zero, Quaternion.identity);
						_gun.transform.parent = weaponMount;
						_gun.transform.localPosition = Vector3.zero;
						_gun.transform.localRotation = Quaternion.Euler(-90, 0, 0);

						if(gunMaterial)
						{
							_gun.GetComponent<Renderer>().material = gunMaterial;
						}
					}
				}
			}
			else if(rangedType == RangedType.Bow)
			{
				if(bowModel)
				{
					if(!_bow)
					{
						// Instantiate the bow model and place it on our weapon mount
						_bow = (GameObject)Instantiate(bowModel, Vector3.zero, Quaternion.identity);
						_bow.transform.parent = weaponMount;
						_bow.transform.localPosition = Vector3.zero;
						_bow.transform.localRotation = Quaternion.identity;

						if(bowMaterial)
						{
							_bow.GetComponent<Renderer>().material = bowMaterial;
						}
					}
				}
			}
		}
	}

	void Chasing()
	{
		// Check to see if enemy is close enough to attack
		if(!_targetController.TargetInRange())
		{
            _navAgent.speed = chaseSpeed;
            _navAgent.stoppingDistance = chaseStoppingDistance;
            _navAgent.destination = _targetController.Target.position;
		}
		else
		{
            _navAgent.destination = _t.position;
			PreAttack();
		}

		if(Vector3.Distance(_t.position, _targetController.ReturnTarget) > chaseRange)
		{
			_targetController.DropTarget();
		}
	}

	private void Bullet()
	{
		if(bullet)
		{
			GameObject go = (GameObject)Instantiate(bullet, weaponMount.position, _t.rotation);

            if (Settings.Get().fullCheapMode) //COMBATKITEDIT
                go.GetComponent<MeshRenderer>().enabled = false;

            EnemyBullet goBullet = go.GetComponent<EnemyBullet>();

            #region COMBATKITEDIT
            if(TargetController.transform
                && TargetController.Target) //object not set to instance fix
                goBullet.transform.forward =
                    (_targetController.Target.position+Vector3.up) - weaponMount.position;
            #endregion

            if (goBullet)
			{
				goBullet.EnemyCommon = this;
			}
		}
	}

	public void Spell()
	{
		if(spell)
		{
			GameObject go = (GameObject)Instantiate(spell, weaponMount.position, _t.rotation);

            if (Settings.Get().fullCheapMode) //COMBATKITEDIT
                go.GetComponent<MeshRenderer>().enabled = false;

            EnemySpell goSpell = go.GetComponent<EnemySpell>();

			if(goSpell)
			{
				goSpell.EnemyCommon = this;
			}
		}
	}

	public void AOESpell()
	{
		if(aoeSpell)
		{
			GameObject go = (GameObject)Instantiate(aoeSpell, weaponMount.position, _t.rotation);

            if(Settings.Get().fullCheapMode) //COMBATKITEDIT
                go.GetComponent<MeshRenderer>().enabled = false; 

			EnemyAoeSpell goAoeSpell = go.GetComponent<EnemyAoeSpell>();

			if(goAoeSpell)
			{
				goAoeSpell.EnemyCommon = this;
			}
		}
	}

	public void Arrow()
	{
		if(arrow)
		{
			GameObject go = (GameObject)Instantiate(arrow, weaponMount.position, _t.rotation);

            if (Settings.Get().fullCheapMode) //COMBATKITEDIT
                go.GetComponent<MeshRenderer>().enabled = false;

            EnemyArrow goArrow = go.GetComponent<EnemyArrow>();

			if(goArrow)
			{
				goArrow.EnemyCommon = this;
			}
		}
	}
}