﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[RequireComponent(typeof(AudioSource))] //COMBATKITEDIT
public class AudioController : MonoBehaviour
{
	public List<Sfx> sfxList = new List<Sfx>();

    //COMBATKITEDIT
    //private AudioSource[] _sources;
    //private int _current = 0; 

    private AudioSource _source; //COMBATKITCOMMENT: This is just a reference
	private Dictionary<string, AudioClip> _sfxClips = new Dictionary<string, AudioClip>();

	void Start()
	{
		_source = GetComponent<AudioSource>();

        #region COMBATKITEDIT
        //foreach(Sfx sfx in sfxList)
        //{
        //	if(sfx.sfxClip)
        //	{
        //		_sfxClips.Add(sfx.sfxName, sfx.sfxClip);
        //	}
        //}
        _sfxClips.Add("Death", Resources.Load("Audio/death") as AudioClip);
        _sfxClips.Add("SwordAttack", Resources.Load("Audio/sword_attack.") as AudioClip);
        _sfxClips.Add("GunReady", Resources.Load<AudioClip>("Audio/gun_ready"));
        _sfxClips.Add("GunFire", Resources.Load<AudioClip>("Audio/gun_shoot"));
        #endregion
    }

	public void Play(string sfxName, bool loop = false)
	{
        if (_source) //COMBATKITEDIT
        {
            if (_sfxClips.ContainsKey(sfxName))
            {
                #region COMBATKITEDIT
                _source.clip = _sfxClips[sfxName];
                _source.loop = loop;
                _source.Play();
                #endregion
            }
            else
            {
                Debug.LogError("[" + gameObject + "] No sfx clip of name '" + sfxName + "' found.");
            }
        }
	}
}