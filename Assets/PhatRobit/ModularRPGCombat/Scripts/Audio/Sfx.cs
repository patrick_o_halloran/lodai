﻿using UnityEngine;
using System.Collections;

[System.Serializable]
public class Sfx
{
	public string sfxName = string.Empty;
	public AudioClip sfxClip;
}