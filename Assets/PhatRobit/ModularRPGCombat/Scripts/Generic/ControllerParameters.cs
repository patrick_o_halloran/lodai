﻿using UnityEngine;
using System.Collections;

public class CP
{
	public static string Speed = "Speed";
	public static string Direction = "Direction";
	public static string Angle = "Angle";
	public static string AngularSpeed = "AngularSpeed";
	public static string Dead = "Dead";
	public static string Respawn = "Respawn";
	public static string MeleeAttack = "MeleeAttack";
	public static string RangedAttack = "RangedAttack";
	public static string SpecialAttack = "SpecialAttack";
    public static string ComboAttack1 = "ComboAttack1";
    public static string ComboAttack2 = "ComboAttack2";
}