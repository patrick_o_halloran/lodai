﻿using UnityEngine;
using System.Collections;

public class StatController : MonoBehaviour
{
	public int level = 1;
	public bool gainXp = true;

    public int healthModifier = 6;
	public int manaModifier = 3;
	public int xpModifier = 8;

	public Team team = Team.None;

	public Renderer objectRenderer;		// The color on this renderer's material will change when this gameObject is damaged
	public Color hurtColor = Color.red;	// This is the color the objectRenderer will change to when hurt
	public float hurtFadeSpeed = 1;		// The speed at which the hurtColor fades away

	public float respawnTime = 3;		// The time it takes to respawn

	public bool regenHealth = true;
	public float healthTimer = 10;		// How much in between health additions (when it will give "healthAmount" to the player).
	public int healthAmount = 5;		// How much health that will given every "healthTimer" seconds.

	public bool regenMana = true;
	public float manaTimer = 10;		// How much in between mana additions (when it will give "manaAmount" to the player).
	public int manaAmount = 10;			// How much mana that will given every "manaTimer" seconds.

	public bool showDebug = false;

	protected bool _respawning = false;

    [HideInInspector]
	public Color _defaultColor = Color.white; //COMBATKITEDIT

    #region COMBATKITEDIT
    public bool _alive = true;          // Is the player alive or dead?
    #endregion

    private Vector3 _spawnPoint;		// Initial spawn point

	protected int _health = 0;
	protected int _maxHealth = 0;
	protected int _mana = 0;
	protected int _maxMana = 0;

	public int _xp = 0; //COMBATKITEDIT
	private int _maxXp = 0;

	protected Transform _t;
	public Animator _animator;			// Reference to the animator component //COMBATKITEDIT

	public bool Alive
	{
		get { return _alive; }
	}

	public int Health
	{
		get { return _health; }
        set { _health = value; } //COMBATKITEDIT
	}

	public int MaxHealth
	{
		get { return _maxHealth; }
	}

	public int Mana
	{
		get { return _mana; }
	}

	public int MaxMana
	{
		get { return _maxMana; }
	}

	public int Xp
	{
		get { return _xp; }
	}

	public int MaxXp
	{
		get { return _maxXp; }
	}

	public virtual void Start()
	{
		_t = transform;
		_animator = GetComponent<Animator>();

		_spawnPoint = _t.position;

		//if(objectRenderer)
		//{
  //          _defaultColor = objectRenderer.materials[1].color; //COMBATKITEDIT
  //      }

		// Set up repeating functions for health and mana regen
		if(regenHealth)
		{
			InvokeRepeating("HealthRegen", healthTimer, healthTimer);
		}

		if(regenMana)
		{
			InvokeRepeating("ManaRegen", manaTimer, manaTimer);
		}

		UpdateStats();
	}

	public virtual void Update()
	{
		if(objectRenderer)
		{
            if (objectRenderer.materials[1].color == _defaultColor)//COMBATKITEDIT
                return;
            // Return material color back to normal
            objectRenderer.materials[1].color = Color.Lerp(objectRenderer.materials[1].color, _defaultColor, Time.deltaTime * hurtFadeSpeed); //COMBATKITEDIT
        }
	}

    public void UpdateStats() //COMBATKITEDIT: Made public
	{
		// Health
		int hpBase = BaseStat(healthModifier);
		_maxHealth = hpBase * 10 + level;
		_health = _maxHealth;

		// Mana
		int mpBase = BaseStat(manaModifier);
		_maxMana = mpBase * 10 + level;
		_mana = _maxMana;

		// Xp
		_maxXp = (xpModifier * level + Diff()) * (45 + 5 * level);

        #region COMBATKITEDIT
        //Grow the size of the NPC based on level
        float sizeGain = 1 + level * Constants.sizeGainPerLevel;
        transform.localScale = new Vector3(sizeGain, sizeGain, sizeGain);

        //..but keep aggro same size
        float radiusGlobal = Constants.aggroRange / transform.localScale.x;
        transform.gameObject.GetComponent<SphereCollider>().radius = radiusGlobal;
        transform.gameObject.GetComponent<NPC>().encounterSphereVis.transform.localScale 
            = new Vector3(radiusGlobal*2, radiusGlobal*2, radiusGlobal*2);
        #endregion
    }

	private int Diff()
	{
		int d = 0;

		if(level == 29)
		{
			d = 1;
		}
		else if(level == 30)
		{
			d = 3;
		}
		else if(level == 31)
		{
			d = 6;
		}
		else if(level >= 32)
		{
			d = 5 * (level - 30);
		}

		return d;
	}

	private int BaseStat(int bonus)
	{
		return bonus + 8 + level + (bonus * (level - 1));
	}

	private void HealthRegen()
	{
		if(Alive)
		{
			ModifyHealth(healthAmount);
		}
	}

	private void ManaRegen()
	{
		if(Alive)
		{
			ModifyMana(manaAmount);
		}
	}

	public virtual void ModifyHealth(int amount, GameObject go = null)
	{
		_health = Mathf.Clamp(_health + amount, 0, _maxHealth);

		if(_alive && _health <= 0)
		{
			_alive = false;

			if(_animator)
			{
				_animator.SetTrigger(CP.Dead);
			}
            else //COMBATKITEDIT
            {
                Debug.Log("Missing animator");
            }
		}

		if(objectRenderer && amount < 0)
		{
			objectRenderer.materials[1].color = hurtColor; //COMBATKITEDIT
		}

		if(showDebug)
		{
			Debug.Log("[" + gameObject.tag + "] " + amount + " HP (" + _health + "/" + _maxHealth + ")");
		}
	}

	public void ModifyMana(int amount)
	{
		_mana = Mathf.Clamp(_mana + amount, 0, _maxMana);

		if(showDebug)
		{
			Debug.Log("[" + gameObject.tag + "] " + amount + " MP (" + _mana + "/" + _maxMana + ")");
		}
	}

	public void Respawn()
	{
		if(!_respawning)
		{
			_respawning = true;
			StartCoroutine(Respawner());
		}
	}

	private IEnumerator Respawner()
	{
		yield return new WaitForSeconds(respawnTime);
		_respawning = false;
		_health = _maxHealth;
		_mana = _maxMana;
		_alive = true;

		if(_animator)
		{
			_animator.SetTrigger(CP.Respawn);
		}

		_t.position = _spawnPoint;
	}

	public void AddXp(int enemyLevel)
	{
		if(gainXp && Alive)
		{
			int amount = 0;
			int zd = Mathf.Clamp(level - 3, 5, 99);

			if(level < 25)
			{
				amount = level * 5 + 45;
			}
			else if(level < 50)
			{
				amount = level * 5 + 235;
			}
			else if(level < 99)
			{
				amount = level * 5 + 580;
			}

			if(level < enemyLevel)
			{
				amount = Mathf.RoundToInt(amount * (1 + 0.05f * (enemyLevel - level)));
			}
			else if(level > enemyLevel)
			{
				amount = Mathf.RoundToInt(amount * (1 - (level - enemyLevel) / zd));
			}

			_xp += amount;

            #region COMBATKITEDIT
            if(GetComponent<NPC>().LOD == LOD.Full
                && DatasetManager.Get().sampling)
                GetComponent<Sampler>().xpGain += amount;
            #endregion

            if(showDebug)
			{
				Debug.Log(gameObject.tag + ": [EXP] " + amount + " (" + _xp + "/" + _maxXp + ")");
			}

			if(_xp >= _maxXp)
			{
				_xp -= _maxXp;
				LevelUp();
            }
		}
	}

    public virtual void LevelUp() //COMBATKITEDIT
	{
		level++;
		UpdateStats();
	}
}