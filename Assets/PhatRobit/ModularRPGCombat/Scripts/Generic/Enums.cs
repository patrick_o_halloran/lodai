﻿public enum Team
{
	None,
	Red,
	Blue,
	Green,
	Yellow,
	Orange,
	Pink,
	Black
}

public enum EnemyLogicStyle
{
	Idle,
	FreeRoam,
	Patrol
}

public enum WeaponType
{
	Melee,
	Ranged
}

public enum RangedType
{
	None,
	Gun,
	Bow,
	Spell,
	AOESpell
}

public enum AggroDetection
{
	Rays,
	Collider
}