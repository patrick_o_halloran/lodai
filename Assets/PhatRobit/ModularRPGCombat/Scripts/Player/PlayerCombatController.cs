﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(Animator))]
[RequireComponent(typeof(LineRenderer))]
public class PlayerCombatController : PlayerCommon
{
	public Transform weaponMount;				// This is where the character's weapon will appear when equipped
	public float meleeRange = 3;				// Max range of melee attacks
	public float meleeComboAttackTime = 1;			// If you hit an enemy, and then press attack again within this limit, you will do a special attack
	public float bulletFadeTime = 0.1f;			// Amount of time it takes until the bullet trail is no longer visible
	public float bulletRange = 100;				// Range of your bullets
	public Color bulletTrailColor = Color.cyan;	// The color of the bullet trail
	public int meleeDamage = 17;				// Melee weapon damage
	public float meleeWaitTime = 1;				// Melee wait time (time between attacks)
    public float meleeTimeBeforeDamage = 0;     // How long does it take before damage is dealt?
	public int rangedDamage = 35;				// Ranged weapon damage
	public float rangedWaitTime = 1;				// Ranged wait time (time between attacks)
	public float rangedSpeed = 30;				// How fast the bullet flies
	public float spellSpeed = 5;
    public float comboTimer = 3;                // How long does the player have to initiate combos?

	public KeyCode attackKey = KeyCode.Space;	// Attack key
	public KeyCode specialAttackKey = KeyCode.LeftShift;	// Special Attack key
	public KeyCode swordKey = KeyCode.Alpha1;	// Equip sword key
	public KeyCode gunKey = KeyCode.Alpha2;		// Equip gun key
	public KeyCode bowKey = KeyCode.Alpha3;		// Equip bow key
	public KeyCode spellKey = KeyCode.Alpha4;	// Equip spell key
	public KeyCode aoeSpellKey = KeyCode.Alpha5;		// Equip Area Of Effect spell key
	public bool laserSight;						// Laser Sight for gun.
	public Color laserSightColor = Color.red;	// The color of the laser sight
	public bool laserGun;						// If the gun should be a lasergun or not

	public GameObject swordModel;				// Model to be used for the sword
	public GameObject gunModel;					// Model to be used for the gun
	public GameObject bowModel;					// Model to be used for the bow
	public Material swordMaterial;				// Material for the sword
	public Material gunMaterial;				// Material for the gun
	public Material bowMaterial;				// Material for the bow

	public MeleeHitbox meleeHitbox;				// The hitbox for melee attacks
	public GameObject bullet;					// The bullet prefab used by gun
	public GameObject spell;					// The spell prefab used
	public GameObject aoeSpell;					// The area of effect spell prefab used
	public GameObject arrow;					// The arrow prefab used by bow

	private GameObject _sword;					// Reference to currently equipped sword object
	private GameObject _gun;					// Reference to currently equipped gun object
	private GameObject _bow;					// Reference to currently equipped bow object

	private float _weaponCD = 0;				// Amount of time left before able to attack again

	private bool _attacked = false;				// A check to see if we have attacked after pressing the attack key
	private bool _meleeCombo = false;

	private WeaponData _weaponData;				// Class that holds our weapon information (type, speed, damage)
	private LineRenderer _bulletTrail;			// Reference to the LineRenderer for the bullet trail

	private AnimatorStateInfo _state;			// Animation state information used to check if we are in the attack animation states

	private Vector3 _targetVerticalLocation;

	// Hash IDs for attack animations
	private int _meleeID = 0;
    private int _combo1ID = 0;
    private int _combo2ID = 0;
    private int _specialID = 0;
	private int _rangedID = 0;

    // Used for combo states
    private bool _activateTimerToReset = false;
    private float _comboTimer;
    private int _currentComboState = 0;

    public WeaponData WeaponData
	{
		get { return _weaponData; }
	}

	public int WeaponDamage
	{
		get { return _weaponData.WeaponDamage; }
		set { _weaponData.WeaponDamage = value; }
	}

	public float WeaponSpeed
	{
		get { return _weaponData.WeaponSpeed; }
		set { _weaponData.WeaponSpeed = value; }
	}

	public Vector3 TargetVerticalLocation
	{
		get { return _targetVerticalLocation; }
		set { _targetVerticalLocation = value; }
	}

	void Start()
	{
        _comboTimer = comboTimer;
        _bulletTrail = GetComponent<LineRenderer>();

		_meleeID = Animator.StringToHash("Base Layer.MeleeAttack");
        _combo1ID = Animator.StringToHash("Base Layer.ComboAttack1");
        _combo2ID = Animator.StringToHash("Base Layer.ComboAttack2");
        _specialID = Animator.StringToHash("Base Layer.SpecialAttack");
		_rangedID = Animator.StringToHash("Base Layer.RangedAttack");

		// We initialize our weapon data and give it some parameters for our initial weapon
		EquipWeapon(WeaponType.Melee, meleeDamage, meleeWaitTime);
	}

    void Update()
	{
        ResetComboState(_activateTimerToReset);

        _state = _animator.GetCurrentAnimatorStateInfo(0);

		if(laserSight)
		{
			if(_weaponData.WeaponType == WeaponType.Ranged && _weaponData.RangedType == RangedType.Gun)
			{
				Ray ray = new Ray(_t.position + new Vector3(0, 1, 0), _t.forward);
				RaycastHit[] hits = Physics.RaycastAll(ray, bulletRange);

				if(_bulletTrail)
				{
					_bulletTrail.SetPosition(0, weaponMount.position);
					_bulletTrail.SetPosition(1, _t.position + (ray.direction * bulletRange));
				}

				foreach(RaycastHit hit in hits)
				{
					if(!hit.collider.isTrigger)
					{
						if(_bulletTrail)
						{
							_bulletTrail.SetPosition(1, hit.point);
						}
					}
				}
			}
			else
			{
				_bulletTrail.SetPosition(0, Vector3.zero);
				_bulletTrail.SetPosition(1, Vector3.zero);
			}
		}
		else
		{
			_bulletTrail.SetPosition(0, Vector3.zero);
			_bulletTrail.SetPosition(1, Vector3.zero);
		}

		if(_weaponCD <= 0 && !Attacking() && _statController.Alive)
		{
			// When keys are pressed, equip a weapon
			if(Input.GetKeyDown(swordKey))
			{
				EquipWeapon(WeaponType.Melee, meleeDamage, meleeWaitTime);
			}

			if(Input.GetKeyDown(gunKey))
			{
				EquipWeapon(WeaponType.Ranged, rangedDamage, rangedWaitTime, RangedType.Gun);
			}

			if(Input.GetKeyDown(spellKey))
			{
				EquipWeapon(WeaponType.Ranged, rangedDamage, rangedWaitTime, RangedType.Spell);
			}

			if(Input.GetKeyDown(aoeSpellKey))
			{
				EquipWeapon(WeaponType.Ranged, rangedDamage, rangedWaitTime, RangedType.AOESpell);
			}

			if(Input.GetKeyDown(bowKey))
			{
				EquipWeapon(WeaponType.Ranged, rangedDamage, rangedWaitTime, RangedType.Bow);
			}

			// Attack!
			if(Input.GetKeyDown(attackKey))
			{
				_attacked = false;
				_weaponCD = _weaponData.WeaponSpeed;

				_targetController.AutoAim();

				if(_weaponData.WeaponType == WeaponType.Melee)
				{
                    if (_currentComboState == 0)
                    {
                        _currentComboState++;
                        _activateTimerToReset = true;
                        _animator.SetTrigger(CP.MeleeAttack);
                        _meleeCombo = true;
                    }

                    else if (_currentComboState == 1)
                    {
                        _currentComboState++;
                        _animator.SetTrigger(CP.ComboAttack1);
                    }

                    else if (_currentComboState == 2)
                    {
                        _currentComboState++;
                        _animator.SetTrigger(CP.ComboAttack2);
                    }
                }

				else if(_weaponData.WeaponType == WeaponType.Ranged)
				{
					_animator.SetTrigger(CP.RangedAttack);
					_audioController.Play("GunReady");
				}
			}

			// Special Attack!
			if(_weaponData.WeaponType == WeaponType.Melee) // Only allow special attack with melee weapon
			{
				if(Input.GetKeyDown(specialAttackKey))
				{
					_attacked = false;
					_weaponCD = _weaponData.WeaponSpeed;
					_animator.SetTrigger(CP.SpecialAttack);
					
					_targetController.AutoAim();
				}
			}
		}
		else
		{
			_weaponCD -= Time.deltaTime;
		}
        

        // If we are attacking but have not attacked (in code) yet, attack!
        if (Attacking() && !_attacked)
		{
			if(_weaponData.WeaponType == WeaponType.Ranged && Shooting() ||
			_weaponData.WeaponType == WeaponType.Melee)
			{
				_attacked = true;
				StartCoroutine(Attack());
			}
		}

		// Done attacking
		if(!Attacking() && _attacked)
		{
			_attacked = false;
		}

		// Fade the bullet trail out
		if(_bulletTrail)
		{
			_bulletTrail.material.color = Color.Lerp(_bulletTrail.material.color, laserSightColor, Time.deltaTime / bulletFadeTime);
		}
	}

    private void ResetComboState(bool startCountDown)
    {
        if (startCountDown)
        {
            _comboTimer -= Time.deltaTime;
            if (_comboTimer <= 0)
            {
                _currentComboState = 0;
                _activateTimerToReset = false;
                _comboTimer = comboTimer;

                _meleeCombo = false;
            }
        }
    }

	private IEnumerator Attack()
	{
		// If we're using a melee weapon, do this!
		if(_weaponData.WeaponType == WeaponType.Melee)
		{
            yield return new WaitForSeconds(meleeTimeBeforeDamage);
            if (meleeHitbox)
			{
                meleeHitbox.damage = _meleeCombo ? _weaponData.WeaponDamage * 2 : _weaponData.WeaponDamage;
				meleeHitbox.gameObject.SetActive(true);
			}

			_audioController.Play("SwordAttack");
		}
		// Otherwise if were using a ranged weapon, do this!
		else if(_weaponData.WeaponType == WeaponType.Ranged)
		{
			if(_weaponData.RangedType == RangedType.Gun)
			{
				if(laserGun)
				{
					Ray ray = new Ray(_t.position + new Vector3(0, 1, 0), _t.forward);
					RaycastHit[] hits = Physics.RaycastAll(ray, bulletRange);

					if(_bulletTrail)
					{
						_bulletTrail.material.color = bulletTrailColor;
						_bulletTrail.SetPosition(0, weaponMount.position);
						_bulletTrail.SetPosition(1, _t.position + (ray.direction * bulletRange));
					}

					foreach(RaycastHit hit in hits)
					{
						if(!hit.collider.isTrigger)
						{
							EnemyStatController health = hit.collider.GetComponent<EnemyStatController>();

							if(health && (health.team == Team.None || health.team != _statController.team))
							{
								health.ModifyHealth(-_weaponData.WeaponDamage, gameObject);
							}

							if(_bulletTrail)
							{
								_bulletTrail.SetPosition(1, hit.point);
							}
						}
					}
				}
				else
				{
					Bullet();
				}
			}
			else if(_weaponData.RangedType == RangedType.Spell)
			{
				Spell();
			}
			else if(_weaponData.RangedType == RangedType.AOESpell)
			{
				AOESpell();
			}
			else if(_weaponData.RangedType == RangedType.Bow)
			{
				Arrow();
			}

			_audioController.Play("GunFire");
		}
	}

	// Here is where the weapons are equipped / unequipped
	public void EquipWeapon(WeaponType weaponType, int weaponDamage, float weaponSpeed, RangedType rangedType = RangedType.None)
	{
		_weaponData = new WeaponData(weaponType, weaponDamage, weaponSpeed, rangedType);

		// Remove the gun
		if(_gun)
		{
			Destroy(_gun);
			_gun = null;
		}

		// Remove the bow
		if(_bow)
		{
			Destroy(_bow);
			_bow = null;
		}

		// Remove the sword
		if(_sword)
		{
			Destroy(_sword);
			_sword = null;
		}

		if(weaponType == WeaponType.Melee)
		{
			if(swordModel)
			{
				if(!_sword)
				{
					// Instantiate the sword model and place it on our weapon mount
					_sword = (GameObject)Instantiate(swordModel, Vector3.zero, Quaternion.identity);
					_sword.transform.parent = weaponMount;
					_sword.transform.localPosition = Vector3.zero;
					_sword.transform.localRotation = Quaternion.Euler(180, 0, 0);

					if(swordMaterial)
					{
						_sword.GetComponent<Renderer>().material = swordMaterial;
					}
				}
			}
		}
		else if(weaponType == WeaponType.Ranged)
		{
			if(rangedType == RangedType.Gun)
			{
				if(gunModel)
				{
					if(!_gun)
					{
						// Instantiate the gun model and place it on our weapon mount
						_gun = (GameObject)Instantiate(gunModel, Vector3.zero, Quaternion.identity);
						_gun.transform.parent = weaponMount;
						_gun.transform.localPosition = Vector3.zero;
						_gun.transform.localRotation = Quaternion.Euler(-90, 0, 0);

						if(gunMaterial)
						{
							_gun.GetComponent<Renderer>().material = gunMaterial;
						}
					}
				}
			}
			else if(rangedType == RangedType.Bow)
			{
				if(bowModel)
				{
					if(!_bow)
					{
						// Instantiate the bow model and place it on our weapon mount
						_bow = (GameObject)Instantiate(bowModel, Vector3.zero, Quaternion.identity);
						_bow.transform.parent = weaponMount;
						_bow.transform.localPosition = Vector3.zero;
						_bow.transform.localRotation = Quaternion.identity;

						if(bowMaterial)
						{
							_bow.GetComponent<Renderer>().material = bowMaterial;
						}
					}
				}
			}
		}
	}

	// A check to see if we are in the attack animation states / transitions
	private bool Attacking()
	{
		return _state.fullPathHash == _meleeID ||
               _state.fullPathHash == _combo1ID ||
               _state.fullPathHash == _combo2ID ||
               _state.fullPathHash == _specialID ||
			   _state.fullPathHash == _rangedID;
	}

	// A check to see if we are in the shooting animation state / transition
	private bool Shooting()
	{
		return _state.fullPathHash == _rangedID;
	}

	public void Bullet()
	{
		if(bullet)
		{
			GameObject go = (GameObject)Instantiate(bullet, weaponMount.position, _t.rotation);

			PlayerBullet goBullet = go.GetComponent<PlayerBullet>();

			if(goBullet)
			{
				goBullet.PlayerCommon = this;
			}
		}
	}

	public void Spell()
	{
		if(spell)
		{
			GameObject go = (GameObject)Instantiate(spell, weaponMount.position, _t.rotation);

			PlayerSpell goSpell = go.GetComponent<PlayerSpell>();
			
			if(goSpell)
			{
				goSpell.PlayerCommon = this;
			}
		}
	}

	public void AOESpell()
	{
		if(aoeSpell)
		{
			GameObject go = (GameObject)Instantiate(aoeSpell, weaponMount.position, _t.rotation);

			PlayerAoeSpell goAOESpell = go.GetComponent<PlayerAoeSpell>();
			
			if(goAOESpell)
			{
				goAOESpell.PlayerCommon = this;
			}
		}
	}

	public void Arrow()
	{
		if(arrow)
		{
			GameObject go = (GameObject)Instantiate(arrow, weaponMount.position, _t.rotation);

			PlayerArrow goArrow = go.GetComponent<PlayerArrow>();

			if(goArrow)
			{
				goArrow.PlayerCommon = this;
			}
		}
	}
}