﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class PlayerTargetController : PlayerCommon
{
	public bool targetSwitch = false;				// Be able to target switch
	public bool lookAtTarget = false;				// Look at the newly selected target

	public float maxTargetDistance = 25;			// The max distance a target can be from the player
	public float maxTargetHeight = 10;				// The max height a target can be from the player

	public KeyCode targetSwitchKey = KeyCode.Tab;	// Target Switch Key
	public Color targetedEnemyColor = Color.blue;	// The color of the info bar of the selected enemy

	public bool autoAimMelee = false;				// Automatically aim at the closest enemy with melee weapons
	public bool autoAimRanged = false;				// Automatically aim at the closest enemy with ranged weapons
	public float autoAimDistance = 50;				// How far away an enemy can be for auto aim to function
	public bool lookAtTargetVertically = false;

	private Transform _target;
	private List<Transform> _targets = new List<Transform>();
	private int _currentTarget = 0;

	public Transform Target
	{
		get { return _target; }
	}

	void Update()
	{
		if(targetSwitch)
		{
			if(Input.GetKeyDown(targetSwitchKey))
			{
				FindEnemies();
				SelectNextTarget();

				if(lookAtTarget)
				{
					FaceTarget();
				}
			}
		}
	}

	private void SelectNextTarget()
	{
		_currentTarget = _target ? _currentTarget + 1 : 0;

		if(_currentTarget >= _targets.Count)
		{
			_currentTarget = 0;
		}

		DeselectTarget();

		if(_targets.Count > 0)
		{
			_target = _targets[_currentTarget];

			EnemyInfoBar enemyInfoBar = _target.GetComponent<EnemyInfoBar>();

			if(enemyInfoBar)
			{
				enemyInfoBar.BarColor = targetedEnemyColor;
			}
		}
	}

	public void DeselectTarget()
	{
		if(_target)
		{
			EnemyInfoBar currentBar = _target.GetComponent<EnemyInfoBar>();

			if(currentBar)
			{
				currentBar.BarColor = currentBar.defaultBarColor;
			}

			_target = null;
		}
	}

	private void FaceTarget()
	{
		if(_target)
		{
			Vector3 point = _target.position;
			if(!lookAtTargetVertically)
			{
				point.y = _t.position.y;
			}
			_t.LookAt(point);
		}
	}

	private void FindEnemies()
	{
		// Get an array of ALL enemies
		GameObject[] enemies = GameObject.FindGameObjectsWithTag(Tags.Enemy);

		foreach(GameObject enemy in enemies)
		{
			StatController stats = enemy.GetComponent<StatController>();
			Transform enemyTransform = enemy.transform;
			float distance = Vector3.Distance(_t.position, enemyTransform.position);
			float height = Mathf.Abs(_t.position.y - enemyTransform.position.y);

			// If the enemy is in range and alive, add it if we haven't already
			if(distance <= maxTargetDistance && height <= maxTargetHeight && !_targets.Contains(enemyTransform) && stats.Alive)
			{
				_targets.Add(enemyTransform);
			}
			// Otherwise if it isn't in range or is dead and is in our target list, remove it
			else if(_targets.Contains(enemyTransform))
			{
				if(distance > maxTargetDistance || height > maxTargetHeight || !stats.Alive)
				{
					_targets.Remove(enemyTransform);
				}
			}
		}

		SortTargets();
	}

	private void SortTargets()
	{
		// Sort targets by distance
		_targets.Sort(delegate(Transform t1, Transform t2)
		{
			return (Vector3.Distance(t1.position, _t.position).CompareTo(Vector3.Distance(t2.position, _t.position)));
		});
	}

	public void AutoAim()
	{
		// If autoaim is enabled, find the nearest enemy within range and rotate to face it
		if(_combatController.WeaponData.WeaponType == WeaponType.Melee && autoAimMelee ||
		_combatController.WeaponData.WeaponType == WeaponType.Ranged && autoAimRanged)
		{
			FindEnemies();

			if(!_target)
			{
				SelectNextTarget();
			}

			FaceTarget();
		}
	}
}