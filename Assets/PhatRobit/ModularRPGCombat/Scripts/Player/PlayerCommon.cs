﻿using UnityEngine;
using System.Collections;

public class PlayerCommon : MonoBehaviour
{
	protected PlayerCombatController _combatController;
	protected PlayerStatController _statController;
	protected PlayerTargetController _targetController;
	protected AudioController _audioController;

	protected Animator _animator;

	protected Transform _t;

	public PlayerCombatController CombatController { get { return _combatController; } }
	public PlayerStatController StatController { get { return _statController; } }
	public PlayerTargetController TargetController { get { return _targetController; } }
	public AudioController AudioController { get { return _audioController; } }

	public virtual void Awake()
	{
		_combatController = GetComponent<PlayerCombatController>();
		_statController = GetComponent<PlayerStatController>();
		_targetController = GetComponent<PlayerTargetController>();
		_audioController = GetComponent<AudioController>();

		_animator = GetComponent<Animator>();

		_t = transform;
	}
}