﻿using UnityEngine;
using System.Collections;

public class PlayerStatController : StatController
{
	private bool _dead = false;

	private Rect _respawnRect;

	private PlayerCommon _common;

	public override void Start()
	{
		base.Start();

		_respawnRect = new Rect(Screen.width / 2f - 75, Screen.height / 2f, 150, 32);

		_common = GetComponent<PlayerCommon>();
	}

	public override void Update()
	{
		base.Update();

		if(!Alive && !_dead)
		{
			_dead = true;
			_common.AudioController.Play("Death");
		}
		else if(Alive && _dead)
		{
			_dead = false;
		}
	}

	void OnGUI()
	{
		if(!Alive)
		{
			_respawnRect = GUILayout.Window(10, _respawnRect, RespawnWindow, "Died");
		}
	}

	private void RespawnWindow(int id)
	{
		if(!_respawning)
		{
			if(GUILayout.Button("Respawn"))
			{
				Respawn();
			}
		}
		else
		{
			GUILayout.Label("Respawning...");
		}
	}

	public override void LevelUp() //COMBATKITEDIT
	{
		base.LevelUp();

		_common.AudioController.Play("LevelUp");
	}
}