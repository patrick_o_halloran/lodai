﻿using UnityEngine;
using System.Collections;

#if UNITY_5_3 || UNITY_5_4
using UnityEngine.SceneManagement;
#endif

public class MrpgcDemoGUI : MonoBehaviour
{
	private Rect _readmeRect;
	private Rect _playerRect;

	private PlayerCommon _playerCommon;

	private string _version = "2.1.1";
	private bool _readme = true;

	void Awake()
	{
		GameObject player = GameObject.FindWithTag("Player");

		if(player)
		{
			_playerCommon = player.GetComponent<PlayerCommon>();
		}
	}

	void Start()
	{
		_readmeRect = new Rect(10, 30, Screen.width - 20, 32);
		_playerRect = new Rect(10, 20, 150, 32);
	}

	void Update()
	{
		if(_readme)
		{
			_readmeRect = new Rect(10, Screen.height / 2f - _readmeRect.height / 2f, _readmeRect.width, _readmeRect.height);
		}
	}

	void OnGUI()
	{
		GUILayout.Label("v" + _version);

		if(_readme)
		{
			_readmeRect = GUILayout.Window(0, _readmeRect, Readme, "Readme");
		}
		else
		{
			if(_playerCommon)
			{
                _playerRect = GUILayout.Window(1, _playerRect, PlayerInformation, "Player Info");
			}
		}
	}

	private void Readme(int id)
	{
		GUILayout.Label("Hello!");
		GUILayout.Label("Thanks for trying out the demo for Modular RPG Combat! Your player information will be displayed at the top left corner of the screen.");

		GUILayout.Space(12);

		GUILayout.Label("Here are most of the default controls:");

		GUILayout.Label("Camera Rotation: Mouse Left / Middle / Right buttons + drag");
		GUILayout.Label("Camera Zoom: Mouse ScrollWheel");
		GUILayout.Label("Movement: WSAD");
		GUILayout.Label("Jump: J");
		GUILayout.Label("Toggle walk / run: Right Shift");
		GUILayout.Label("Attack: Spacebar");
		GUILayout.Label("Special Attack (2x damage, only usable with Melee weapon): Left Shift");
		GUILayout.Label("Equip Sword: 1");
		GUILayout.Label("Equip Gun: 2");
		GUILayout.Label("Equip Bow: 3");
		GUILayout.Label("Equip Fireball: 4");
		GUILayout.Label("Equip Firefield (AOE): 5");
		GUILayout.Label("Target the closest enemy: TAB");

		if(GUILayout.Button("Got it!"))
		{
			_readme = false;
		}
	}

	private void PlayerInformation(int id)
	{
		if(GUILayout.Button("Return To Scene Selection"))
		{
#if UNITY_5_3 || UNITY_5_4
			SceneManager.LoadScene("SceneSelection");
#else
			Application.LoadLevel("SceneSelection");
#endif
		}

		GUILayout.BeginHorizontal();

		GUILayout.BeginVertical();
		GUILayout.Label("Health:");
		GUILayout.Label("Mana:");
		GUILayout.Label("Level:");
		GUILayout.Label("Experience:");

		GUILayout.EndVertical();

		GUILayout.BeginVertical();
		GUILayout.Label(_playerCommon.StatController.Health + "/" + _playerCommon.StatController.MaxHealth);
		GUILayout.Label(_playerCommon.StatController.Mana + "/" + _playerCommon.StatController.MaxMana);
		GUILayout.Label(_playerCommon.StatController.level.ToString());
		GUILayout.Label(_playerCommon.StatController.Xp + "/" + _playerCommon.StatController.MaxXp);
		GUILayout.EndVertical();

		GUILayout.EndHorizontal();

		_playerCommon.TargetController.autoAimMelee = GUILayout.Toggle(_playerCommon.TargetController.autoAimMelee, "Auto Aim Melee");
		_playerCommon.TargetController.autoAimRanged = GUILayout.Toggle(_playerCommon.TargetController.autoAimRanged, "Auto Aim Ranged");
		_playerCommon.TargetController.targetSwitch = GUILayout.Toggle(_playerCommon.TargetController.targetSwitch, "Target Switching");

		if(_playerCommon.TargetController.targetSwitch)
		{
			_playerCommon.TargetController.lookAtTarget = GUILayout.Toggle(_playerCommon.TargetController.lookAtTarget, "Look At Target");
		}
		if(_playerCommon.TargetController.lookAtTarget || _playerCommon.TargetController.autoAimMelee || _playerCommon.TargetController.autoAimRanged )
		{
			_playerCommon.TargetController.lookAtTargetVertically = GUILayout.Toggle(_playerCommon.TargetController.lookAtTargetVertically, "Look At Target Vertically");
		}
	}
}