﻿using UnityEngine;
using System.Collections;

#if UNITY_5_3 || UNITY_5_4
using UnityEngine.SceneManagement;
#endif

public class MrpgcSceneManager : MonoBehaviour
{
	public string[] scenes;

	private Rect _sceneRect;

	void Start()
	{
		_sceneRect = new Rect(Screen.width / 2f - 150, Screen.height / 2f, 300, 32);
	}

	private void OnGUI()
	{
		_sceneRect = GUILayout.Window(0, _sceneRect, SceneSelect, "Scene Selection");
	}

	private void SceneSelect(int id)
	{
		foreach(string scene in scenes)
		{
			if(GUILayout.Button(scene))
			{
#if UNITY_5_3 || UNITY_5_4
				SceneManager.LoadScene(scene);
#else
				Application.LoadLevel(scene);
#endif
			}
		}
	}
}