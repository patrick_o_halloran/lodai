Note: To those who are updating from version 1.5 or earlier, it is recommended that you do a clean import as some files have changed location.

We recommend you read through Modular RPG Combat.pdf

If you have any questions, feel free to contact us on our forum at http://unity.phatrobit.com/forum/ or by sending an email to phatrobit@gmail.com