﻿using UnityEngine;

public class FlyCamera : MonoBehaviour
{
    public static FlyCamera Instance;

    public float DefaultMoveSpeed = 15;

    private float MoveSpeed;

    public float X_MouseSensitivity = 5f;
    public float Y_MouseSensitivity = 5f;

    private float _inputX = 0f;
    private float _inputY = 0f;

    public void Awake()
    {
        MoveSpeed = DefaultMoveSpeed;

        Instance = this;
        Cursor.visible = true;
        Cursor.lockState = CursorLockMode.None;
    }

    public void Move(float x, float y)
    {
        Vector3 movement = new Vector3(0, 0, 0);

        movement += transform.forward * x;
        movement += transform.right * y;

        movement.Normalize();

        transform.position += movement * Time.deltaTime * MoveSpeed;
    }

    public void Rotate(float x, float y)
    {
        _inputX += x * X_MouseSensitivity;
        _inputY -= y * Y_MouseSensitivity;

        transform.eulerAngles = new Vector3(_inputY,
            _inputX,
            0);
    }

    void Update()
    {
        if (!Cursor.visible)
        {
            Move(Input.GetAxisRaw("Vertical"), Input.GetAxisRaw("Horizontal"));
            Rotate(Input.GetAxis("Mouse X"), Input.GetAxis("Mouse Y"));
        }

        if(Input.GetKeyDown("escape"))
        {
            Cursor.visible = true;
            Cursor.lockState = CursorLockMode.None;
        }

        if(Input.GetMouseButtonDown(0))
        {
            Cursor.visible = false;
            Cursor.lockState = CursorLockMode.Locked;
        }

        if (Input.GetKey(KeyCode.LeftShift))
            MoveSpeed = DefaultMoveSpeed * 2;
        else
            MoveSpeed = DefaultMoveSpeed;
    }
}