﻿using UnityEngine;
using System.Collections;

/// <summary>
/// A simpler version of EnemyMovementController, requiring less memory resources
/// </summary>
[RequireComponent(typeof(NavMesh))]
public class ProxyMovementController : MonoBehaviour {

    protected NavMeshAgent _navAgent;

    public float speed = 4;
    public float stoppingDistance = 0.5f;
    public float waitTime = 1f;	
    public float roamRadius = 5f;			

    public bool _freeRoamDone = true;			
    public float _waitTimer;

    public bool AgentDone()
    {
        return !_navAgent.pathPending && AgentStopping();
    }

    protected bool AgentStopping()
    {
        return _navAgent.remainingDistance <= _navAgent.stoppingDistance;
    }

    void Awake()
    {
        _navAgent = GetComponent<NavMeshAgent>();
        _navAgent.updateRotation = false;
    }

    void Start () {

    }

    void Update()
    {
        _navAgent.speed = speed;
        _navAgent.stoppingDistance = stoppingDistance; //FIXME: why do it every frame?!
        FreeRoam();
    }

    private void FreeRoam()
    {
        if (_freeRoamDone && (_waitTimer >= waitTime))
        {
            Vector3 randomDirection = transform.position + (Random.insideUnitSphere * roamRadius);
            NavMeshHit hit;
            NavMesh.SamplePosition(randomDirection, out hit, roamRadius, 1);
            _navAgent.destination = hit.position;
            _freeRoamDone = false;
            _waitTimer = 0;
        }
        else
        {
            float dist = _navAgent.remainingDistance;
            if (dist != Mathf.Infinity && AgentDone())
            {
                _waitTimer += Time.deltaTime;
                _freeRoamDone = true;
            }
        }
    }
}
