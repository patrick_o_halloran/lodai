﻿using System;
using System.Collections.Generic;
using UnityEngine;

public class Encounter
{
    public List<GameObject> combatants;
    public List<Faction> factions = new List<Faction>();
    public bool fullsim = false;

    /// <summary>
    /// The Encounter constructor.
    /// See comments
    /// </summary>
    /// <param name="combatants"></param>
    public Encounter(List<GameObject> combatants)
    {
        this.combatants = combatants;

        //Check if any of the agents is at Full LOD, if so set all to Full LOD
        {
            fullsim = false;

            for (int i = 0; i < combatants.Count; i++)
            {
                if (combatants[i].GetComponent<NPC>().LOD == LOD.Full)
                {
                    fullsim = true;
                    break;
                }
            }

            if (fullsim)
            {
                for (int i = 0; i < combatants.Count; i++)
                    if (combatants[i].GetComponent<NPC>().LOD == LOD.Proxy)
                        combatants[i].GetComponent<NPC>().SetLOD(LOD.Full);
            }
        }

        for (int i = 0; i < combatants.Count; i++)
        {
            NPC npc = combatants[i].GetComponent<NPC>();

            npc.currentEncounter = this;
            npc.ResetEncounterTimer();

            if (npc.IsInvoking())
            {
                npc.CancelInvoke();
                npc.InterpolatePrediction();
            }
        }

        //Computes the faction details for the npc feature vectors 
        //Shared computation cached for building feature vector later
        {
            for (int i = 0; i < Spawner.Get().numberOfFactions; i++)
                factions.Add(new Faction());

            for (int i = 0; i < combatants.Count; i++)
            {
                EnemyStatController enemyStatController =
                combatants[i].GetComponent<EnemyStatController>();
                NPC npc = combatants[i].GetComponent<NPC>();

                Team faction = enemyStatController.team;
                Faction details = factions[((int)faction - 1)];

                details.numberOfAgents++;
                details.totalLevel += enemyStatController.level;
                details.totalHealth += enemyStatController.Health;

                if (enemyStatController.Health < 50)
                    details.totalCritical++;

                details.numberOfAgentsByClass[Convert.ToInt32(npc.characterClass)]++;
            }
        }

        List<Feature> featuresToUse = FeatureExtractor.featureVectorPresets["fvectorv2"];

        //Do prediction / sampling
        for (int i = combatants.Count-1; i >= 0; i--)
        {
            NPC npc = combatants[i].GetComponent<NPC>();    
            FeatureExtractor.BuildFeatureVector(ref npc, ref featuresToUse, ref npc.fvectorv2);

            npc.Predict();

            if (npc.LOD == LOD.Full)
            {
                if (DatasetManager.Get().sampling)
                    npc.sampler.BeginSample();
            }
            else
            {
                npc.FreezeMovement();
                if (Convert.ToBoolean(npc.prediction.winner))
                    npc.Invoke("InvokeLive", npc.prediction.duration);
                else
                    npc.Invoke("InvokeDie", npc.prediction.duration);
            }

            npc.bInCombat = true; //Must be set AFTER computing features, as it is used 
                                  //to see if an agent got a 'back attack'
        }
    }

    /// <summary>
    /// Encounter.Update() removes agents who have finished fighting or died
    /// </summary>
    /// <param name="deltaTime"></param>
    public void Update(float deltaTime)
    {
        bool observed = false;

        for (int i = combatants.Count - 1; i >= 0; i--)
        {
            NPC npc = combatants[i].GetComponent<NPC>();

            if (npc.LOD == LOD.Full && npc.FinishedFighting)
            {
                if (DatasetManager.Get().sampling)
                    npc.sampler.EndSample(true);

                Remove(npc.gameObject);
            }

            if (npc.statController._dead)
            {
                if (DatasetManager.Get().sampling && npc.LOD == LOD.Full)
                    npc.sampler.EndSample(false);

                Remove(npc.gameObject);
            }

            npc.encounterTimer += Time.deltaTime;

            if (npc.observed)
                observed = true;
        }

        if (fullsim && !observed)
        {
            RemoveAll();
        }
    }

    /// <summary>
    /// Removes the agent from the encounter. Calls ShutDown() if tey're dead.
    /// Removes the encounter from the encounterManager if it has no combatants
    /// left
    /// </summary>
    /// <param name="combatant"></param>
    public void Remove(GameObject combatant)
    {
        combatants.Remove(combatant);       

        NPC npc = combatant.GetComponent<NPC>();
        npc.currentEncounter = null;
        npc.bInCombat = false;
        if (npc.statController._dead)
            if (!npc.IsShutDown)
                npc.ShutDown();

        if (combatants.Count == 0)
            EncounterManager.encounters.Remove(this);
    }

    //Only ever called from the Full detail. TODO: Instead of removing them,
    // just re-do the Encounter setup
    public void RemoveAll()
    {
        for (int i = combatants.Count - 1; i >= 0; i--)
        {
            NPC npc = combatants[i].GetComponent<NPC>();
            npc.currentEncounter = null;
            npc.bInCombat = false;
            if (npc.statController._dead)
                if (!npc.IsShutDown)
                    npc.ShutDown();
        }

        Observer.UpdateLOD();

        for (int i = 0; i < combatants.Count; i++)
            if(!combatants[i].GetComponent<NPC>().IsShutDown)
                combatants[i].GetComponent<NPC>().ResetTrigger();

        EncounterManager.encounters.Remove(this);
    } 
}
