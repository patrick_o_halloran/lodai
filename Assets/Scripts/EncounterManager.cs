﻿using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class EncounterManager : MonoBehaviour {

    public static List<Encounter> encounters = new List<Encounter>();

    [ShowOnly]
    public int numberOfEncounters = 0;

    private static EncounterManager Instance = null;
    public static EncounterManager Get()
    {
        if (Instance == null)
            Instance = (EncounterManager)FindObjectOfType(typeof(EncounterManager));
        return Instance;
    }

    public void RegisterTriggerEvent(GameObject first,
        GameObject second)
    {
        Observer.UpdateLOD(); //Make sure they're in the right LOD - FIXME: Necessary?

        NPC firstNPC = first.GetComponent<NPC>();
        NPC secondNPC = second.GetComponent<NPC>();

        if (!firstNPC.statController.Alive
            || !secondNPC.statController.Alive)
            return; //Sometimes the triggers still work for a short time after they've been disabled - perhaps a quirk of the physics engine?

        HashSet<GameObject> combatants = new HashSet<GameObject>(); //GC Alloc
        combatants.Add(first);
        combatants.Add(second);

        //Encounters merged
        if (firstNPC.currentEncounter != null)
        {
            foreach (GameObject combatant in firstNPC.currentEncounter.combatants)
                combatants.Add(combatant);
            encounters.Remove(firstNPC.currentEncounter);
        }
        if (secondNPC.currentEncounter != null)
        {
            foreach (GameObject combatant in secondNPC.currentEncounter.combatants)
                combatants.Add(combatant);
            encounters.Remove(secondNPC.currentEncounter);
        }

        encounters.Add(new Encounter(combatants.ToList())); //GC Alloc
    }
	
	void Update ()
    {
        numberOfEncounters = encounters.Count;

        for (int i = encounters.Count - 1; i >= 0; i--)
        {
            try
            {
                encounters[i].Update(Time.deltaTime);
            }
            catch
            {
                Debug.Log("Exception caught in EncounterManager");
            }
        }
    }
}
