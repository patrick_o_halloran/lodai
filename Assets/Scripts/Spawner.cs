﻿using UnityEngine;
using System.Collections.Generic;

public class Spawner : MonoBehaviour
{
    public Terrain terrain;
    public GameObject npcPrefab;

    public enum DistributionType
    {
        exponential, linear
    }
    public DistributionType distributionType;

    public int spawnAmountMin = 2;
    public int spawnAmountMax = 50;

    public float squareMetresPerAgent = 1.0f;
    private int spawnAmt;

    public List<float> classFrequencies; 
    public int maxSpawnLvl = 10;

    [ShowOnly]
    public int numberOfFactions = Constants.numberOfFactions;
    [HideInInspector]
    public List<NPC> npcs = new List<NPC>();

    public float maxInactiveTime = 25.0f;
    public float maxInstanceTime = 90.0f;

    public bool runTimers = false;
    public bool newInstanceOnFactionWin = false;
    public bool newInstance = false;

    [ShowOnly]
    public float timeInactive = 0;
    [ShowOnly]
    public float instanceTime = 0;
    
    public static int[] factionPop = 
        new int[Constants.numberOfFactions] { 0, 0 };

    public bool spawnMore = true;

    private static Spawner Instance = null;
    public static Spawner Get()
    {
        if (Instance == null)
            Instance = (Spawner)FindObjectOfType(typeof(Spawner));
        return Instance;
    }

    void Start ()
    {
        SpawnNPCS(false);
    }

    /// <summary>
    /// Spawner.Update() updates timers if they are set to run, 
    /// and will start a new instance if they reach their respective maxes
    /// (inactive or total). The user can also trigger a new instance manually
    /// </summary>
    void Update()
    {
        if (runTimers)
        {
            if (EncounterManager.encounters.Count == 0)
                timeInactive += Time.deltaTime;
            instanceTime += Time.deltaTime;
        }

        if (timeInactive >= maxInactiveTime
            || instanceTime >= maxInstanceTime)
            newInstance = true;

        if (newInstanceOnFactionWin)
        {
            int remainingFactions = 0;
            for (int i = 0; i < factionPop.Length; i++)
            {
                if (factionPop[i] > 0)
                    remainingFactions++;
            }

            if (remainingFactions < 2
                && remainingFactions != 0)
                newInstance = true;
        }

        if (newInstance)
        {
            newInstance = false;
 
            EncounterManager.encounters.Clear();
            timeInactive = 0;
            instanceTime = 0;

            SpawnNPCS(true);
        }
    }

    /// <summary>
    /// Spawns a random number of NPCs. Destroys old ones.
    /// </summary>
    /// <param name="deleteOldAgents">Set to true if not the first instance</param>
    public void SpawnNPCS(bool deleteOldAgents)
    {
        if (deleteOldAgents)
        {
            for (int i = 0; i < factionPop.Length; i++)
                factionPop[i] = 0;
            for (int i = 0; i < npcs.Count; i++)
                Destroy(npcs[i].gameObject, 0);
            npcs.Clear();
        }
      
        if (distributionType == DistributionType.exponential)
            spawnAmt = (int)RandomFromDistribution.RandomRangeExponential(spawnAmountMin, spawnAmountMax,
                2, RandomFromDistribution.Direction_e.Left);
        else
            spawnAmt = Random.Range(spawnAmountMin, spawnAmountMax);

        if (spawnAmt > 1)
        {
            for (int i = 1; i <= numberOfFactions; i++)
                SpawnNPC(i);
            for (int i = 0; i < spawnAmt - numberOfFactions; i++)
                SpawnNPC(-1);
        }
        else
        {
            SpawnNPC(-1); // Spawning one npc is useful for testing sometimes
        }

        Observer.UpdateLOD();
    }

    /// <summary>
    /// Spawns NPCs within the area decided by the pop. density (squareMetersPerAgent)
    /// A class, team and level are generated.
    /// </summary>
    /// <param name="team">Pass -1 for a random team</param>
    public void SpawnNPC(int team)
    {
        float padding = (terrain.terrainData.size.x 
            - Mathf.Sqrt(squareMetresPerAgent * spawnAmt))/2;
        float x = Random.Range(padding, terrain.terrainData.size.x - padding);
        float z = Random.Range(padding, terrain.terrainData.size.z - padding);
        float y = terrain.SampleHeight(new Vector3(x, 0, z));

        GameObject npc = Instantiate(npcPrefab, 
            new Vector3(x, y, z), Quaternion.identity) as GameObject;

        switch (Choose(classFrequencies))
        {
            case 0:
                npc.GetComponent<NPC>().characterClass = CharacterClass.Fighter;
                break;
            case 1:
                npc.GetComponent<NPC>().characterClass = CharacterClass.Gunner;
                break;
            case 2:
                npc.GetComponent<NPC>().characterClass = CharacterClass.Mage;
                break;
            case 3:
                npc.GetComponent<NPC>().characterClass = CharacterClass.Archer;
                break;
        }

        EnemyStatController statController =
            npc.GetComponent<EnemyStatController>();
        statController.regenHealth = false;
        statController.respawnTime = 0; //No respawns

        if(team == -1)
            npc.GetComponent<NPC>().SetTeam(Random.Range(1/*inclusive*/, numberOfFactions+1/*exclusive*/));
        else
            npc.GetComponent<NPC>().SetTeam(team);

        factionPop[(int)npc.GetComponent<StatController>().team-1]++;

        statController.level = 1 +
            Mathf.FloorToInt(
                RandomFromDistribution.RandomFromExponentialDistribution(
                3, RandomFromDistribution.Direction_e.Left) * maxSpawnLvl);
        statController.UpdateStats();

        npc.GetComponent<NPC>().SetLOD(LOD.Proxy, true); //By default NPCs are set up as proxies
        npcs.Add(npc.GetComponent<NPC>());
    }

    /// <summary>
    /// Choose an index based on list of probabilities
    /// https://docs.unity3d.com/Manual/RandomNumbers.html
    /// </summary>
    /// <param name="probs"></param>
    /// <returns></returns>
    int Choose(List<float> probs)
    {
        float total = 0;

        foreach (float elem in probs)
        {
            total += elem;
        }

        float randomPoint = Random.value * total;

        for (int i = 0; i < probs.Count; i++)
        {
            if (randomPoint < probs[i])
            {
                return i;
            }
            else {
                randomPoint -= probs[i];
            }
        }
        return probs.Count - 1;
    }
    /// <summary>
    /// Inverse cumulative probability curve
    /// </summary>
    /// <param name="curve"></param>
    /// <returns></returns>
    float CurveWeightedRandom(AnimationCurve curve)
    {
        return curve.Evaluate(Random.value);
    }
}
