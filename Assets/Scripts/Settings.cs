﻿using System;
using System.Collections.Generic;
using UnityEngine;

static class Constants
{
    public const int minSampleSize = 250;

    public const int aggroRange = 10;
    public const float sizeGainPerLevel = 0.25f;

    public const int numberOfFactions = 2;

    public static List<Color> teamColors = new List<Color>
    {
        Color.white,
        Color.red,
        Color.blue,
        Color.green,
        Color.yellow,
        Color.magenta, //orange
        Color.cyan, //pink
        Color.black
    };
}

/// <summary>
/// Some settings and miscellaneous stuff for the framework
/// </summary>
public class Settings : MonoBehaviour {

    public bool showNPCLabels = true;
    public float showLabelMaxDistance = 100.0f;

    public bool requestGC = true;
    public bool showDebugMessages = true;
    public bool vsyncEnabled = true;

    public bool registerEncounters = true;
    public bool fullCheapMode = false;

    public bool showEncounterSpheres = false;
    public bool showCapsules = false;

    //Profiling
    private float totalUpdateTime;
    private float updateTimeSamples;
    private float updateTimeAverage;

    private float deltaTime = 0.0f;
    [ShowOnly]
    public float msec;
    [ShowOnly]
    public float fps;

    public bool showInGameGUI = false;
    public bool useNN = true;

    private static Settings Instance = null;
    public static Settings Get()
    {
        if (Instance == null)
            Instance = (Settings)FindObjectOfType(typeof(Settings));
        return Instance;
    }

    void Awake () {

#if UNITY_EDITOR
        UnityEditor.SceneView.FocusWindowIfItsOpen(typeof(UnityEditor.SceneView));
#endif
        Application.runInBackground = true;

        // write FPS to "profilerLog.txt"
        //Profiler.logFile = Application.dataPath + "/profilerLog.txt";
        // write Profiler Data to "profilerLog.txt.data"                                                                                        
        //Profiler.enableBinaryLog = true;
        //Profiler.enabled = true;

        FeatureExtractor.SetUpPresets();
    }

    void Update ()
    {
        deltaTime += (Time.deltaTime - deltaTime) * 0.1f;
        msec = deltaTime * 1000.0f;
        fps = 1.0f / deltaTime;

        if (Time.frameCount % 30 == 0 && requestGC)
            System.GC.Collect();

        QualitySettings.vSyncCount = Convert.ToInt32(vsyncEnabled);

        updateTimeSamples++;
        totalUpdateTime += Time.deltaTime;
        updateTimeAverage = totalUpdateTime / updateTimeSamples;

        if (updateTimeSamples == 2000)
            Debug.Log("UPDATE_TIME_AVERAGE: " + updateTimeAverage);
    }

    void OnGUI()
    {
        if (!showInGameGUI)
            return;

        GUI.Label(new Rect(10, 10, 1000, 50), "DistanceThreshold: " 
            + Observer.Get().distanceThreshold);
        GUI.Label(new Rect(10, 50, 1000, 50), "UpdateTimeTotalAvg: "
            + updateTimeAverage);
        GUI.Label(new Rect(10, 90, 1000, 50), "FPS "
            + fps);
        GUI.Label(new Rect(10, 130, 1000, 50), "Total frames "
            + updateTimeSamples);
    }  

    void OnApplicationQuit()
    {
        Profile.PrintResults();
    }
}
