﻿using Emgu.CV;
using System;
using System.Collections.Generic;

public enum Feature
{
    characterClass,
    characterClassLegacy,
    unitLevel,
    unitHp,
    preEmptiveStrike,
    alliesNumberOfAgents,
    alliesAverageHealth,
    alliesAverageLevel,
    alliesTeamComposition,
    alliesCritical,
    enemiesNumberOfAgents,
    enemiesAverageHealth,
    enemiesAverageLevel,
    enemiesTeamComposition,
    enemiesCritical
}

public class FeatureExtractor {

    public static Dictionary<string, List<Feature>> featureVectorPresets
        = new Dictionary<string, List<Feature>>();

    /// <summary>
    /// These are preset feature vectors for convenience and readability
    /// </summary>
    public static void SetUpPresets()
    {
        featureVectorPresets["fvectorv1"]
            = new List<Feature>()
            {
                Feature.characterClassLegacy,
                Feature.unitLevel,
                Feature.unitHp,
                Feature.alliesNumberOfAgents,
                Feature.alliesAverageHealth,
                Feature.alliesAverageLevel,
                Feature.enemiesNumberOfAgents,
                Feature.enemiesAverageHealth,
                Feature.enemiesAverageLevel
            };

        featureVectorPresets["fvectorv1.1"]
            = new List<Feature>()
            {
                Feature.characterClass,
                Feature.unitLevel,
                Feature.unitHp,
                Feature.alliesNumberOfAgents,
                Feature.alliesAverageHealth,
                Feature.alliesAverageLevel,
                Feature.enemiesNumberOfAgents,
                Feature.enemiesAverageHealth,
                Feature.enemiesAverageLevel
            };

        featureVectorPresets["fvectorv2"]
            = new List<Feature>()
            {
                Feature.characterClass, //0
                Feature.unitLevel, //1
                Feature.unitHp, //2
                Feature.preEmptiveStrike, //3
                Feature.alliesNumberOfAgents, //4
                Feature.alliesAverageHealth, //5
                Feature.alliesAverageLevel, //6
                Feature.alliesTeamComposition, //7-10
                Feature.alliesCritical, //11
                Feature.enemiesNumberOfAgents, //12
                Feature.enemiesAverageHealth, //13
                Feature.enemiesAverageLevel, //14
                Feature.enemiesTeamComposition, //15-18
                Feature.enemiesCritical //19
            };
    }

    /// <summary>
    /// Builds a feature vector to make a prediction for a particular npc.
    /// The vector can built with features specified in any order.
    /// </summary>
    /// <param name="npc">The npc in question</param>
    /// <param name="featuresToUse"></param>
    /// <param name="vector">Vector to fill</param>
    public static void BuildFeatureVector(
        ref NPC npc,
        ref List<Feature> featuresToUse,
        ref Matrix<float> vector)
    {
        Faction allyFaction = 
            npc.currentEncounter.factions
            [(int)npc.statController.team - 1];
        Faction enemyFaction =
            npc.currentEncounter.factions
            [Convert.ToInt32(((int)npc.statController.team - 1) == 0)];

        int idx = 0;
        for (int i = 0; i < featuresToUse.Count; i++)
        {
            switch(featuresToUse[i])
            {
                case Feature.characterClass:
                case Feature.characterClassLegacy:
                    {
                        vector[0, idx] = (int)npc.characterClass;
                        if (featuresToUse[i] == Feature.characterClassLegacy && vector[0, idx] == 3)
                            vector[0, idx] = 4;
                        idx++;
                    }
                    break;

                case Feature.unitLevel:
                    {
                        vector[0, idx++] = npc.statController.level;
                    }
                    break;

                case Feature.unitHp:
                    {
                        vector[0, idx++] = npc.statController.Health;
                    }
                    break;

                case Feature.preEmptiveStrike:
                    {
                        vector[0, idx++] = npc.bInCombat ? 1 : 0; //Getting the 'jump' on them
                    }
                    break;

                case Feature.alliesNumberOfAgents:
                    {
                        vector[0, idx++] = allyFaction.numberOfAgents;
                    }
                    break;

                case Feature.alliesAverageHealth:
                    {
                        vector[0, idx++] = allyFaction.AverageHealth;
                    }
                    break;

                case Feature.alliesAverageLevel:
                    {
                        vector[0, idx++] = allyFaction.AverageLevel;
                    }
                    break;

                case Feature.alliesTeamComposition: 
                    {
                        float[] allyFactionComposition = allyFaction.Composition;
                        for (int j = 0; j < 4; j++)
                            vector[0, idx++] = allyFactionComposition[j];
                    }
                    break;

                case Feature.alliesCritical:
                    {
                        vector[0, idx++] = allyFaction.Critical;
                    }
                    break;

                case Feature.enemiesNumberOfAgents:
                    {
                        vector[0, idx++] = enemyFaction.numberOfAgents;
                    }
                    break;

                case Feature.enemiesAverageHealth:
                    {
                        vector[0, idx++] = enemyFaction.AverageHealth;
                    }
                    break;

                case Feature.enemiesAverageLevel:
                    {
                        vector[0, idx++] = enemyFaction.AverageLevel;
                    }
                    break;

                case Feature.enemiesTeamComposition: //Slow
                    {
                        float[] enemyFactionComposition = enemyFaction.Composition;
                        for (int j = 0; j < 4; j++)
                            vector[0, idx++] = enemyFactionComposition[j];
                    }
                    break;

                case Feature.enemiesCritical:
                    {
                        vector[0, idx] = enemyFaction.Critical;
                    }
                    break;
            }
        }
    }
}
