﻿using Emgu.CV;
using Emgu.CV.ML;
using Emgu.CV.ML.MlEnum;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Diagnostics;
using UnityEngine;

/// <summary>
/// The ModelTrainer class oversees the training of the models, by preparing
/// the training and test sets, calling the algorithms in the algorithm class,
/// and evaluating the resulting models.
/// </summary>
class ModelTrainer 
{
    /// <summary>
    /// Takes in the samples, and fills the four output Matrices with training and test
    /// data and classes for consumption by the algorithms
    /// </summary>
    /// <param name="samples"></param>
    /// <param name="trainingData"></param>
    /// <param name="trainingClasses"></param>
    /// <param name="testData"></param>
    /// <param name="testClasses"></param>
    /// <param name="targetVarAmt"></param>
    public static void PrepareData(List<Matrix<float>> samples,
        out Matrix<float> trainingData, out Matrix<float> trainingClasses,
        out Matrix<float> testData, out Matrix<float> testClasses, int targetVarAmt)
    {
        int sampleAmt, numTrainingSamples; 

        /*if (ModelManager.Get().useSpecificNumberOfSamples)
        {
            sampleAmt = ModelManager.Get().specificNumberOfSamples;
            numTrainingSamples =
                Mathf.RoundToInt(sampleAmt * ModelManager.Get().trainingToTestRatio);
        }
        else*/if (ModelManager.Get().useSpecificSetSizes)
        {
            sampleAmt = ModelManager.Get().trainingSetSize +
                ModelManager.Get().testSetSize;
            numTrainingSamples = ModelManager.Get().trainingSetSize;
        }
        else
        {
            sampleAmt = samples.Count;
            numTrainingSamples =
                Mathf.RoundToInt(sampleAmt * ModelManager.Get().trainingToTestRatio);
        }

        int featureAmt = samples[0].Cols - targetVarAmt;
        int numTestSamples = sampleAmt - numTrainingSamples;

        //Training Set
        {           
            Matrix<float> trainingSet =
                new Matrix<float>(numTrainingSamples, featureAmt + targetVarAmt);

            if (!ModelManager.Get().testSetFromFront)
            {
                for (int i = 0; i < numTrainingSamples; i++)
                    samples[i].GetRow(0).CopyTo(trainingSet.GetRow(i));
            }
            else
            {
                for (int i = numTestSamples; i < sampleAmt; i++)
                    samples[i].GetRow(0).CopyTo(trainingSet.GetRow(i - numTestSamples));
            }

            //Features
            trainingData = new Matrix<float>(numTrainingSamples, featureAmt);
            for (int i = 0; i < numTrainingSamples; i++)
                for (int j = 0; j < featureAmt; j++)
                    trainingData[i, j] = trainingSet[i, j];

            //Classes
            trainingClasses = new Matrix<float>(numTrainingSamples, targetVarAmt);
            for (int i = 0; i < numTrainingSamples; i++)
                for (int j = 0; j < targetVarAmt; j++)
                    trainingClasses[i, j] = trainingSet[i, featureAmt + j];
        }

        //Test Set
        {
            Matrix<float> testSet =
               new Matrix<float>(numTestSamples, featureAmt + targetVarAmt);

            if (!ModelManager.Get().testSetFromFront)
            {
                for (int i = numTrainingSamples; i < sampleAmt; i++)
                    samples[i].GetRow(0).CopyTo(testSet.GetRow(i - numTrainingSamples));
            }
            else
            {
                for (int i = 0; i < numTestSamples; i++)
                    samples[i].GetRow(0).CopyTo(testSet.GetRow(i));
            }

            //Features
            testData = new Matrix<float>(numTestSamples, featureAmt);
            for (int i = 0; i < numTestSamples; i++)
                for (int j = 0; j < featureAmt; j++)
                    testData[i, j] = testSet[i, j];

            //Classes
            testClasses = new Matrix<float>(numTestSamples, targetVarAmt);
            for (int i = 0; i < numTestSamples; i++)
                for (int j = 0; j < targetVarAmt; j++)
                    testClasses[i, j] = testSet[i, featureAmt + j];
        }
    }

    public static /*StatModel*/void TrainModel(List<Matrix<float>> samples,
        Matrix<Byte> varType, string modelName, int outputs)
    {
        System.Diagnostics.Debug.Assert(samples.Count > 0);

        Matrix<float> trainingData, trainingClasses,
            testData, testClasses;
        PrepareData(samples, out trainingData, out trainingClasses,
            out testData, out testClasses, outputs);

        for (int i = 0; i < outputs; i++)
            if (varType[trainingData.Cols + i, 0] == (byte)VAR_TYPE.CATEGORICAL)
                BaseClassifier(trainingClasses, i);

        if (outputs == 1)
        {
            if(ModelManager.Get().decisionTree)
                Algorithm.Decisiontree(trainingData, trainingClasses, testData, testClasses,
                    varType, modelName);
            if (ModelManager.Get().extremelyRandomTrees)
                Algorithm.ExtremelyRandomTrees(trainingData, trainingClasses, testData, testClasses,
                varType, modelName);
            if (ModelManager.Get().gradiantBoostedTrees)
                Algorithm.GradientBoostedTrees(trainingData, trainingClasses, testData, testClasses,
                varType, modelName);
            if (ModelManager.Get().rTrees)
                Algorithm.RTrees(trainingData, trainingClasses, testData, testClasses,
                varType, modelName);

            if (ModelManager.Get().knn)
                Algorithm.Knn(trainingData, trainingClasses, testData, testClasses,
                    varType, modelName);
            if (ModelManager.Get().svm)
                Algorithm.Svm(trainingData, trainingClasses, testData, testClasses,
                    varType, modelName);

            if (varType[trainingData.Cols, 0] == (byte)VAR_TYPE.CATEGORICAL)
            {
                if (ModelManager.Get().bayes)
                    Algorithm.Bayes(trainingData, trainingClasses, testData, testClasses, modelName);
                //Boosted trees can only be used for 2-class classification           
                if (ModelManager.Get().boost)
                    Algorithm.Boost(trainingData, trainingClasses, testData, testClasses,
                    varType, modelName);
            }              
        }

        if (ModelManager.Get().mlp)
            Algorithm.Mlp(trainingData, trainingClasses, testData, testClasses,
            varType, modelName);

        if (varType[trainingData.Cols, 0] == (byte)VAR_TYPE.CATEGORICAL
            && outputs == 1 && ModelManager.Get().ensemble)
        {
            Ensemble ensemble = new Ensemble();
            ensemble.models.Add(ModelManager.LoadANN(modelName));
            ensemble.models.Add(ModelManager.LoadBayes(modelName));
            ensemble.models.Add(ModelManager.LoadRTrees(modelName));
            ensemble.models.Add(ModelManager.LoadBoost(modelName));
            ensemble.models.Add(ModelManager.LoadDTree(modelName));

            double predictionTotalTime = 0;
            Stopwatch stopwatch;
            Matrix<float> predicted = new Matrix<float>(testClasses.Rows, 1);
            for (int i = 0; i < testData.Rows; i++)
            {
                Matrix<float> sample = testData.GetRow(i);
                stopwatch = Stopwatch.StartNew();
                predicted[i, 0] = ensemble.Predict(sample, Ensemble.Mode.Majority);
                stopwatch.Stop();
                predictionTotalTime += ((double)stopwatch.ElapsedTicks / Stopwatch.Frequency) * 1000;
            }

            UnityEngine.Debug.Log("ENSEMBLE (Majority)\n"
                + evaluationStr(predicted, testClasses, false));
            UnityEngine.Debug.Log("Average prediction time: " + predictionTotalTime * (1.0 / testData.Rows));
        }
        else if (varType[trainingData.Cols, 0] == (byte)VAR_TYPE.NUMERICAL
            && outputs == 1 && ModelManager.Get().ensemble)
        {
            Ensemble ensemble = new Ensemble();
            ensemble.models.Add(ModelManager.LoadANN(modelName));
            ensemble.models.Add(ModelManager.LoadRTrees(modelName));
            ensemble.models.Add(ModelManager.LoadDTree(modelName));
            //ensemble.models.Add(ModelManager.LoadERTrees(modelName));

            Matrix<float> predicted = new Matrix<float>(testClasses.Rows, 1);

            double predictionTotalTime = 0;
            Stopwatch stopwatch;
            for (int i = 0; i < testData.Rows; i++)
            {
                Matrix<float> sample = testData.GetRow(i);
                stopwatch = Stopwatch.StartNew();
                predicted[i, 0] = ensemble.Predict(sample, Ensemble.Mode.Mean);
                stopwatch.Stop();
                predictionTotalTime += ((double)stopwatch.ElapsedTicks / Stopwatch.Frequency) * 1000;
            }
            UnityEngine.Debug.Log("ENSEMBLE (Mean)");
            UnityEngine.Debug.Log(evaluationStr(predicted, testClasses, true));
            UnityEngine.Debug.Log("Average prediction time: " + predictionTotalTime * (1.0 / testData.Rows));

            predictionTotalTime = 0;
            for (int i = 0; i < testData.Rows; i++)
            {
                Matrix<float> sample = testData.GetRow(i);
                stopwatch = Stopwatch.StartNew();
                predicted[i, 0] = ensemble.Predict(sample, Ensemble.Mode.Median);
                stopwatch.Stop();
                predictionTotalTime += ((double)stopwatch.ElapsedTicks / Stopwatch.Frequency) * 1000;
            }
            UnityEngine.Debug.Log("ENSEMBLE (Median)");
            UnityEngine.Debug.Log(evaluationStr(predicted, testClasses, true));
            UnityEngine.Debug.Log("Average prediction time: " + predictionTotalTime * (1.0 / testData.Rows));
        }
    }

    /// <summary>
    /// A 'ToString' for evaluation
    /// </summary>
    /// <param name="predicted"></param>
    /// <param name="actual"></param>
    /// <param name="regression"></param>
    /// <param name="i"></param>
    /// <returns></returns>
    public static string evaluationStr(Matrix<float> predicted, Matrix<float> actual, bool regression,
        int i = 0)
    {
        if (!regression)
            return "Classification accuracy (fraction):" + evaluateC(predicted, actual, i);
        else
            return "Regression RMSE (target unit):" + evaluateR(predicted, actual, i)
                + "\nRegression MAE (target unit):" + evaluateR(predicted, actual, i, true);    
    }

    /// <summary>
    /// This function evaluates the classification accuracy i.e. the percentage
    /// of correct classifications
    /// </summary>
    /// <param name="predicted"></param>
    /// <param name="actual"></param>
    /// <param name="idx">For multiple outputs</param>
    /// <returns></returns>
    public static float evaluateC(Matrix<float> predicted, Matrix<float> actual,
        int idx)
    {
        int t = 0;
        int f = 0;

        for (int i = 0; i < actual.Rows; i++)
        {
            float p = predicted[i, idx];
            float a = actual[i, idx];

            if (p == a)
                t++;
            else
                f++;
        }

        return (float)t / actual.Rows;
    }

    /// <summary>
    /// This function evaluates regression accuracy, returns error measures in the
    /// same units as the target variable
    /// </summary>
    /// <param name="predicted"></param>
    /// <param name="actual"></param>
    /// <param name="idx">If there are multiple outputs</param>
    /// <param name="MAE">Mean avg. error or RMSE</param>
    /// <returns></returns>
    public static float evaluateR(Matrix<float> predicted, Matrix<float> actual,
        int idx, bool MAE = false)
    {
        float sum = 0.0f;
        for (int i = 0; i < actual.Rows; i++)
        {
            float difference = actual[i, idx] - predicted[i, idx];

            if (!MAE)
                sum += Mathf.Pow(difference, 2);
            else
                sum += Mathf.Abs(difference);
        }

        if (!MAE)
            return Mathf.Sqrt(sum / actual.Rows);
        else
            return sum / actual.Rows;
    }

    /// <summary>
    /// The base classifier just picks the most frequent class in the training set.
    /// </summary>
    /// <param name="trainingClasses"></param>
    public static void BaseClassifier(Matrix<float> trainingClasses)
    {
        BaseClassifier(trainingClasses, 0);
    }

    public static void BaseClassifier(Matrix<float> trainingClasses,
        int idx)
    {
        int zero = 0;
        int one = 0;
        for (int i = 0; i < trainingClasses.Rows; i++)
        {
            if (trainingClasses[i, idx] == 0)
                zero++;
            else
                one++;
        }

        UnityEngine.Debug.Log("Base classifier: " +
            ((float)Mathf.Max(zero, one) / trainingClasses.Rows));
    }
}

