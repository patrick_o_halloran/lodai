﻿using Emgu.CV;
using Emgu.CV.ML;
using System.Collections.Generic;
using UnityEngine;

public enum LOD { Full, Proxy }
public enum CharacterClass : int { Fighter, Gunner, Archer, Mage }

public class Prediction
{
    public int winner = -1;
    public float duration = 0;
    public int xpGain = 0;
    public int hpLoss = 0;
    
    public float t0 = 0;
    public int remainingHp;

    /// <summary>
    /// For interpolation
    /// </summary>
    public float T
    {
        get
        {
            return (Time.time - t0) / duration;
        }
    }

    public Prediction(int winner, float duration, int xpGain, int hpLoss,
         float t0, int currentHp)
    {
        this.winner = winner;
        this.duration = duration;
        this.hpLoss = hpLoss;

        this.t0 = t0; //t0 is saved for interpolation
        remainingHp = currentHp - hpLoss; 
    }
}

public class NPC : MonoBehaviour
{
    public Encounter currentEncounter;
    public Prediction prediction;

    [ShowOnly]
    public float encounterTimer = 0;
    [ShowOnly]
    public bool bInCombat = false;

    public bool observed = false;

    public Avatar avatar; //Animator avatar prefab
    public GameObject modelPrefab;

    public CharacterClass characterClass;

    public LOD LOD = LOD.Full;

    public Matrix<float> fvectorv2 = new Matrix<float>(1, 20);
    Matrix<float> response = new Matrix<float>(1, 4);

    //bool memLOD = true; //TODO

    #region Components
    [HideInInspector]
    public EnemyTargetController targetController;
    [HideInInspector]
    public EnemyMovementController movementController;
    [HideInInspector]
    public EnemyStatController statController;
    [HideInInspector]
    public EnemyCombatController combatController;
    [HideInInspector]
    public Sampler sampler;
    [HideInInspector]
    public AudioController audioController;
    [HideInInspector]
    public LineRenderer lineRenderer;
    [HideInInspector]
    public Animator animator;
    [HideInInspector]
    public AudioSource audioSource;
    [HideInInspector]
    public CapsuleCollider capsuleCollider;
    [HideInInspector]
    public NavMeshAgent navMeshAgent;
    [HideInInspector]
    public SkinnedMeshRenderer skinnedMeshRenderer;
    [HideInInspector]
    public SphereCollider encounterSphere;
    #endregion

    [HideInInspector]
    public GameObject encounterSphereVis;
    [HideInInspector]
    public GameObject capsuleVis;
    [HideInInspector]
    public GameObject MrpgcChibit;

    /// <summary>
    /// If an agent is online, it is deemed to have won an encounter if it
    /// doesn't have a target, it is not returning, and it is alive.
    /// </summary>
    public bool FinishedFighting
    {
        get
        {
            return !targetController.Target
            && !movementController.Returning
            && statController.Alive
            && (LOD == LOD.Full)
            && currentEncounter != null;
        }
    }

    /// <summary>
    /// If the agent has an encounter, it is in combat. This is consulted when
    /// changing LODs, as agents don't switch down when in combat (in this version
    /// at least)
    /// </summary>
    public bool IsInCombat
    {
        get
        {
            return currentEncounter != null;
        }
    }

    public bool IsShutDown
    {
        get
        {
            return statController == null;
        }
    }

    void Awake()
    {
        encounterSphereVis = transform.FindChild("EncounterSphere").gameObject;
        encounterSphereVis.GetComponent<MeshRenderer>().enabled = true;
        capsuleVis = transform.FindChild("Capsule").gameObject;
        capsuleVis.GetComponent<MeshRenderer>().enabled = true;
        //armature = transform.FindChild("Armature").gameObject;

        encounterSphere = GetComponent<SphereCollider>();    
        statController = GetComponent<EnemyStatController>();
        sampler = GetComponent<Sampler>();
        navMeshAgent = GetComponent<NavMeshAgent>();
        //skinnedMeshRenderer = GetComponentInChildren<SkinnedMeshRenderer>();
    }

    void Start()
    {      


        ResetTrigger();

        //Set up encounter sphere visualisation material
        {
            Material m = encounterSphereVis.GetComponent<Renderer>().material;
            Color teamColor = Constants.teamColors[(int)statController.team];
            m.color = new Color(teamColor.r, teamColor.g, teamColor.b, 0.4f);
            //http://docs.unity3d.com/Manual/MaterialsAccessingViaScript.html
            m.SetFloat("_Mode", 2);
            m.SetInt("_SrcBlend", (int)UnityEngine.Rendering.BlendMode.SrcAlpha);
            m.SetInt("_DstBlend", (int)UnityEngine.Rendering.BlendMode.OneMinusSrcAlpha);
            m.SetInt("_ZWrite", 0);
            m.DisableKeyword("_ALPHATEST_ON");
            m.EnableKeyword("_ALPHABLEND_ON");
            m.DisableKeyword("_ALPHAPREMULTIPLY_ON");
            m.renderQueue = 3000;
        }
    }

    /// <summary>
    /// This function sets the LOD of the npc, and the appropiate NPC gameObject
    /// is build by adding / destroying components to free up processing and memory
    /// resources.
    /// </summary>
    /// <param name="set">The new LOD</param>
    public void SetLOD(LOD set, bool init = false)
    {
        Profile.StartProfile("SetLOD");
        if (set == LOD.Full)
        {
            if (LOD == LOD.Full && !init)
                return;
            LOD = LOD.Full;

            if (animator)
            {
                Debug.Log("Animator already exists");
                return;
            }

            if (IsInvoking())
            {
                CancelInvoke();
                UnFreezeMovement();
                InterpolatePrediction();
            }

            //Dead or alive
            {
                //sampler = gameObject.AddComponent<Sampler>();

                //if (!Settings.Get().fullCheapMode)
                //{
                //    GetComponent<Rigidbody>().interpolation
                //            = RigidbodyInterpolation.Interpolate;
                //}
                //else
                //{
                    GetComponent<Rigidbody>().interpolation
                            = RigidbodyInterpolation.None;
                //}

                capsuleVis.SetActive(Settings.Get().fullCheapMode);

                //#region TODO: delete armature
                //armature = Instantiate(armaturePrefab) as GameObject;
                

                //#region TODO: delete skinnedMeshRenderer        
                MrpgcChibit = Instantiate(modelPrefab) as GameObject;
                MrpgcChibit.transform.name = "Model";
                MrpgcChibit.transform.parent = transform;
                MrpgcChibit.transform.localPosition = new Vector3(0, 0, 0);
                MrpgcChibit.transform.localRotation = Quaternion.Euler(new Vector3(0, 0, 0));
                MrpgcChibit.transform.localScale = new Vector3(1, 1, 1);
                skinnedMeshRenderer = GetComponentInChildren<SkinnedMeshRenderer>();
                //skinnedMeshRenderer.enabled = false;
                //skinnedMeshRenderer.rootBone = transform.Find("Armature/hips");          
                //#endregion
                skinnedMeshRenderer.enabled = !Settings.Get().fullCheapMode;

                //armature.transform.name = "Armature";
                //armature.transform.parent = transform;
                //armature.transform.localPosition = new Vector3(0, 0, 0);
                //armature.transform.localScale = new Vector3(1, 1, 1);
                //#endregion
                //armature.SetActive(!Settings.Get().fullCheapMode);

                statController.objectRenderer = skinnedMeshRenderer;
                statController._defaultColor = skinnedMeshRenderer.materials[1].color;
                skinnedMeshRenderer.materials[0].color 
                    = Constants.teamColors[(int)statController.team];

                animator = gameObject.AddComponent<Animator>();
                animator.runtimeAnimatorController =
                    Resources.Load<RuntimeAnimatorController>
                        ("AnimationControllers/MrpgcEnemyAnimator");
                animator.cullingMode = AnimatorCullingMode.CullUpdateTransforms;
                animator.avatar = avatar;

                statController._animator = animator;
            }
         
            if (statController._dead)
            {
                animator.Play(Animator.StringToHash(CP.Dead), 0, 1.0f);
            }
            else
            {
                {
                    statController.enabled = true;

                    if (!Settings.Get().fullCheapMode)
                    {
                        navMeshAgent.obstacleAvoidanceType
                                = ObstacleAvoidanceType.HighQualityObstacleAvoidance;
                    }
                    else
                    {
                        navMeshAgent.obstacleAvoidanceType
                            = ObstacleAvoidanceType.NoObstacleAvoidance;
                    }
                }

                targetController = gameObject.AddComponent<EnemyTargetController>();
                targetController.aggressive = true;
                targetController.aggroDetection = AggroDetection.Collider;
                targetController.fieldOfView = 180;
                targetController.aggroRange = 10;

                if (!Settings.Get().fullCheapMode)
                {
                    lineRenderer = gameObject.AddComponent<LineRenderer>();
                    lineRenderer.SetPosition(1, new Vector3(0, 0, 0));
                
                    audioSource = gameObject.AddComponent<AudioSource>();
                    GetComponent<AudioSource>().spatialBlend = 1;

                    audioController = gameObject.AddComponent<AudioController>();
                }
                else
                {
                    if (lineRenderer)
                        Destroy(lineRenderer);
                    if (audioSource)
                        Destroy(audioSource);
                    if (audioController)
                        Destroy(audioController);
                }

                capsuleCollider = gameObject.AddComponent<CapsuleCollider>();
                capsuleCollider.height = 2;
                capsuleCollider.radius = 0.3f;
                capsuleCollider.center = new Vector3(0, 1, 0);

                movementController = gameObject.AddComponent<EnemyMovementController>();
                movementController.roamRadius = 20;
                movementController.enabled = true;
                movementController.logicStyle = EnemyLogicStyle.FreeRoam;
                if (GetComponent<ProxyMovementController>())
                {
                    ProxyMovementController proxyMovementController
                        = GetComponent<ProxyMovementController>();
                    movementController._freeRoamDone = proxyMovementController._freeRoamDone;
                    movementController._waitTimer = proxyMovementController._waitTimer;
                    Destroy(proxyMovementController);
                }

                combatController = gameObject.AddComponent<EnemyCombatController>();
                combatController.chaseSpeed = 5;
                combatController.weaponDamage = 40;
                combatController.weaponMount = transform.Find("Model/Armature/hips/lower_spine/mid_spine/upper_spine/" +
                    "shoulder_r/upper_arm_r/lower_arm_r/hand_r/hand_mount_r");
                combatController.swordModel = Resources.Load<GameObject>
                    ("Prefabs/Weapons/Sword");
                combatController.gunModel = Resources.Load<GameObject>
                    ("Prefabs/Weapons/Gun");
                combatController.bowModel = Resources.Load<GameObject>
                    ("Prefabs/Weapons/Bow");
                combatController.swordMaterial = Resources.Load<Material>
                    ("Models/Sword/Materials/Sword001");
                combatController.gunMaterial = Resources.Load<Material>
                    ("Models/Gun/Materials/Gun");
                combatController.bowMaterial = Resources.Load<Material>
                    ("Models/Bow/Materials/Bow");
                combatController.bullet = Resources.Load<GameObject>
                    ("Prefabs/Weapons/EnemyBullet");
                combatController.spell = Resources.Load<GameObject>
                    ("Prefabs/Weapons/EnemySpell");
                combatController.aoeSpell = Resources.Load<GameObject>
                    ("Prefabs/Weapons/EnemyAoeSpell");
                combatController.arrow = Resources.Load<GameObject>
                    ("Prefabs/Weapons/EnemyArrow");
                switch (characterClass)
                {
                    case CharacterClass.Fighter:
                        combatController.weaponStyle = WeaponType.Melee;
                        combatController.rangedStyle = RangedType.None;
                        combatController.weaponRange = 3;
                        break;
                    case CharacterClass.Gunner:
                        combatController.weaponStyle = WeaponType.Ranged;
                        combatController.weaponRange = 20;
                        combatController.rangedStyle = RangedType.Gun;
                        break;
                    case CharacterClass.Archer:
                        combatController.weaponStyle = WeaponType.Ranged;
                        combatController.weaponRange = 10;
                        combatController.rangedStyle = RangedType.Bow;
                        break;
                    case CharacterClass.Mage:
                        combatController.weaponStyle = WeaponType.Ranged;
                        combatController.weaponRange = 20;
                        combatController.rangedStyle = RangedType.AOESpell;
                        break;
                }
                combatController.EquipWeapon(
                    combatController.weaponStyle,
                    combatController.weaponDamage,
                    combatController.weaponSpeed,
                    combatController.rangedStyle);

                EnemyCommon[] enemyCommons = GetComponents<EnemyCommon>();
                for (int i = 0; i < enemyCommons.Length; i++)
                {
                    enemyCommons[i]._movementController = movementController;
                    enemyCommons[i]._combatController = combatController;
                    enemyCommons[i]._audioController = audioController;
                    enemyCommons[i]._animator = animator;
                }
            }
        }
        else if(set == LOD.Proxy)
        {
            if ((LOD == LOD.Proxy || IsInCombat) && !init) //Don't switch if combat already initiated
                return;
            LOD = LOD.Proxy;

            if (GetComponent<ProxyMovementController>())
            {
                Debug.Log("ProxyMovementController already exists");
                return;
            }

            if (!statController._dead)
            {
                {
                    statController.enabled = false;
                    navMeshAgent.obstacleAvoidanceType
                        = ObstacleAvoidanceType.NoObstacleAvoidance; 
                }

                if(targetController)
                    Destroy(targetController);              
                if (combatController)
                    Destroy(combatController);
                if (lineRenderer)
                    Destroy(lineRenderer);
                if (audioController)
                    Destroy(audioController);
                if(audioSource)
                    Destroy(audioSource);             
                if(capsuleCollider)
                    Destroy(capsuleCollider);

                ProxyMovementController proxyMovementController
                    = gameObject.AddComponent<ProxyMovementController>();
                proxyMovementController.roamRadius = 20;
                proxyMovementController.enabled = true;
                if (movementController)
                {
                    proxyMovementController._freeRoamDone = movementController._freeRoamDone;
                    proxyMovementController._waitTimer = movementController._waitTimer;
                    Destroy(movementController);
                }
            }

            //Dead or alive
            {
                if(animator)
                    Destroy(animator);
                //Destroy(sampler);
                GetComponent<Rigidbody>().interpolation
                           = RigidbodyInterpolation.None;
                capsuleVis.SetActive(true);
                if (MrpgcChibit)
                    Destroy(MrpgcChibit);
            }
        }
        Profile.EndProfile("SetLOD");
    }

    /// <summary>
    /// This function is called if the agent dies, removing nearly all of its
    /// components
    /// </summary>
    public void ShutDown()
    {
        Spawner.factionPop[(int)GetComponent<StatController>().team-1]--;

        if (Spawner.Get().spawnMore)
            Spawner.Get().SpawnNPC(-1);

        Destroy(combatController);
        //Destroy(statController);
        Destroy(navMeshAgent);
        if (GetComponent<EnemyMovementController>())
            Destroy(GetComponent<EnemyMovementController>());
        else if (GetComponent<ProxyMovementController>())
            Destroy(GetComponent<ProxyMovementController>());
        Destroy(targetController);   
        Destroy(audioController);
        Destroy(GetComponent<AudioSource>());
        Destroy(lineRenderer);
        Destroy(capsuleCollider);
        Destroy(encounterSphere);
        Destroy(encounterSphereVis);
        //Destroy(sampler);  
    }

    /// <summary>
    /// This function is called when the agent using proxy LOD, to emulate the time
    /// passing. UnFreezeMovement is invoked to reverse these changes when NPC is finished
    /// the mock battle
    /// </summary>
    public void FreezeMovement()
    {
        GetComponent<ProxyMovementController>().enabled = false;
        navMeshAgent.enabled = false;
    }

    /// <summary>
    /// UnfreezeMovement is called by invocation if the agent is predicted to
    /// win the battle. It is also called if the LOD is switched to Full
    /// mid battle
    /// </summary>
    public void UnFreezeMovement()
    {
        GetComponent<ProxyMovementController>().enabled = true;
        navMeshAgent.enabled = true;
    }

    /// <summary>
    /// This function is called if the agent has to cancel its invocations
    /// in order to jump up to full LOD. Interpolation is used to find the correct
    /// state between the current state and the predicted state
    /// </summary>
    public void InterpolatePrediction()
    {
        statController.Health = (int)Mathf.Max(Mathf.Lerp(statController.Health,
                                      statController.Health - prediction.hpLoss,
                                      prediction.T), 1);
    }

    /// <summary>
    /// This function is invoked if the npc's prediction is !winner
    /// </summary>
    public void InvokeDie()
    {
        GetComponent<EnemyStatController>()._alive = false;
        GetComponent<EnemyStatController>()._dead = true;   
    }

    /// <summary>
    /// This function is invoked if the npc's prediction is winner==true
    /// It sets the health to the predicted remaining HP, and it also performs
    /// n level-up operations
    /// </summary>
    public void InvokeLive()
    {
        try {
            currentEncounter.Remove(gameObject);

            statController.Health = Mathf.Max(prediction.remainingHp, 1);
            statController._xp += prediction.xpGain;

            while (statController.Xp >= statController.MaxXp)
            {
                statController._xp -= statController.MaxXp;
                statController.LevelUp();
            }

            UnFreezeMovement();
        }
        catch
        {
            Debug.Log("Exception Caught");
        }
    }

    /// <summary>
    /// SetTeam is called when an npc is spawned
    /// </summary>
    /// <param name="team"></param>
    public void SetTeam(int team)
    {
        statController.team = (Team)team;
    }

    /// <summary>
    /// Controls visualations of targets, 
    /// offline agent capsules and encounter spheres
    /// </summary>
    void Update()
    {
        #region Visualisation
        if (LOD == LOD.Full && !Settings.Get().fullCheapMode)
        {
            if (statController.Alive && targetController)
            {
                if (targetController.Target)
                {         
                    UnityEngine.Debug.DrawRay(transform.position + Vector3.up,
                        targetController.Target.transform.position
                        - transform.position);
                }
            }
        }
        else
        {
            if (Settings.Get().showCapsules)
            {
                if (!capsuleVis.activeSelf)
                    capsuleVis.SetActive(true);
            }
            else
            {
                if (capsuleVis.activeSelf)
                    capsuleVis.SetActive(false);
            }
        }

        if (!statController._dead)
        {
            if (Settings.Get().showEncounterSpheres)
            {
                if (!encounterSphereVis.activeSelf)
                    encounterSphereVis.SetActive(true);
            }
            else
            {
                if (encounterSphereVis.activeSelf)
                    encounterSphereVis.SetActive(false);
            }
        }
        #endregion
    }

    /// <summary>
    ///  Hack to get triggers to work when they're already overlapping (such
    ///  as when they're spawned)
    /// </summary>
    public void ResetTrigger()
    {
        encounterSphere.enabled = false;
        encounterSphere.enabled = true;
    }

    public void ResetEncounterTimer()
    {
        encounterTimer = 0;
    }

    /// <summary>
    /// The NPC.OnTriggerEnter registers the trigger event to the EncounterManager
    /// </summary>
    /// <param name="other"></param>
    void OnTriggerEnter(Collider other)
    {
        if (!Settings.Get().registerEncounters || !other.GetComponent<NPC>())
            return;

        StatController _targetStats = other.GetComponent<StatController>();
        if ((_targetStats.team == Team.None || _targetStats.team != statController.team)
            && (other.GetComponent<NPC>().currentEncounter != currentEncounter
                || other.GetComponent<NPC>().currentEncounter == null
                || currentEncounter == null))
            EncounterManager.Get().RegisterTriggerEvent(gameObject, other.gameObject);        
    }

    /// <summary>
    /// This function obtains the predicted state for the NPC, using the FeatureExtractor
    /// and ModelManager
    /// </summary>
    public void Predict()
    {
        Profile.StartProfile("Predict()");

        if (Settings.Get().useNN)
        {
            ANN_MLP _2_1111 = ModelManager.models["_2_1111_ANN"] as ANN_MLP; //TODO: Cache this?
            _2_1111.Predict(fvectorv2, response);
        }
        else
        {
            RTrees _2_1000 = ModelManager.models["_2_1000_RTrees"] as RTrees;
            response[0, 0] = _2_1000.Predict(fvectorv2, null);
            RTrees _2_0100 = ModelManager.models["_2_0100_RTrees"] as RTrees;
            response[0, 1] = _2_0100.Predict(fvectorv2, null);
            RTrees _2_0010 = ModelManager.models["_2_0010_RTrees"] as RTrees;
            response[0, 2] = _2_0010.Predict(fvectorv2, null);
            RTrees _2_0001 = ModelManager.models["_2_0001_RTrees"] as RTrees;
            response[0, 3] = _2_0001.Predict(fvectorv2, null);
        }

        Profile.EndProfile("Predict()");

        prediction = new Prediction(
            (int)Mathf.Round(response[0, 0]),
            response[0, 1],
            (int)response[0, 2],
            (int)response[0, 3],
            Time.time,
            GetComponent<StatController>().Health);
    }
}
