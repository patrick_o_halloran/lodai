﻿using Emgu.CV;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Xml;
using System.Xml.Serialization;
using UnityEngine;

//TODO
public class Dataset
{
    List<Matrix<float>> samples;
    Matrix<byte> varType;
}

/// <summary>
/// The DatasetManager stores all the datasets that are collected between sessions,
/// They are stored in .xml files
/// </summary>
public class DatasetManager : MonoBehaviour {

    [ShowOnly]
    public List<int> noOfSamples;
    public bool sampling = true;
    public bool shuffleSamples = false;

    //Each dataset is a list of samples
    public static Dictionary<string, List<Matrix<float>>> datasets
        = new Dictionary<string, List<Matrix<float>>>();

    private static DatasetManager Instance = null;
    public static DatasetManager Get()
    {
        if (Instance == null)
            Instance = (DatasetManager)FindObjectOfType(typeof(DatasetManager));
        return Instance;
    }

    void Awake()
    {
        datasets["_1_0100"] = LoadDataset("_1_0100");
        datasets["_1_1000"] = LoadDataset("_1_1000");
        datasets["_1_1100"] = LoadDataset("_1_1100");
        datasets["_1_1111"] = LoadDataset("_1_1111");
        datasets["_2_1111"] = LoadDataset("_2_1111");

        if(shuffleSamples)
            datasets["_2_1111"] = ShuffleSamples(datasets["_2_1111"]);

        SaveDatasets();

        noOfSamples = new List<int>(new int[datasets.Count]);
    }

    void Update()
    {
        int i = 0;
        foreach (KeyValuePair<string, List<Matrix<float>>> dataset in datasets)//Allocates 48 Bytes
            noOfSamples[i++] = dataset.Value.Count;
    }

    public List<Matrix<float>> ShuffleSamples(List<Matrix<float>> samples)
    {
        System.Random rnd = new System.Random();
        return samples.OrderBy(item => rnd.Next()).ToList();
    }

    public void SaveDatasets()
    {
        foreach (KeyValuePair<string, List<Matrix<float>>> dataset in datasets)
            SaveDataset(dataset.Value, dataset.Key);
    }

    public static void SaveDataset(List<Matrix<float>> samples, string filename)
    {
        Matrix<float> sampMat = new Matrix<float>(samples.Count, samples[0].Cols);

        //Copy sample vectors in to a matrix for storing
        for (int i = 0; i < samples.Count; i++)
            samples[i].GetRow(0).CopyTo(sampMat.GetRow(i));

        XmlDocument xDoc = new XmlDocument();
        StringBuilder sb = new StringBuilder();
        (new XmlSerializer(typeof(Matrix<float>))).Serialize(
            new StringWriter(sb), sampMat);
        xDoc.LoadXml(sb.ToString());
        xDoc.Save("Datasets/" + filename + ".xml"); //FIXME: File share violation
    }

    public static List<Matrix<float>> LoadDataset(string filename)
    {
        return AppendDataset("Datasets/" + filename + ".xml", new List<Matrix<float>>());
    }

    public static List<Matrix<float>> AppendDataset(string filename, List<Matrix<float>> samples)
    {
        try
        {
            XmlDocument xDoc = new XmlDocument();

            xDoc.Load(filename);

            //Deserialise samples matrix
            Matrix<float> sampMat = (Matrix<float>)
                (new XmlSerializer(typeof(Matrix<float>)))
                .Deserialize(new XmlNodeReader(xDoc));

            //Parse in to samples list
            for (int i = 0; i < sampMat.Rows/*-15*/; i++)
            {
                Matrix<float> sample = new Matrix<float>(1, sampMat.Cols);

                for (int j = 0; j < sampMat.Cols; j++)
                    sample[0, j] = sampMat[i, j];

                samples.Add(sample);
            }
        }
        catch
        {

        }

        return samples;
    }

    /// <summary>
    /// SubSamples takes a list of row samples and a bitarray specifying which columns
    /// are wanted in the subsamples list which is returned.
    /// </summary>
    /// <param name="samples"></param>
    /// <param name="useFlags"></param>
    /// <returns></returns>
    public static List<Matrix<float>> SubSamples(List<Matrix<float>> samples, BitArray useFlags)
    {
        List<Matrix<float>> subsamples = new List<Matrix<float>>();

        int rmAmt = 0;
        for (int i = 0; i < useFlags.Count; i++)
            if (!useFlags[i])
                rmAmt++;

        foreach (Matrix<float> sample in samples)
            subsamples.Add(SubSample(sample, useFlags, rmAmt));

        return subsamples;
    }

    private static Matrix<float> SubSample(Matrix<float> sample, BitArray useFlags, int rmAmt)
    {
        Matrix<float> subsample = new Matrix<float>(1, useFlags.Count - rmAmt);
        int j = 0;
        for (int i = 0; i < useFlags.Count; i++)
            if (useFlags[i])
                subsample[0, j++] = sample[0, i];
        return subsample;
    }
}
