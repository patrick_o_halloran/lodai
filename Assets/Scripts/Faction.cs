﻿/// <summary>
/// This class is used when computing faction details for the feature vector
/// </summary>
public class Faction {

    public int numberOfAgents;
    public int totalHealth;
    public int totalLevel;

    public int totalCritical = 0;

    public int[] numberOfAgentsByClass = new int[4];

    public float AverageHealth
    {
        get { return totalHealth / numberOfAgents; }
    }

    public float AverageLevel
    {
        get { return totalLevel / numberOfAgents; }
    }

    public float[] Composition
    {
        get
        {
            float[] composition = new float[4];

            for(int i = 0; i < 4; i++)
            {
                composition[i] =
                    (float)numberOfAgentsByClass[i]
                    / numberOfAgents;
            }

            return composition;
        }
    }

    public float Critical
    {
        get
        {
            return (float)totalCritical / numberOfAgents;
        }
    }
}
