﻿using Emgu.CV;
using System;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Attached to the agent
/// </summary>
public class Sampler : MonoBehaviour {

    private float hpAtBeginSample = 0;
    [HideInInspector]
    public int xpGain = 0;

    public void BeginSample()
    {
        xpGain = 0;
        hpAtBeginSample = GetComponent<EnemyStatController>().Health;

        if(Settings.Get().showDebugMessages)
            UnityEngine.Debug.Log(transform.GetInstanceID() + " beginning sample.. combatants.Count: " 
                + GetComponent<NPC>().currentEncounter.combatants.Count);
    }

    public void EndSample(bool win)
    {     
        //Target attributes
        Matrix<float> winTgt = new Matrix<float>(1, 1);
        winTgt[0, 0] = Convert.ToInt32(win);

        Matrix<float> durationTgt = new Matrix<float>(1, 1);
        durationTgt[0, 0] = GetComponent<NPC>().encounterTimer;

        Matrix<float> xpGainTgt = new Matrix<float>(1, 1);
        xpGainTgt[0, 0] = xpGain;

        Matrix<float> hpLossTgt = new Matrix<float>(1, 1);
        hpLossTgt[0, 0] = hpAtBeginSample - GetComponent<EnemyStatController>().Health;

        //position delta?

        DatasetManager.datasets["_2_1111"].Add(
            BuildSample(new List<Matrix<float>> {
                GetComponent<NPC>().fvectorv2,
                winTgt,
                durationTgt,
                xpGainTgt,
                hpLossTgt}));
       
        DatasetManager.Get().SaveDatasets();

        if (Settings.Get().showDebugMessages)
        {
            if (!win)
                UnityEngine.Debug.Log("End Sample (Loss) " + transform.GetInstanceID());
            else
                UnityEngine.Debug.Log("End Sample (Win) " + transform.GetInstanceID());
        }
    }

    public static Matrix<float> BuildSample(List<Matrix<float>> components)
    {
        Matrix<float> sample = components[0];
        for (int i = 1; i < components.Count; i++)
            sample = sample.ConcateHorizontal(components[i]);
        return sample;
    }
}
