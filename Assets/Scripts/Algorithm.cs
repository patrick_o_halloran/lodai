﻿using Emgu.CV;
using Emgu.CV.ML;
using Emgu.CV.ML.MlEnum;
using Emgu.CV.ML.Structure;
using Emgu.CV.Structure;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Text;
using UnityEngine;

/// <summary>
/// This class contains the various statistical model training algorithms available in OpenCV.
/// Each function takes the training and test sets, as well as the varType vector which 
/// specifies if variables in the sample vectors are discrete or continuous
/// Adapted heavily from http://bytefish.de/blog/machine_learning_opencv/
/// </summary>
class Algorithm
{
    public static DTree Decisiontree(Matrix<float> trainingData,
                  Matrix<float> trainingClasses,
                  Matrix<float> testData,
                  Matrix<float> testClasses,
                  Matrix<Byte> varType,
                  string modelName)
    {
        DTree dtree = new DTree();
        AlgorithmSettings settings = AlgorithmSettings.Get();

        MCvDTreeParams treeParams = new MCvDTreeParams();
        treeParams.maxCategories = settings.maxCategories;
        treeParams.maxDepth = 2147483647; //max int 'The optimal value can be obtained using cross validation'
        /*'minimum samples required at a leaf node for it to be split. 
        A reasonable value is a small percentage of the total data e.g. 1%.'*/
        treeParams.minSampleCount = (int)(trainingData.Rows * .01f); //1% of total data        
        treeParams.cvFolds = settings.crossValidationFolds;
        treeParams.use1seRule = settings.use1seRule;
        treeParams.useSurrogates = settings.useSurrogates;
        treeParams.truncatePrunedTree = settings.truncatePrunedTree;
        treeParams.regressionAccuracy = 0.0099999978f;

        Stopwatch stopwatch = Stopwatch.StartNew();
        dtree.Train(trainingData,
                    DATA_LAYOUT_TYPE.ROW_SAMPLE,
                    trainingClasses,
                    null,
                    null,
                    varType,
                    null,
                    treeParams
                    );
        stopwatch.Stop();
        long trainingTime = stopwatch.ElapsedMilliseconds;
        UnityEngine.Debug.Log("TREE\nTrained in: " + trainingTime + "ms");

        double predictionTotalTime = 0;
        Matrix<float> predicted = new Matrix<float>(testClasses.Rows, 1);
        for (int i = 0; i < testData.Rows; i++)
        {
            Matrix<float> sample = testData.GetRow(i);

            stopwatch = Stopwatch.StartNew();
            MCvDTreeNode prediction = dtree.Predict(sample, null, false);
            stopwatch.Stop();
            predictionTotalTime += ((double)stopwatch.ElapsedTicks / Stopwatch.Frequency) * 1000;

            predicted[i, 0] = (float)prediction.value;
        }

        UnityEngine.Debug.Log(ModelTrainer.evaluationStr(predicted, testClasses, varType[trainingData.Cols, 0] != (byte)VAR_TYPE.CATEGORICAL));
        double avgPredictionTime = predictionTotalTime * (1.0 / testData.Rows);
        UnityEngine.Debug.Log("Average prediction time: " + avgPredictionTime);

        if (ModelManager.Get().saveNewModels)
            dtree.Save("Models/" + modelName + "/" + modelName + "_dtree.xml");

        if (ModelManager.Get().writeResultsCSV)
            WriteResults(ref trainingData, ref testData, ref testClasses, ref predicted,
                avgPredictionTime, trainingTime, ref varType, modelName, "DTree");

        return dtree;
    }

    public static RTrees RTrees(Matrix<float> trainingData,
           Matrix<float> trainingClasses,
           Matrix<float> testData,
           Matrix<float> testClasses,
           Matrix<Byte> varType,
           string modelName)
    {
        RTrees rTrees = new RTrees();
        AlgorithmSettings settings = AlgorithmSettings.Get();

        MCvRTParams treeParams = new MCvRTParams();
        treeParams.maxCategories = settings.maxCategories; 
        treeParams.maxDepth = 2147483647; //default: 5
        treeParams.minSampleCount = (int)(trainingData.Rows * .01f); //default: 10
        treeParams.cvFolds = settings.crossValidationFolds; //default: 0
        treeParams.use1seRule = settings.use1seRule; //default: false
        treeParams.useSurrogates = settings.useSurrogates; //default false
        treeParams.truncatePrunedTree = settings.truncatePrunedTree; //default: false
        treeParams.regressionAccuracy = 0.0099999978f; //default: 0
        treeParams.calcVarImportance = true;
        treeParams.nactiveVars = 0; //The number of variables that are randomly selected 
                                    //at each tree node and that are used to find the best split(s).
                                    //If you set it to 0 then the size will be set to the square root of the total number of features. //default: 0
        treeParams.termCrit = new MCvTermCriteria(
            settings.maxNumberOfTreesInForest, //max_num_of_trees_in_the_forest //default 50
            0.00001f //forest_accuracy //default 0.1
            );

        Stopwatch stopwatch = Stopwatch.StartNew();
        rTrees.Train(trainingData,
            DATA_LAYOUT_TYPE.ROW_SAMPLE,
            trainingClasses,
            null,
            null,
            varType,
            null,
            treeParams
            );
        stopwatch.Stop();
        long trainingTime = stopwatch.ElapsedMilliseconds;
        UnityEngine.Debug.Log("RTREE\nTrained in: " + trainingTime + "ms");

        double predictionTotalTime = 0;
        Matrix<float> predicted = new Matrix<float>(testClasses.Rows, 1);
        for (int i = 0; i < testData.Rows; i++)
        {
            Matrix<float> sample = testData.GetRow(i);

            stopwatch = Stopwatch.StartNew();
            predicted[i, 0] = rTrees.Predict(sample, null);
            stopwatch.Stop();
            predictionTotalTime += ((double)stopwatch.ElapsedTicks / Stopwatch.Frequency) * 1000;
        }

        UnityEngine.Debug.Log(ModelTrainer.evaluationStr(predicted, testClasses, varType[trainingData.Cols, 0] != (byte)VAR_TYPE.CATEGORICAL));
        double avgPredictionTime = predictionTotalTime * (1.0 / testData.Rows);
        UnityEngine.Debug.Log("Average prediction time: " + avgPredictionTime);
        UnityEngine.Debug.Log("TreeCount: " + rTrees.TreeCount);
        //for (int i = 0; i < rTrees.VarImportance.Cols; i++)
            //UnityEngine.Debug.Log("VarImportance[0," + i + "]= " + rTrees.VarImportance[0, i]);

        if (ModelManager.Get().saveNewModels)
            rTrees.Save("Models/" + modelName + "/" + modelName + "_RTrees.xml");

        if (ModelManager.Get().writeResultsCSV)
        {
            for (int i = 0; i < rTrees.VarImportance.Cols; i++)
                File.AppendAllText("Results/varImportance/" + modelName + "_varImportance.csv",
                    Convert.ToString(rTrees.VarImportance[0, i]) + Environment.NewLine);

            WriteResults(ref trainingData, ref testData, ref testClasses, ref predicted,
                avgPredictionTime, trainingTime, ref varType, modelName, "RTrees", false);
            File.AppendAllText("Results/" + modelName + "/" +modelName + "_RTrees.csv", 
                "," +Convert.ToString(treeParams.termCrit.max_iter) + Environment.NewLine);
        }

        return rTrees;
    }

    public static ERTrees ExtremelyRandomTrees(Matrix<float> trainingData,
                  Matrix<float> trainingClasses,
                  Matrix<float> testData,
                  Matrix<float> testClasses,
                  Matrix<Byte> varType,
                  string modelName)
    {
        ERTrees extremelyRandomTrees = new ERTrees();

        AlgorithmSettings settings = AlgorithmSettings.Get();
        MCvRTParams treeParams = new MCvRTParams();
        treeParams.maxCategories = settings.maxCategories;
        treeParams.maxDepth = 2147483647;
        treeParams.minSampleCount = (int)(trainingData.Rows * .01f);
        treeParams.cvFolds = settings.crossValidationFolds;
        treeParams.use1seRule = settings.use1seRule;
        treeParams.useSurrogates = settings.useSurrogates;
        treeParams.truncatePrunedTree = settings.truncatePrunedTree;
        treeParams.regressionAccuracy = 0.0099999978f;
        treeParams.calcVarImportance = true;
        treeParams.nactiveVars = 0;

        treeParams.termCrit = new MCvTermCriteria(
            settings.maxNumberOfTreesInForest,
            0.00001f
            );

        Stopwatch stopwatch = Stopwatch.StartNew();
        extremelyRandomTrees.Train(trainingData,
            DATA_LAYOUT_TYPE.ROW_SAMPLE,
            trainingClasses,
            null,
            null,
            varType,
            null,
            treeParams
            );
        stopwatch.Stop();
        long trainingTime = stopwatch.ElapsedMilliseconds;
        UnityEngine.Debug.Log("ERTREE\nTrained in: " + trainingTime + "ms");

        double predictionTotalTime = 0;
        Matrix<float> predicted = new Matrix<float>(testClasses.Rows, 1);
        for (int i = 0; i < testData.Rows; i++)
        {
            Matrix<float> sample = testData.GetRow(i);

            stopwatch = Stopwatch.StartNew();
            predicted[i, 0] = extremelyRandomTrees.Predict(sample, null);
            stopwatch.Stop();
            predictionTotalTime += ((double)stopwatch.ElapsedTicks / Stopwatch.Frequency) * 1000;
        }

        UnityEngine.Debug.Log(ModelTrainer.evaluationStr(predicted, testClasses, varType[trainingData.Cols, 0] != (byte)VAR_TYPE.CATEGORICAL));
        double avgPredictionTime = predictionTotalTime * (1.0 / testData.Rows);
        UnityEngine.Debug.Log("Average prediction time: " + avgPredictionTime);
        UnityEngine.Debug.Log("TreeCount: " + extremelyRandomTrees.TreeCount);
        //for (int i = 0; i < extremelyRandomTrees.VarImportance.Cols; i++)
           // UnityEngine.Debug.Log("VarImportance[0," + i + "]= " + extremelyRandomTrees.VarImportance[0, i]);

        if (ModelManager.Get().saveNewModels)
            extremelyRandomTrees.Save("Models/" + modelName + "/" + modelName + "_erTrees.xml");

        if (ModelManager.Get().writeResultsCSV)
        {
            WriteResults(ref trainingData, ref testData, ref testClasses, ref predicted,
                avgPredictionTime, trainingTime, ref varType, modelName, "ERTrees", false);
            File.AppendAllText("Results/" + modelName + "/" + modelName + "_ERTrees.csv",
                "," + Convert.ToString(settings.maxNumberOfTreesInForest) + Environment.NewLine);
        }

        return extremelyRandomTrees;
    }

    public static GBTrees GradientBoostedTrees(Matrix<float> trainingData,
                 Matrix<float> trainingClasses,
                 Matrix<float> testData,
                 Matrix<float> testClasses,
                 Matrix<Byte> varType,
                 string modelName)
    {
        GBTrees gbTrees = new GBTrees();
        AlgorithmSettings settings = AlgorithmSettings.Get();

        MCvGBTreesParams treeParams = new MCvGBTreesParams();

        treeParams.maxCategories = settings.maxCategories;
        treeParams.maxDepth = 2147483647;
        treeParams.minSampleCount = (int)(trainingData.Rows * .01f);
        treeParams.cvFolds = settings.crossValidationFolds;
        treeParams.use1seRule = settings.use1seRule;
        treeParams.useSurrogates = settings.useSurrogates;
        treeParams.truncatePrunedTree = settings.truncatePrunedTree;
        treeParams.regressionAccuracy = 0.0099999978f;

        //enum { SQUARED_LOSS = 0, ABSOLUTE_LOSS, HUBER_LOSS = 3, DEVIANCE_LOSS };
        //http://docs.opencv.org/2.4/modules/ml/doc/gradient_boosted_trees.html#training-gbt
        treeParams.LossFunctionType = 4; //classification
        //treeParams.WeakCount = 100; setting this causes crash
        treeParams.Shrinkage = 0.8f;
        //treeParams.priors
        //treeParams.SubsamplePortion

        Stopwatch stopwatch = Stopwatch.StartNew();
        gbTrees.Train(trainingData,
            DATA_LAYOUT_TYPE.ROW_SAMPLE,
            trainingClasses,
            null,
            null,
            varType,
            null,
            treeParams,
            false);
        stopwatch.Stop();
        long trainingTime = stopwatch.ElapsedMilliseconds;
        UnityEngine.Debug.Log("GBTREE\nTrained in: " + trainingTime + "ms");

        double predictionTotalTime = 0;
        Matrix<float> predicted = new Matrix<float>(testClasses.Rows, 1);
        for (int i = 0; i < testData.Rows; i++)
        {
            Matrix<float> sample = testData.GetRow(i);

            stopwatch = Stopwatch.StartNew();
            predicted[i, 0] = gbTrees.Predict(sample, null, null, MCvSlice.WholeSeq, false);
            stopwatch.Stop();
            predictionTotalTime += ((double)stopwatch.ElapsedTicks / Stopwatch.Frequency) * 1000;
        }

        UnityEngine.Debug.Log(ModelTrainer.evaluationStr(predicted, testClasses, varType[trainingData.Cols, 0] != (byte)VAR_TYPE.CATEGORICAL));
        double avgPredictionTime = predictionTotalTime * (1.0 / testData.Rows);
        UnityEngine.Debug.Log("Average prediction time: " + avgPredictionTime);

        if (ModelManager.Get().saveNewModels)
            gbTrees.Save("Models/" + modelName + "/" + modelName + "_gbTrees.xml");
        if (ModelManager.Get().writeResultsCSV)
            WriteResults(ref trainingData, ref testData, ref testClasses, ref predicted,
                avgPredictionTime, trainingTime, ref varType, modelName, "GBTrees");

        return gbTrees;
    }

    public static Boost Boost(Matrix<float> trainingData,
       Matrix<float> trainingClasses,
       Matrix<float> testData,
       Matrix<float> testClasses,
       Matrix<Byte> varType,
       string modelName)
    {
        Boost boost = new Boost();
        AlgorithmSettings settings = AlgorithmSettings.Get();

        MCvBoostParams treeParams = new MCvBoostParams();
        treeParams.maxCategories = settings.maxCategories;
        treeParams.maxDepth = 2147483647;
        treeParams.minSampleCount = (int)(trainingData.Rows * .01f);
        treeParams.cvFolds = settings.crossValidationFolds;
        treeParams.use1seRule = settings.use1seRule;
        treeParams.useSurrogates = settings.useSurrogates;
        treeParams.truncatePrunedTree = settings.truncatePrunedTree;
        treeParams.regressionAccuracy = 0.0099999978f;

        if (varType[trainingData.Cols, 0] == (byte)VAR_TYPE.CATEGORICAL)
            treeParams.boostType = BOOST_TYPE.DISCRETE;
        else
            treeParams.boostType = BOOST_TYPE.GENTLE; //Doesn't seem to work?

        treeParams.weakCount = settings.weakCount;
        treeParams.weightTrimRate = settings.weightTrimRate;

        Stopwatch stopwatch = Stopwatch.StartNew();
        boost.Train(trainingData,
            DATA_LAYOUT_TYPE.ROW_SAMPLE,
            trainingClasses,
            null,
            null,
            varType,
            null,
            treeParams,
            false
            );
        stopwatch.Stop();
        long trainingTime = stopwatch.ElapsedMilliseconds;
        UnityEngine.Debug.Log("BOOST\nTrained in: " + trainingTime + "ms");

        double predictionTotalTime = 0;
        Matrix<float> predicted = new Matrix<float>(testClasses.Rows, 1);
        for (int i = 0; i < testData.Rows; i++)
        {
            Matrix<float> sample = testData.GetRow(i);

            stopwatch = Stopwatch.StartNew();
            predicted[i, 0] = boost.Predict(sample, null, null, MCvSlice.WholeSeq, false);
            stopwatch.Stop();
            predictionTotalTime += ((double)stopwatch.ElapsedTicks / Stopwatch.Frequency) * 1000;
        }

        UnityEngine.Debug.Log(ModelTrainer.evaluationStr(predicted, testClasses, false));
        double avgPredictionTime = predictionTotalTime * (1.0 / testData.Rows);
        UnityEngine.Debug.Log("Average prediction time: " + avgPredictionTime);

        if (ModelManager.Get().saveNewModels)
            boost.Save("Models/" + modelName + "/" + modelName + "_Boost.xml");
        if (ModelManager.Get().writeResultsCSV)
            WriteResults(ref trainingData, ref testData, ref testClasses, ref predicted,
                avgPredictionTime, trainingTime, ref varType, modelName, "Boost");

        return boost;
    }

    public static KNearest Knn(Matrix<float> trainingData,
            Matrix<float> trainingClasses,
            Matrix<float> testData,
            Matrix<float> testClasses,
            Matrix<Byte> varType,
            string modelName)
    {
        //http://stackoverflow.com/questions/11568897/value-of-k-in-k-nearest-neighbour-algorithm
        int K = (int)Mathf.Pow(trainingData.Rows, 0.5f);

        KNearest knn =
            new KNearest(trainingData, trainingClasses, null, false, K);

        double predictionTotalTime = 0;
        Matrix<float> predicted = new Matrix<float>(testClasses.Rows, 1);
        for (int i = 0; i < testData.Rows; i++)
        {
            Matrix<float> sample = testData.GetRow(i);

            Stopwatch stopwatch = Stopwatch.StartNew();
            predicted[i, 0] = knn.FindNearest(sample, K,
                null, null, null, null);
            stopwatch.Stop();
            predictionTotalTime += ((double)stopwatch.ElapsedTicks / Stopwatch.Frequency) * 1000;
        }

        UnityEngine.Debug.Log("KNN");
        UnityEngine.Debug.Log(ModelTrainer.evaluationStr(predicted, testClasses, varType[trainingData.Cols, 0] != (byte)VAR_TYPE.CATEGORICAL));
        double avgPredictionTime = predictionTotalTime * (1.0 / testData.Rows);
        UnityEngine.Debug.Log("Average prediction time: " + avgPredictionTime);

        //knn.Save("Models/" + modelName + "/" + modelName + "_KNN.xml"); //FIXME: Can't save KNN?
        if (ModelManager.Get().writeResultsCSV)
            WriteResults(ref trainingData, ref testData, ref testClasses, ref predicted,
                avgPredictionTime, 0, ref varType, modelName, "KNN");

        return knn;
    }

    public static NormalBayesClassifier Bayes(Matrix<float> trainingData,
            Matrix<float> trainingClasses,
            Matrix<float> testData,
            Matrix<float> testClasses,
            string modelName)
    {
        NormalBayesClassifier bayes = new NormalBayesClassifier();

        Matrix<int> trainingClassesInt = new Matrix<int>(trainingClasses.Rows,
            trainingClasses.Cols);

        for (int i = 0; i < trainingClasses.Rows; i++)
            for (int j = 0; j < trainingClasses.Cols; j++)
                trainingClassesInt[i, j] = (int)trainingClasses[i, j];

        Stopwatch stopwatch = Stopwatch.StartNew();
        bayes.Train(trainingData, trainingClassesInt, null, null, /*update=*/false);
        stopwatch.Stop();
        long trainingTime = stopwatch.ElapsedMilliseconds;
        UnityEngine.Debug.Log("BAYES\nTrained in: " + trainingTime + "ms");

        double predictionTotalTime = 0;
        Matrix<float> predicted = new Matrix<float>(testClasses.Rows, 1);
        for (int i = 0; i < testData.Rows; i++)
        {
            Matrix<float> sample = testData.GetRow(i);

            stopwatch = Stopwatch.StartNew();
            predicted[i, 0] = bayes.Predict(sample, null);
            stopwatch.Stop();
            predictionTotalTime += ((double)stopwatch.ElapsedTicks / Stopwatch.Frequency) * 1000;
        }

        UnityEngine.Debug.Log(ModelTrainer.evaluationStr(predicted, testClasses, false));
        double avgPredictionTime = predictionTotalTime * (1.0 / testData.Rows);
        UnityEngine.Debug.Log("Average prediction time: " + avgPredictionTime);

        if (ModelManager.Get().saveNewModels)
            bayes.Save("Models/" + modelName + "/" + modelName + "_Bayes.xml");
        if (ModelManager.Get().writeResultsCSV)
        {
            Matrix<Byte> varType = new Matrix<byte>(trainingData.Cols + 1, 1);
            varType.SetValue((byte)Emgu.CV.ML.MlEnum.VAR_TYPE.CATEGORICAL);
            WriteResults(ref trainingData, ref testData, ref testClasses, ref predicted,
                avgPredictionTime, trainingTime, ref varType, modelName, "Bayes");
        }

        return bayes;
    }

    public static ANN_MLP Mlp(Matrix<float> trainingData,
            Matrix<float> trainingClasses,
            Matrix<float> testData,
            Matrix<float> testClasses,
            Matrix<Byte> varType,
            string modelName)
    {
        AlgorithmSettings settings = AlgorithmSettings.Get();

        int inputs = trainingData.Cols;
        int outputs = trainingClasses.Cols;

        //int[] layers = new int[AlgorithmSettings.Get().hiddenLayers.Count + 2];
        int[] layers = new int[3];
        layers[0] = inputs;
        //for (int i = 1; i < layers.Length - 1; i++)
            //layers[i] = AlgorithmSettings.Get().hiddenLayers[i - 1];
        layers[1] = Mathf.RoundToInt((inputs + outputs) / 2.0f);
        layers[layers.Length - 1] = outputs;
        Matrix<int> layersMat = new Matrix<int>(layers);

        ANN_MLP mlp = new ANN_MLP(layersMat,
            settings.activationFunction,
            settings.alpha,
            settings.beta
            );

        MCvANN_MLP_TrainParams param = new MCvANN_MLP_TrainParams();
        param.term_crit = new MCvTermCriteria(settings.ANNMaxIterations,
            /*eps - how much the error could change between the iterations to make the algorithm continue*/ 0.00001f);
        param.train_method = settings.trainMethod;

        param.bp_dw_scale = settings.dwScale;
        param.bp_moment_scale = settings.momentScale;

        Stopwatch stopwatch = Stopwatch.StartNew();
        mlp.Train(trainingData, trainingClasses, null, null, param,
            Emgu.CV.ML.MlEnum.ANN_MLP_TRAINING_FLAG.DEFAULT);
        stopwatch.Stop();
        long trainingTime = stopwatch.ElapsedMilliseconds;
        UnityEngine.Debug.Log("ANN\nTrained in: " + trainingTime + "ms");

        double predictionTotalTime = 0;
        Matrix<float> predicted = new Matrix<float>(testClasses.Rows, layersMat[layersMat.Rows - 1, 0]/*output layer*/);
        for (int i = 0; i < testData.Rows; i++)
        {
            Matrix<float> response = new Matrix<float>(1, layersMat[layersMat.Rows - 1, 0]/*output layer*/);
            Matrix<float> sample = testData.GetRow(i);

            stopwatch = Stopwatch.StartNew();
            mlp.Predict(sample, response);
            stopwatch.Stop();
            predictionTotalTime += ((double)stopwatch.ElapsedTicks / Stopwatch.Frequency) * 1000;

            for (int j = 0; j < predicted.Cols; j++)
            {
                if (varType[trainingData.Cols + j, 0] == (byte)VAR_TYPE.CATEGORICAL)
                    predicted[i, j] = Mathf.Round(response[0, j]);
                else
                    predicted[i, j] = response[0, j];
            }
        }

        for (int i = 0; i < predicted.Cols; i++)
            UnityEngine.Debug.Log(ModelTrainer.evaluationStr(predicted, testClasses,
                varType[trainingData.Cols + i, 0] != (byte)VAR_TYPE.CATEGORICAL, i));
        double avgPredictionTime = predictionTotalTime * (1.0 / testData.Rows);
        UnityEngine.Debug.Log("Average prediction time: " + avgPredictionTime);

        if (ModelManager.Get().saveNewModels)
            mlp.Save("Models/" + modelName + "/" + modelName + "_ANN.xml");
        if (ModelManager.Get().writeResultsCSV)
        {
            WriteResults(ref trainingData, ref testData, ref testClasses, ref predicted,
                avgPredictionTime, trainingTime, ref varType, modelName, "ANN", false);
            File.AppendAllText("Results/" + modelName + "/" + modelName + "_ANN.csv",
                "," + Convert.ToString(settings.activationFunction == ANN_MLP_ACTIVATION_FUNCTION.GAUSSIAN)
                + "," + Convert.ToString(settings.dwScale)
                + "," + Convert.ToString(settings.momentScale)
                + "," + Convert.ToString(settings.ANNMaxIterations)
                + Environment.NewLine);
        }

        return mlp;
    }

    public static SVM Svm(Matrix<float> trainingData,
            Matrix<float> trainingClasses,
            Matrix<float> testData,
            Matrix<float> testClasses,
            Matrix<byte> varType,
            string modelName)
    {
        SVMParams p = new SVMParams();

        p.KernelType = SVM_KERNEL_TYPE.RBF; //4 types

        if (varType[trainingData.Cols, 0] == (byte)VAR_TYPE.CATEGORICAL)
            p.SVMType = SVM_TYPE.NU_SVC;
        else
            p.SVMType = SVM_TYPE.NU_SVR; //EPS_SVR or NU_SVR for regression

        p.C = 7; //Parameter C of a SVM optimization problem(C_SVC / EPS_SVR / NU_SVR).
        p.Nu = 0.5f; //Parameter nu of a SVM optimization problem (NU_SVC / ONE_CLASS / NU_SVR).
        p.P = 0.0; //Parameter epsilon of a SVM optimization problem (EPS_SVR).
        p.TermCrit = new MCvTermCriteria(1000, 1e-6);
        p.Coef0 = 0; //Parameter coef0 of a kernel function (POLY / SIGMOID).
        p.Gamma = 20; //Parameter gamma of a kernel function(POLY / RBF / SIGMOID).
        p.Degree = 0; //Parameter degree of a kernel function (POLY).
        //p.ClassWeights = null;

        SVM model = new SVM();

        Stopwatch stopwatch = Stopwatch.StartNew();
        model.Train(trainingData, trainingClasses, null, null, p);
        stopwatch.Stop();
        long trainingTime = stopwatch.ElapsedMilliseconds;
        UnityEngine.Debug.Log("SVM\nTrained in: " + trainingTime + "ms");

        double predictionTotalTime = 0;
        Matrix<float> predicted = new Matrix<float>(testClasses.Rows, 1);
        for (int i = 0; i < testData.Rows; i++)
        {
            Matrix<float> sample = testData.GetRow(i);

            stopwatch = Stopwatch.StartNew();
            predicted[i, 0] = model.Predict(sample);
            stopwatch.Stop();
            predictionTotalTime += ((double)stopwatch.ElapsedTicks / Stopwatch.Frequency) * 1000;
        }

        UnityEngine.Debug.Log(ModelTrainer.evaluationStr(predicted, testClasses, varType[trainingData.Cols, 0] != (byte)VAR_TYPE.CATEGORICAL));
        double avgPredictionTime = predictionTotalTime * (1.0 / testData.Rows);
        UnityEngine.Debug.Log("Average prediction time: " + avgPredictionTime);

        if (ModelManager.Get().saveNewModels)
            model.Save("Models/" + modelName + "/" + modelName + "_SVM.xml");
        if (ModelManager.Get().writeResultsCSV)
            WriteResults(ref trainingData, ref testData, ref testClasses, ref predicted,
                avgPredictionTime, trainingTime, ref varType, modelName, "SVM");

        return model;
    }

    public static void WriteResults(
            ref Matrix<float> trainingData,
            ref Matrix<float> testData,
            ref Matrix<float> testClasses,
            ref Matrix<float> predicted,
            double avgPredictionTime,
            long trainingTime,
            ref Matrix<byte> varType,
            string modelName,
            string algorithmName,
            bool newline = true)
    {      
        List<string> rowData = new List<string>();

        rowData.Add(Convert.ToString(trainingData.Rows));
        rowData.Add(Convert.ToString(testData.Rows));
        rowData.Add(Convert.ToString(avgPredictionTime));
        rowData.Add(Convert.ToString(trainingTime));
        for (int i = 0; i < predicted.Cols; i++)
        {
            if (varType[trainingData.Cols + i, 0] != (byte)VAR_TYPE.CATEGORICAL)
            {
                rowData.Add(Convert.ToString(ModelTrainer.evaluateR(predicted, testClasses, i)));
                rowData.Add(Convert.ToString(ModelTrainer.evaluateR(predicted, testClasses, i, true)));
            }
            else
            {
                rowData.Add(Convert.ToString(ModelTrainer.evaluateC(predicted, testClasses, i)));
            }
        }

        string text = string.Join(",", rowData.ToArray());
        if (newline)
            text += Environment.NewLine;

        string filePath = "Results/" + modelName + "/" + modelName + "_" + algorithmName + ".csv";

        if (!Directory.Exists("Results/" + modelName))
            Directory.CreateDirectory("Results/" + modelName);

        if (!File.Exists(filePath))
        {
            string header = "trainingSet,testSet,predictionTime,trainingTime";

            for (int i = 0; i < predicted.Cols; i++)
            {
                if (varType[trainingData.Cols + i, 0] != (byte)VAR_TYPE.CATEGORICAL)
                {
                    header += ",RMSE" + i;
                    header += ",MAE" + i;
                }
                else
                {
                    header += ",accuracy" + i;
                }
            }

            header += Environment.NewLine;

            File.AppendAllText(filePath, header);
        }

        File.AppendAllText(filePath, text);    
    }
}

