﻿using Emgu.CV;
using Emgu.CV.ML;
using Emgu.CV.Structure;
using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class Ensemble
{
    public enum Mode { Majority, Mean, Median }

    public List<StatModel> models
        = new List<StatModel>();

    /// <summary>
    /// The ensemble is a meta-algorithm that uses several models to choose an output.
    /// For classification problems, a majority vote is used.
    /// For regression problems, either the mean or median is used.
    /// </summary>
    /// <param name="fvector"></param>
    /// <param name="mode"></param>
    /// <returns></returns>
    public float Predict(Matrix<float> fvector, Mode mode)
    {
        if (mode == Mode.Majority)
        {
            List<float> votes = new List<float>();

            foreach (StatModel model in models)
            {
                if (model is ANN_MLP)
                {
                    ANN_MLP ann = model as ANN_MLP;
                    Matrix<float> response =
                        new Matrix<float>(1, 1);
                    ann.Predict(fvector, response);
                    votes.Add((int)Mathf.Round(response[0, 0]));
                }
                else if (model is DTree)
                {
                    DTree dtree = model as DTree;
                    votes.Add((float)dtree.Predict(fvector, null, false).value);
                }
                else if (model is NormalBayesClassifier)
                {
                    NormalBayesClassifier bayes = model as NormalBayesClassifier;
                    votes.Add(bayes.Predict(fvector, null));
                }
                else if (model is RTrees)
                {
                    RTrees rTrees = model as RTrees;
                    votes.Add(rTrees.Predict(fvector, null));
                }
                else if (model is Boost)
                {
                    Boost boost = model as Boost;
                    votes.Add(boost.Predict(fvector, null, null, MCvSlice.WholeSeq, false));
                }
            }

            return votes.GroupBy(i => i).OrderByDescending(grp => grp.Count()).Select(grp => grp.Key).First();
        }
        else if (mode == Mode.Mean)
        {
            float total = 0;

            foreach (StatModel model in models)
            {
                if (model is ANN_MLP)
                {
                    ANN_MLP ann = model as ANN_MLP;
                    Matrix<float> response =
                        new Matrix<float>(1, 1);
                    ann.Predict(fvector, response);
                    total += response[0, 0];
                }
                else if (model is DTree)
                {
                    DTree dtree = model as DTree;
                    total += ((float)dtree.Predict(fvector, null, false).value);
                }
                else if (model is RTrees)
                {
                    RTrees rTrees = model as RTrees;
                    total += (rTrees.Predict(fvector, null));
                }
                else if (model is ERTrees)
                {
                    ERTrees erTrees = model as ERTrees;
                    total += (erTrees.Predict(fvector, null));
                }
            }

            total *= 0.25f;
            return total;
        }
        else
        {    
            List<double> results = new List<double>();

            foreach (StatModel model in models)
            {
                if (model is ANN_MLP)
                {
                    ANN_MLP ann = model as ANN_MLP;
                    Matrix<float> response =
                        new Matrix<float>(1, 1);
                    ann.Predict(fvector, response);
                    results.Add(response[0, 0]);
                }
                else if (model is DTree)
                {
                    DTree dtree = model as DTree;
                    results.Add(dtree.Predict(fvector, null, false).value);
                }
                else if (model is RTrees)
                {
                    RTrees rTrees = model as RTrees;
                    results.Add(rTrees.Predict(fvector, null));
                }
                else if (model is ERTrees)
                {
                    ERTrees erTrees = model as ERTrees;
                    results.Add(erTrees.Predict(fvector, null));
                }
            }

            List<double> sortedList = results.OrderBy(d => d).ToList();
            int mid = (int)(sortedList.Count * 0.5f);
            return (float)((sortedList.Count % 2 != 0) ? 
                sortedList[mid] :  (sortedList[mid] + sortedList[mid - 1]) * 0.5f);
        }
    }

    //http://stackoverflow.com/questions/4140719/calculate-median-in-c-sharp
    public static double GetMedian(double[] sourceNumbers)
    {
        //Framework 2.0 version of this method. there is an easier way in F4        
        if (sourceNumbers == null || sourceNumbers.Length == 0)
            throw new System.Exception("Median of empty array not defined.");

        //make sure the list is sorted, but use a new array
        double[] sortedPNumbers = (double[])sourceNumbers.Clone();
        Array.Sort(sortedPNumbers);

        //get the median
        int size = sortedPNumbers.Length;
        int mid = size / 2;
        double median = (size % 2 != 0) ? (double)sortedPNumbers[mid] : ((double)sortedPNumbers[mid] + (double)sortedPNumbers[mid - 1]) / 2;
        return median;
    }
}
