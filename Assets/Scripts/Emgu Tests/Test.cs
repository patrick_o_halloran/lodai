﻿using Emgu.CV;
using Emgu.CV.ML;
using Emgu.CV.ML.MlEnum;
using Emgu.CV.ML.Structure;
using Emgu.CV.Structure;
using System;
using System.Drawing;
using UnityEngine;

//Ported from http://bytefish.de/blog/machine_learning_opencv/
public class Test : MonoBehaviour {

    public int numTrainingPoints = 200;
    public int numTestPoints = 2000;

    public int eq = 1;
    public bool discrete = true;

    public int size = 200;

    double minVal;
    double maxVal;

    // Use this for initialization
    void Start () {

        //make a numTraininPoints x 2 matrix
        Matrix <float> trainingData = 
            new Matrix<float>(numTrainingPoints, 2);

        //make a numTestPoints x 2 matrix
        Matrix<float> testData =
            new Matrix<float>(numTestPoints, 2);

        trainingData.SetRandUniform(new MCvScalar(0), new MCvScalar(1));
        testData.SetRandUniform(new MCvScalar(0), new MCvScalar(1));

        Matrix<float> trainingClasses = labelData(trainingData, eq);
        Matrix<float> testClasses = labelData(testData, eq);

        //Debug.Log(trainingClasses.);

        #pragma warning disable 0168 // variable declared but not used.
        #pragma warning disable 0219 // variable assigned but not used.
        Point minLoc;
        Point maxLoc;
        trainingData.MinMax(out minVal, out maxVal, out minLoc, out maxLoc);
        #pragma warning restore 0168 // variable declared but not used.
        #pragma warning restore 0219 // variable assigned but not used.

        plot(trainingData, trainingClasses, "Train", size);
        plot(testData, testClasses, "Test", size);

        //if(discrete)
            svm(trainingData, trainingClasses, testData, testClasses);
        mlp(trainingData, trainingClasses, testData, testClasses);
        knn(trainingData, trainingClasses, testData, testClasses, 3);
        if(discrete)
            bayes(trainingData, trainingClasses, testData, testClasses);
        decisiontree(trainingData, trainingClasses, testData, testClasses);

        #region HELLO WORLD
        //using (Image<Bgr, Byte> img = new Image<Bgr, byte>(400, 200, new Bgr(255, 0, 0)))
        //{
        //    //Create the font
        //    MCvFont f = new MCvFont(Emgu.CV.CvEnum.FONT.CV_FONT_HERSHEY_COMPLEX, 1.0, 1.0);

        //    //Draw "Hello, world." on the image using the specific font
        //    img.Draw("Hello, world", ref f, new Point(10, 80), new Bgr(0, 255, 0));

        //    //Show the image using ImageViewer from Emgu.CV.UI

        //    CvInvoke.cvShowImage("Test Window", img);
        //}
        #endregion
    }
	
	// Update is called once per frame
	void Update () {
	    
	}

    void decisiontree(Matrix<float> trainingData,
                  Matrix<float> trainingClasses,
                  Matrix<float> testData,
                  Matrix<float> testClasses)
    {
        DTree dtree = new DTree();

        //CvBoostTree
        //Random forest 

        //2 inputs + 1 output
        //cv::Mat var_type(3, 1, CV_8U);
        //// define attributes as numerical
        //var_type.at < unsigned int> (0,0) = CV_VAR_NUMERICAL;
        //var_type.at < unsigned int> (0,1) = CV_VAR_NUMERICAL; //or CV_VAR_CATEGORICAL
        // define output node as numerical
        //var_type.at < unsigned int> (0,2) = CV_VAR_NUMERICAL;

        Matrix<Byte> varType = new Matrix<byte>(3, 1);
        varType.SetValue((byte)Emgu.CV.ML.MlEnum.VAR_TYPE.NUMERICAL); 

        MCvDTreeParams treeParams = new MCvDTreeParams();
        treeParams.maxCategories = 10;
        treeParams.maxDepth = 2147483647; //max int
        treeParams.minSampleCount = 10;
        treeParams.cvFolds = 10;
        treeParams.use1seRule = true;
        treeParams.useSurrogates = true;
        treeParams.truncatePrunedTree = true;
        treeParams.regressionAccuracy = 0.0099999978f;
        //priors?

        dtree.Train(trainingData,
                    DATA_LAYOUT_TYPE.ROW_SAMPLE, //or CV_COL_SAMPLE
                    trainingClasses,
                    null,
                    null,
                    varType,
                    null,
                    treeParams
                    );

        Matrix<float> predicted = new Matrix<float>(testClasses.Rows, 1);

        //For each test data row
        for (int i = 0; i < testData.Rows; i++)
        {
            Matrix<float> sample = testData.GetRow(i);
            MCvDTreeNode prediction = dtree.Predict(sample, null, true); //try false?
            predicted[i, 0] = (float)prediction.value;
        }

        if (discrete)
            Debug.Log(String.Format("Accuracy TREE = {0}", 
                evaluate(predicted, testClasses)));
        else
            Debug.Log(String.Format("Accuracy TREE = {0}",
                evaluateR(predicted, testClasses)));

        //cout << dtree.getVarImportance(); <-- find out how to get this!
        //dtree.Save("save.xml");

        plot(testData, predicted, "Tree", size);
    }

    //A Support Vector Machine may perform much better, if you choose an appropriate 
    //Kernel and optimize the parameters subsequently 
    //(for example with a Grid Search).
    void svm(Matrix<float> trainingData,
            Matrix<float> trainingClasses,
            Matrix<float> testData,
            Matrix<float> testClasses)
    {
        SVMParams p = new SVMParams();
        p.KernelType = Emgu.CV.ML.MlEnum.SVM_KERNEL_TYPE.RBF;
        if (discrete)
            p.SVMType = Emgu.CV.ML.MlEnum.SVM_TYPE.C_SVC;
        else
            p.SVMType = SVM_TYPE.NU_SVR;
        p.C = 7;
        p.Nu = 0.5f;
        p.P = 0.0;
        p.TermCrit = new MCvTermCriteria(1000, 1e-6);
        p.Coef0 = 0;
        p.Gamma = 20;
        p.Degree = 0;

        //p.ClassWeights null

        // SVM training (use train auto for OpenCV>=2.0)
        //CvSVM svm(trainingData, trainingClasses, cv::Mat(), cv::Mat(), param);
        SVM model = new SVM();
        model.Train(trainingData, trainingClasses, 
            null, null, p); //kfold??

        Matrix<float> predicted = new Matrix<float>(testClasses.Rows, 1);

        //For each test data row
        for (int i = 0; i < testData.Rows; i++)
        {
            Matrix<float> sample = testData.GetRow(i);
            predicted[i, 0] = model.Predict(sample);
        }
        
        if (discrete)
            Debug.Log(String.Format("Accuracy SVM = {0}",
                evaluate(predicted, testClasses)));
        else
            Debug.Log(String.Format("Accuracy SVM = {0}",
                evaluateR(predicted, testClasses)));

        plot(testData, predicted, "SVM", size);

        #region PLOT SUPPORT VECTORS
        // plot support vectors
        //if (plotSupportVectors)
        //{
        //    cv::Mat plot_sv(size, size, CV_8UC3);
        //    plot_sv.setTo(cv::Scalar(255.0, 255.0, 255.0));

        //    int svec_count = svm.get_support_vector_count();
        //    for (int vecNum = 0; vecNum < svec_count; vecNum++)
        //    {
        //        const float* vec = svm.get_support_vector(vecNum);
        //        cv::circle(plot_sv, Point(vec[0] * size, vec[1] * size), 3, CV_RGB(0, 0, 0));
        //    }
        //    cv::imshow("Support Vectors", plot_sv);
        //}
        #endregion
    }

    //A Neural Network may perform much better, when choosing the appropriate 
    //number of layers and training iterations. 
    void mlp(Matrix<float> trainingData,
            Matrix<float> trainingClasses,
            Matrix<float> testData,
            Matrix<float> testClasses)
    {

        //Matrix<int> layers = new Matrix<int>(4, 1);

        //layers[0] = cv::Scalar(2);
        //layers.row(1) = cv::Scalar(10); //hidden layer
        //layers.row(2) = cv::Scalar(15); //hidden layer
        //layers.row(3) = cv::Scalar(1);

        Matrix<int> layerSize = new Matrix<int>(new int[] { 2, 10, 15, 1 });

        ANN_MLP mlp = new ANN_MLP(layerSize, 
            Emgu.CV.ML.MlEnum.ANN_MLP_ACTIVATION_FUNCTION.SIGMOID_SYM, 1.0, 1.0);

        MCvANN_MLP_TrainParams param = new MCvANN_MLP_TrainParams();
        param.term_crit = new MCvTermCriteria(100, 0.00001f);
        param.train_method = Emgu.CV.ML.MlEnum.ANN_MLP_TRAIN_METHOD.BACKPROP;
        param.bp_dw_scale = 0.05f;
        param.bp_moment_scale = 0.05f;

        //mlp.create(layers);

        // train
        mlp.Train(trainingData, trainingClasses, null, null, param,
            Emgu.CV.ML.MlEnum.ANN_MLP_TRAINING_FLAG.DEFAULT);

        //Matrix<float> response = new Matrix<float>(1, 1);
        Matrix<float> predicted = new Matrix<float>(testClasses.Rows, 1);

        for (int i = 0; i < testData.Rows; i++)
        {
            Matrix<float> response = new Matrix<float>(1, 1);
            Matrix<float> sample = testData.GetRow(i);

            mlp.Predict(sample, response);
            predicted[i, 0] = response[0, 0];
        }

        if(discrete)
            Debug.Log(String.Format("Accuracy ANN = {0}",
                evaluate(predicted, testClasses)));
        else
            Debug.Log(String.Format("Accuracy ANN = {0}",
                evaluateR(predicted, testClasses)));

        plot(testData, predicted, "Backprop", size);
    }

    void knn(Matrix<float> trainingData,
            Matrix<float> trainingClasses,
            Matrix<float> testData,
            Matrix<float> testClasses, int K)
    {
        KNearest knn = 
            new KNearest(trainingData, trainingClasses, null, !discrete, K);

        Matrix<float> predicted = new Matrix<float>(testClasses.Rows, 1);

        for (int i = 0; i < testData.Rows; i++)
        {
            Matrix<float> sample = testData.GetRow(i);
            predicted[i, 0] = knn.FindNearest(sample, K, null, null, null, null);
        }

        if (discrete)
            Debug.Log(String.Format("Accuracy KNN = {0}",
                evaluate(predicted, testClasses)));
        else
            Debug.Log(String.Format("Accuracy KNN = {0}",
                evaluateR(predicted, testClasses)));

        plot(testData, predicted, "KNN", size);
    }

    void bayes(Matrix<float> trainingData,
            Matrix<float> trainingClasses,
            Matrix<float> testData,
            Matrix<float> testClasses)
    {
        NormalBayesClassifier bayes = new NormalBayesClassifier();

        Matrix<int> trainingClassesInt = new Matrix<int>(trainingClasses.Rows,
            trainingClasses.Cols);

        for (int i = 0; i < trainingClasses.Rows; i++)
            for (int j = 0; j < trainingClasses.Cols; j++)
                trainingClassesInt[i, j] = (int)trainingClasses[i, j];

        bayes.Train(trainingData, trainingClassesInt, null, null, false);

        //CvNormalBayesClassifier bayes(trainingData, trainingClasses);
        Matrix<float> predicted = new Matrix<float>(testClasses.Rows, 1);

        for (int i = 0; i < testData.Rows; i++)
        {
            Matrix<float> sample = testData.GetRow(i);
            predicted[i, 0] = bayes.Predict(sample, null);
        }

        Debug.Log(String.Format("Accuracy Bayes = {0}",
            evaluate(predicted, testClasses)));

        plot(testData, predicted, "Bayes", size);
    }

    void plot(Matrix<float> data, Matrix<float> classes, string name, int size)
    {
        using (Image<Bgr, Byte> img = new Image<Bgr, byte>(size, size,
            new Bgr(255, 255, 255)))
        {
            for (int i = 0; i < data.Rows; i++)
            {
                float x = data[i, 0] * size;
                float y = data[i, 1] * size;

                if (discrete)
                {
                    if (classes[i, 0] > 0)
                        img.Draw(new CircleF(new PointF(x, y), 2), new Bgr(255, 0, 0), 1);
                    else
                        img.Draw(new CircleF(new PointF(x, y), 2), new Bgr(0, 255, 0), 1);
                }
                else
                {
                    float grad =
                        (float)((classes[i, 0] - minVal) / (maxVal - minVal));

                    img.Draw(new CircleF(new PointF(x, y), 2),
                        new Bgr(grad * 255,
                        (1 - grad) * 255,
                        0), 1);
                }
            }

            CvInvoke.cvShowImage(name, img);
        }
    }

    // label data with equation
    Matrix<float> labelData(Matrix<float> samples, int equation)
    {
        Matrix<float> labels = new Matrix<float>(samples.Rows, 1); //outputs

        //for every sample
        for (int i = 0; i < samples.Rows; i++)
        {
            float x = samples[i, 0]; //attribute 1
            float y = samples[i, 1]; //attribute 2

            labels[i,0] = f(x, y, equation); //real function output
        }

        return labels;
    }

    float f(float x, float y, int equation)
    {
        if (discrete)
        {
            switch (equation)
            {
                case 0:
                    return y > Math.Sin(x * 10) ? -1 : 1;
                case 1:
                    return y > Math.Cos(x * 10) ? -1 : 1;
                case 2:
                    return y > 2 * x ? -1 : 1;
                case 3:
                    return y > Math.Tan(x * 10) ? -1 : 1;
                default:
                    return y > Math.Cos(x * 10) ? -1 : 1;
            }
        }
        else
        {
            switch (equation)
            {
                case 0:
                    return (float)(y - Math.Sin(x * 10));
                case 1:
                    return (float)(y - Math.Cos(x * 10)); 
                case 2:
                    return y - 2 * x;
                case 3:
                    return (float)(y - Math.Tan(x * 10)); 
                default:
                    return (float)(y - Math.Cos(x * 10));
            }
        }
    }

    float evaluate(Matrix<float> predicted, Matrix<float> actual)
    {
        //assert(predicted.rows == actual.rows);

        int t = 0;
        int f = 0;

        for (int i = 0; i < actual.Rows; i++)
        {
            float p = predicted[i, 0];
            float a = actual[i, 0];

            if ((p >= 0.0 && a >= 0.0) || (p <= 0.0 && a <= 0.0))
                t++;
            else
                f++;
        }

        return (float)t / (t + f);
    }

    float evaluateR(Matrix<float> predicted, Matrix<float> actual)
    {
        float sum = 0.0f;
        for (int i = 0; i < actual.Rows; i++)
        {
            float difference = actual[i, 0] - predicted[i, 0];
            sum += Mathf.Pow(difference, 2);
        }
        return Mathf.Sqrt(sum / actual.Rows);
    }

}
