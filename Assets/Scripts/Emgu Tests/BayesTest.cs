﻿using UnityEngine;
using System.Collections;

using System.Drawing;
using Emgu.CV.Structure;
using Emgu.CV.ML;
using Emgu.CV.ML.Structure;
using Emgu.CV;
using System;

//http://www.emgu.com/wiki/index.php/Normal_Bayes_Classifier_in_CSharp
public class BayesTest : MonoBehaviour {

	// Use this for initialization
	void Start () {

        Bgr[] colors = new Bgr[] {
           new Bgr(0, 0, 255),
           new Bgr(0, 255, 0),
           new Bgr(255, 0, 0)};
        int trainSampleCount = 150;

        #region Generate the traning data and classes
        Matrix<float> trainData = new Matrix<float>(trainSampleCount, 2);
        Matrix<int> trainClasses = new Matrix<int>(trainSampleCount, 1);

        Image<Bgr, Byte> img = new Image<Bgr, byte>(500, 500);

        Matrix<float> sample = new Matrix<float>(1, 2);

        Matrix<float> trainData1 = trainData.GetRows(0, trainSampleCount / 3, 1);
        trainData1.GetCols(0, 1).SetRandNormal(new MCvScalar(100), new MCvScalar(50));
        trainData1.GetCols(1, 2).SetRandNormal(new MCvScalar(300), new MCvScalar(50));

        Matrix<float> trainData2 = trainData.GetRows(trainSampleCount / 3, 2 * trainSampleCount / 3, 1);
        trainData2.SetRandNormal(new MCvScalar(400), new MCvScalar(50));

        Matrix<float> trainData3 = trainData.GetRows(2 * trainSampleCount / 3, trainSampleCount, 1);
        trainData3.GetCols(0, 1).SetRandNormal(new MCvScalar(300), new MCvScalar(50));
        trainData3.GetCols(1, 2).SetRandNormal(new MCvScalar(100), new MCvScalar(50));

        Matrix<int> trainClasses1 = trainClasses.GetRows(0, trainSampleCount / 3, 1);
        trainClasses1.SetValue(1);
        Matrix<int> trainClasses2 = trainClasses.GetRows(trainSampleCount / 3, 2 * trainSampleCount / 3, 1);
        trainClasses2.SetValue(2);
        Matrix<int> trainClasses3 = trainClasses.GetRows(2 * trainSampleCount / 3, trainSampleCount, 1);
        trainClasses3.SetValue(3);
        #endregion

        using (NormalBayesClassifier classifier = new NormalBayesClassifier())
        {
            classifier.Train(trainData, trainClasses, null, null, false);
            #region Classify every image pixel
            for (int i = 0; i < img.Height; i++)
                for (int j = 0; j < img.Width; j++)
                {
                    sample.Data[0, 0] = i;
                    sample.Data[0, 1] = j;
                    int response = (int)classifier.Predict(sample, null);

                    Bgr color = colors[response - 1];

                    img[j, i] = new Bgr(color.Blue * 0.5, color.Green * 0.5, color.Red * 0.5);
                }
            #endregion
        }

        // display the original training samples
        for (int i = 0; i < (trainSampleCount / 3); i++)
        {
            PointF p1 = new PointF(trainData1[i, 0], trainData1[i, 1]);
            img.Draw(new CircleF(p1, 2.0f), colors[0], -1);
            PointF p2 = new PointF(trainData2[i, 0], trainData2[i, 1]);
            img.Draw(new CircleF(p2, 2.0f), colors[1], -1);
            PointF p3 = new PointF(trainData3[i, 0], trainData3[i, 1]);
            img.Draw(new CircleF(p3, 2.0f), colors[2], -1);
        }

        CvInvoke.cvShowImage("Bayes", img);
        //Emgu.CV.UI.ImageViewer.Show(img);

    }
	
	// Update is called once per frame
	void Update () {
	
	}
}
 
