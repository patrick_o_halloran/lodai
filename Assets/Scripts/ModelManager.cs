﻿using Emgu.CV;
using Emgu.CV.ML;
using Emgu.CV.ML.MlEnum;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using CielaSpike;
using System.Threading;

/// <summary>
/// This class contains a dictionary of models which are loaded from file
/// When starting up, any datasets that are selected in the inspector will
/// be used to train models using the algorithms selected in the inspector
/// </summary>
public class ModelManager : MonoBehaviour {

    public bool multithread = false;
    public bool retestModels = false;

    public float trainingToTestRatio = 0.6f;
    public bool useSpecificSetSizes = false;
    public int trainingSetSize = 2736;
    public int testSetSize = 684;
    public bool testSetFromFront = true;

    public bool saveNewModels = false;
    public bool writeResultsCSV = false;

    [Header("Convergence")]
    public bool testConvergence = false;
    public int minTrainingSet = 100;
    public int maxTrainingSet = 1000;
    public int interval = 100;

    public static Dictionary<string, StatModel> models = 
        new Dictionary<string, StatModel>();
    //public static Ensemble ensemble = new Ensemble();

    //won,duration,xpGain,hpLoss
    [Header("Train")]
    //TODO: select feature vector, target variables in editor
    public bool _1_1000 = false;
    public bool _1_0100 = false;
    public bool _1_1100 = false;
    public bool _1_1111 = false;
    
    public bool _2_1000 = false;
    public bool _2_0100 = false;
    public bool _2_0010 = false;
    public bool _2_0001 = false;
    public bool _2_1100 = false;
    public bool _2_1111 = false;

    public bool _3_1000 = false;
    public bool _3_0100 = false;  
    public bool _3_1111 = false;

    [Header("Algorithms")]
    public bool decisionTree = false;
    public bool extremelyRandomTrees = false;
    public bool gradiantBoostedTrees = false;
    public bool rTrees = false;
    public bool boost = false;
    public bool bayes = false;
    public bool knn = false;
    public bool svm = false;
    public bool mlp = false;
    public bool ensemble = false;

    private static ModelManager Instance = null;
    public static ModelManager Get()
    {
        if (Instance == null)
            Instance = (ModelManager)FindObjectOfType(typeof(ModelManager));
        return Instance;
    }

    void Awake()
    {     
        //UnityEngine.Debug.Log("won | duration | xpGain | hpLoss");

        if(testConvergence)
        {
            trainingSetSize = minTrainingSet;
            useSpecificSetSizes = true;
        }
        else
        {
            maxTrainingSet = trainingSetSize;             
        }

        if (multithread)
        {
            ModelManager.Get();
            DatasetManager.Get();
            AlgorithmSettings.Get();

            this.StartCoroutineAsync(DoTrainingCoroutine());
        }
        else
        {
            DoTraining();
        }

        models["_2_1111_ANN"] = LoadANN("_2_1111");
        models["_2_1000_RTrees"] = LoadRTrees("_2_1000");
        models["_2_0100_RTrees"] = LoadRTrees("_2_0100");
        models["_2_0010_RTrees"] = LoadRTrees("_2_0010");
        models["_2_0001_RTrees"] = LoadRTrees("_2_0001");

        if (retestModels)
        {         
            Matrix<Byte> varType = new Matrix<byte>(24, 1);
            varType[0, 0] = (byte)VAR_TYPE.CATEGORICAL;
            for (int i = 1; i < 20; i++)
                varType[i, 0] = (byte)VAR_TYPE.NUMERICAL;
            varType[20, 0] = (byte)VAR_TYPE.CATEGORICAL;
            varType[21, 0] = (byte)VAR_TYPE.NUMERICAL;
            varType[22, 0] = (byte)VAR_TYPE.NUMERICAL;
            varType[23, 0] = (byte)VAR_TYPE.NUMERICAL;
            TestModel(models["_2_1111_ANN"] as ANN_MLP, DatasetManager.datasets["_2_1111"], 4, varType);

            List<Matrix<float>> subsamples;
            BitArray useFlags = new BitArray(24, true);

            varType = new Matrix<byte>(21, 1);
            varType[0, 0] = (byte)VAR_TYPE.CATEGORICAL;
            for (int i = 1; i < 20; i++)
                varType[i, 0] = (byte)VAR_TYPE.NUMERICAL;
           
            varType[20, 0] = (byte)VAR_TYPE.CATEGORICAL;
            useFlags[20] = true;
            useFlags[21] = false;
            useFlags[22] = false;
            useFlags[23] = false;
            subsamples = DatasetManager.SubSamples(DatasetManager.datasets["_2_1111"], useFlags);
            TestModel(models["_2_1000_RTrees"] as RTrees, subsamples, 1, varType);
            
            //varType[20, 0] = (byte)VAR_TYPE.NUMERICAL;
            //useFlags[20] = false;
            //useFlags[21] = true;
            //useFlags[22] = false;
            //useFlags[23] = false;
            //subsamples = DatasetManager.SubSamples(DatasetManager.datasets["_2_1111"], useFlags);
            //TestModel(models["_2_0100_RTrees"] as RTrees, subsamples, 1000, 1, varType);
            
            //varType[20, 0] = (byte)VAR_TYPE.NUMERICAL;
            //useFlags[20] = false;
            //useFlags[21] = false;
            //useFlags[22] = true;
            //useFlags[23] = false;
            //subsamples = DatasetManager.SubSamples(DatasetManager.datasets["_2_1111"], useFlags);
            //TestModel(models["_2_0010_RTrees"] as RTrees, subsamples, 1000, 1, varType);
           
            //varType[20, 0] = (byte)VAR_TYPE.NUMERICAL;
            //useFlags[20] = false;
            //useFlags[21] = false;
            //useFlags[22] = false;
            //useFlags[23] = true;
            //subsamples = DatasetManager.SubSamples(DatasetManager.datasets["_2_1111"], useFlags);
            //TestModel(models["_2_0001_RTrees"] as RTrees, subsamples, 1000, 1, varType);
        }

        //ensemble.models.Add(LoadANN("_2_1000"));
        //ensemble.models.Add(LoadDTree("_2_1000"));
        //ensemble.models.Add(LoadBayes("_2_1000"));
        //ensemble.models.Add(LoadBoost("_2_1000"));
        //ensemble.models.Add(LoadRTrees("_2_1000"));
    }

    IEnumerator DoTrainingCoroutine()
    {
        DoTraining();
        yield return null;
    }

    void DoTraining()
    {
        string modelName, datasetName;

        while (trainingSetSize <= maxTrainingSet)
        {
            modelName = datasetName = "_1_1000";
            if (_1_1000 && DatasetManager.datasets[datasetName].Count > Constants.minSampleSize)
            {
                UnityEngine.Debug.Log(modelName);

                Matrix<Byte> varType = new Matrix<byte>(10, 1);
                varType[0, 0] = (byte)VAR_TYPE.CATEGORICAL;
                for (int i = 1; i < 9; i++)
                    varType[i, 0] = (byte)VAR_TYPE.NUMERICAL;
                varType[9, 0] = (byte)VAR_TYPE.CATEGORICAL;

                ModelTrainer.TrainModel(DatasetManager.datasets[datasetName],
                                        varType,
                                        modelName,
                                        1);
            }

            modelName = datasetName = "_1_0100";
            if (_1_0100 && DatasetManager.datasets[datasetName].Count >
                Constants.minSampleSize)
            {
                UnityEngine.Debug.Log(modelName);

                Matrix<Byte> varType = new Matrix<byte>(10, 1);
                varType[0, 0] = (byte)VAR_TYPE.CATEGORICAL;
                for (int i = 1; i < 10; i++)
                    varType[i, 0] = (byte)VAR_TYPE.NUMERICAL;

                ModelTrainer.TrainModel(DatasetManager.datasets[datasetName],
                    varType,
                    modelName,
                    1);
            }

            modelName = datasetName = "_1_1100";
            if (_1_1100 && DatasetManager.datasets[datasetName].Count >
                Constants.minSampleSize)
            {
                UnityEngine.Debug.Log(modelName);

                Matrix<Byte> varType = new Matrix<byte>(11, 1);
                varType[0, 0] = (byte)VAR_TYPE.CATEGORICAL;
                for (int i = 1; i < 9; i++)
                    varType[i, 0] = (byte)VAR_TYPE.NUMERICAL;
                varType[9, 0] = (byte)VAR_TYPE.CATEGORICAL;
                varType[10, 0] = (byte)VAR_TYPE.NUMERICAL;

                ModelTrainer.TrainModel(DatasetManager.datasets[datasetName], varType,
                    modelName, 2);
            }

            modelName = datasetName = "_1_1111";
            if (_1_1111 && DatasetManager.datasets[datasetName].Count >
                Constants.minSampleSize)
            {
                UnityEngine.Debug.Log(modelName);

                Matrix<Byte> varType = new Matrix<byte>(13, 1);
                varType[0, 0] = (byte)VAR_TYPE.CATEGORICAL;
                for (int i = 1; i < 9; i++)
                    varType[i, 0] = (byte)VAR_TYPE.NUMERICAL;
                varType[9, 0] = (byte)VAR_TYPE.CATEGORICAL;
                varType[10, 0] = (byte)VAR_TYPE.NUMERICAL;
                varType[11, 0] = (byte)VAR_TYPE.NUMERICAL;
                varType[12, 0] = (byte)VAR_TYPE.NUMERICAL;

                ModelTrainer.TrainModel(DatasetManager.datasets[datasetName], varType,
                    modelName, 4);
            }

            modelName = datasetName = "_2_1111";
            if (_2_1111 && DatasetManager.datasets[datasetName].Count >
                Constants.minSampleSize)
            {
                UnityEngine.Debug.Log(modelName);

                Matrix<Byte> varType = new Matrix<byte>(24, 1);
                varType[0, 0] = (byte)VAR_TYPE.CATEGORICAL;
                for (int i = 1; i < 20; i++)
                    varType[i, 0] = (byte)VAR_TYPE.NUMERICAL;
                varType[20, 0] = (byte)VAR_TYPE.CATEGORICAL;
                varType[21, 0] = (byte)VAR_TYPE.NUMERICAL;
                varType[22, 0] = (byte)VAR_TYPE.NUMERICAL;
                varType[23, 0] = (byte)VAR_TYPE.NUMERICAL;

                ModelTrainer.TrainModel(DatasetManager.datasets[datasetName], varType,
                    modelName, 4);
            }

            modelName = "_2_1100";
            datasetName = "_2_1111";
            if (_2_1100 && DatasetManager.datasets[datasetName].Count >
                Constants.minSampleSize)
            {
                UnityEngine.Debug.Log(modelName);

                Matrix<Byte> varType = new Matrix<byte>(22, 1);
                varType[0, 0] = (byte)VAR_TYPE.CATEGORICAL;
                for (int i = 1; i < 20; i++)
                    varType[i, 0] = (byte)VAR_TYPE.NUMERICAL;
                varType[20, 0] = (byte)VAR_TYPE.CATEGORICAL;
                varType[21, 0] = (byte)VAR_TYPE.NUMERICAL;

                BitArray useFlags = new BitArray(24, true);
                useFlags[22] = false;
                useFlags[23] = false;
                List<Matrix<float>> subsamples
                    = DatasetManager.SubSamples(DatasetManager.datasets[datasetName], useFlags);

                ModelTrainer.TrainModel(subsamples, varType,
                    modelName, 2);
            }

            modelName = "_2_1000";
            datasetName = "_2_1111";
            if (_2_1000 &&
                DatasetManager.datasets[datasetName].Count > Constants.minSampleSize)
            {
                UnityEngine.Debug.Log(modelName);

                Matrix<Byte> varType = new Matrix<byte>(21, 1);
                varType[0, 0] = (byte)VAR_TYPE.CATEGORICAL;
                for (int i = 1; i < 20; i++)
                    varType[i, 0] = (byte)VAR_TYPE.NUMERICAL;
                varType[20, 0] = (byte)VAR_TYPE.CATEGORICAL;

                BitArray useFlags = new BitArray(24, true);
                useFlags[21] = false;
                useFlags[22] = false;
                useFlags[23] = false;
                List<Matrix<float>> subsamples
                    = DatasetManager.SubSamples(DatasetManager.datasets[datasetName], useFlags);

                ModelTrainer.TrainModel(subsamples, varType, modelName, 1);
            }

            modelName = "_2_0100";
            datasetName = "_2_1111";
            if (_2_0100 &&
                DatasetManager.datasets[datasetName].Count > Constants.minSampleSize)
            {
                UnityEngine.Debug.Log(modelName);

                Matrix<Byte> varType = new Matrix<byte>(21, 1);
                varType[0, 0] = (byte)VAR_TYPE.CATEGORICAL;
                for (int i = 1; i < 20; i++)
                    varType[i, 0] = (byte)VAR_TYPE.NUMERICAL;
                varType[20, 0] = (byte)VAR_TYPE.NUMERICAL;

                BitArray useFlags = new BitArray(24, true);
                useFlags[20] = false;
                useFlags[22] = false;
                useFlags[23] = false;
                List<Matrix<float>> subsamples
                    = DatasetManager.SubSamples(DatasetManager.datasets[datasetName], useFlags);

                ModelTrainer.TrainModel(subsamples, varType, modelName, 1);
            }

            modelName = "_3_0100";
            datasetName = "_2_1111";
            if (_3_0100 &&
                DatasetManager.datasets[datasetName].Count > Constants.minSampleSize)
            {
                UnityEngine.Debug.Log(modelName);

                Matrix<Byte> varType = new Matrix<byte>(10, 1);
                varType[0, 0] = (byte)VAR_TYPE.CATEGORICAL;
                for (int i = 1; i < 10; i++)
                    varType[i, 0] = (byte)VAR_TYPE.NUMERICAL;

                BitArray useFlags = new BitArray(24, true);
                useFlags[3] = useFlags[7] = useFlags[8] = useFlags[9] =
                useFlags[10] = useFlags[11] = useFlags[15] = useFlags[16] =
                useFlags[17] = useFlags[18] = useFlags[19] = useFlags[20] =
                useFlags[22] = useFlags[23] = false;

                List<Matrix<float>> subsamples
                    = DatasetManager.SubSamples(DatasetManager.datasets[datasetName], useFlags);

                ModelTrainer.TrainModel(subsamples, varType, modelName, 1);
            }

            modelName = "_3_1000";
            datasetName = "_2_1111";
            if (_3_1000 &&
                DatasetManager.datasets[datasetName].Count > Constants.minSampleSize)
            {
                UnityEngine.Debug.Log(modelName);

                Matrix<Byte> varType = new Matrix<byte>(10, 1);
                varType[0, 0] = (byte)VAR_TYPE.CATEGORICAL;
                for (int i = 1; i < 9; i++)
                    varType[i, 0] = (byte)VAR_TYPE.NUMERICAL;
                varType[9, 0] = (byte)VAR_TYPE.CATEGORICAL;

                BitArray useFlags = new BitArray(24, true);
                useFlags[3] = useFlags[7] = useFlags[8] = useFlags[9] =
                useFlags[10] = useFlags[11] = useFlags[15] = useFlags[16] =
                useFlags[17] = useFlags[18] = useFlags[19] = useFlags[21] =
                useFlags[22] = useFlags[23] = false;

                List<Matrix<float>> subsamples
                    = DatasetManager.SubSamples(DatasetManager.datasets[datasetName], useFlags);

                ModelTrainer.TrainModel(subsamples, varType, modelName, 1);
            }

            modelName = "_3_1111";
            datasetName = "_2_1111";
            if (_3_1111 &&
                DatasetManager.datasets[datasetName].Count > Constants.minSampleSize)
            {
                UnityEngine.Debug.Log(modelName);

                Matrix<Byte> varType = new Matrix<byte>(13, 1);
                varType[0, 0] = (byte)VAR_TYPE.CATEGORICAL;
                for (int i = 1; i < 9; i++)
                    varType[i, 0] = (byte)VAR_TYPE.NUMERICAL;
                varType[9, 0] = (byte)VAR_TYPE.CATEGORICAL;
                varType[10, 0] = (byte)VAR_TYPE.NUMERICAL;
                varType[11, 0] = (byte)VAR_TYPE.NUMERICAL;
                varType[12, 0] = (byte)VAR_TYPE.NUMERICAL;

                BitArray useFlags = new BitArray(24, true);
                useFlags[3] = useFlags[7] = useFlags[8] = useFlags[9] =
                useFlags[10] = useFlags[11] = useFlags[15] = useFlags[16] =
                useFlags[17] = useFlags[18] = useFlags[19] = false;

                List<Matrix<float>> subsamples
                    = DatasetManager.SubSamples(DatasetManager.datasets[datasetName], useFlags);

                ModelTrainer.TrainModel(subsamples, varType, modelName, 4);
            }

            modelName = "_2_0010";
            datasetName = "_2_1111";
            if (_2_0010 &&
                DatasetManager.datasets[datasetName].Count > Constants.minSampleSize)
            {
                UnityEngine.Debug.Log(modelName);

                Matrix<Byte> varType = new Matrix<byte>(21, 1);
                varType[0, 0] = (byte)VAR_TYPE.CATEGORICAL;
                for (int i = 1; i < 20; i++)
                    varType[i, 0] = (byte)VAR_TYPE.NUMERICAL;
                varType[20, 0] = (byte)VAR_TYPE.NUMERICAL;

                BitArray useFlags = new BitArray(24, true);
                useFlags[20] = false;
                useFlags[21] = false;
                useFlags[23] = false;
                List<Matrix<float>> subsamples
                    = DatasetManager.SubSamples(DatasetManager.datasets[datasetName], useFlags);

                ModelTrainer.TrainModel(subsamples, varType, modelName, 1);
            }

            modelName = "_2_0001";
            datasetName = "_2_1111";
            if (_2_0001 &&
                DatasetManager.datasets[datasetName].Count > Constants.minSampleSize)
            {
                UnityEngine.Debug.Log(modelName);

                Matrix<Byte> varType = new Matrix<byte>(21, 1);
                varType[0, 0] = (byte)VAR_TYPE.CATEGORICAL;
                for (int i = 1; i < 20; i++)
                    varType[i, 0] = (byte)VAR_TYPE.NUMERICAL;
                varType[20, 0] = (byte)VAR_TYPE.NUMERICAL;

                BitArray useFlags = new BitArray(24, true);
                useFlags[20] = false;
                useFlags[21] = false;
                useFlags[22] = false;
                List<Matrix<float>> subsamples
                    = DatasetManager.SubSamples(DatasetManager.datasets[datasetName], useFlags);

                ModelTrainer.TrainModel(subsamples, varType, modelName, 1);
            }

            if (!testConvergence)
                break;

            trainingSetSize += interval;
        }

        Debug.Log("FINISHED TRAINING");
    }

    public static ANN_MLP LoadANN(string modelName)
    {
        ANN_MLP model = new ANN_MLP(new Matrix<int>(new int[] { 1,2,1}),
                ANN_MLP_ACTIVATION_FUNCTION.SIGMOID_SYM, 1.0, 1.0);
        try
        {
            model.Load("Models/" + modelName + "/" + modelName + "_ANN.xml");
        }
        catch { }
        return model;
    }

    public static DTree LoadDTree(string modelName)
    {
        DTree model = new DTree();
        try {
            model.Load("Models/" + modelName + "/" + modelName + "_dtree.xml");
        }
        catch { }
        return model;
    }

    public static NormalBayesClassifier LoadBayes(string modelName)
    {
        NormalBayesClassifier model = new NormalBayesClassifier();
        try {
            model.Load("Models/" + modelName + "/" + modelName + "_Bayes.xml");
        }
        catch { }
        return model;
    }

    public static RTrees LoadRTrees(string modelName)
    {
        RTrees model = new RTrees();
        try
        {
            model.Load("Models/" + modelName + "/" + modelName + "_RTrees.xml");
        }
        catch { }
        return model;
    }

    public static ERTrees LoadERTrees(string modelName)
    {
        ERTrees model = new ERTrees();
        try
        {
            model.Load("Models/" + modelName + "/" + modelName + "_ERTrees.xml");
        }
        catch { }
        return model;
    }

    public static Boost LoadBoost(string modelName)
    {
        Boost model = new Boost();
        try
        {
            model.Load("Models/" + modelName + "/" + modelName + "_Boost.xml");
        }
        catch { }
        return model;
    }

    public static void TestModel(StatModel model, List<Matrix<float>> samples, 
        int targetVarAmt, Matrix<Byte> varType)
    {
        Matrix<float> testData, testClasses;
        int featureAmt = samples[0].Cols - targetVarAmt;

        //Test Set
        {
            Matrix<float> testSet =
               new Matrix<float>(Get().testSetSize, featureAmt + targetVarAmt);

            for (int i = 0; i < Get().testSetSize; i++)
                samples[i].GetRow(0).CopyTo(testSet.GetRow(i));

            //Features
            testData = new Matrix<float>(Get().testSetSize, featureAmt);
            for (int i = 0; i < Get().testSetSize; i++)
                for (int j = 0; j < featureAmt; j++)
                    testData[i, j] = testSet[i, j];

            //Classes
            testClasses = new Matrix<float>(Get().testSetSize, targetVarAmt);
            for (int i = 0; i < Get().testSetSize; i++)
                for (int j = 0; j < targetVarAmt; j++)
                    testClasses[i, j] = testSet[i, featureAmt + j];
        }

        Matrix<float> predicted = new Matrix<float>(testClasses.Rows, targetVarAmt);
        for (int i = 0; i < testData.Rows; i++)
        {
            Matrix<float> response = new Matrix<float>(1, targetVarAmt/*output layer*/);
            Matrix<float> sample = testData.GetRow(i);

            if (model is ANN_MLP)
            {
                ANN_MLP mlp = model as ANN_MLP;
                mlp.Predict(sample, response);

                for (int j = 0; j < predicted.Cols; j++)
                {
                    if (varType[featureAmt + j, 0] == (byte)VAR_TYPE.CATEGORICAL)
                        predicted[i, j] = Mathf.Round(response[0, j]);
                    else
                        predicted[i, j] = response[0, j];
                }
            }
            else if (model is RTrees)
            {
                RTrees rTrees = model as RTrees;
                predicted[i, 0] = rTrees.Predict(sample, null);
            }
            else
            {
                return;
            }        
        }

        for (int i = 0; i < predicted.Cols; i++)
            UnityEngine.Debug.Log(ModelTrainer.evaluationStr(predicted, testClasses,
                varType[featureAmt + i, 0] != (byte)VAR_TYPE.CATEGORICAL, i));

    }
}
