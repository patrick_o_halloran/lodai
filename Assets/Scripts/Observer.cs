﻿using UnityEngine;
using System.Collections;

[ExecuteInEditMode]
public class Observer : MonoBehaviour
{
    [Range(1, 1500)]
    public float distanceThreshold = 15.0f;
    SphereCollider LODSphere;
    public float overspill = 2.0f;

    private static Observer Instance = null;
    public static Observer Get()
    {
        if (Instance == null)
            Instance = (Observer)FindObjectOfType(typeof(Observer));
        return Instance;
    }

    void Awake()
    {
        //ObserverManager.observers.Add(this);
    }

    void Start()
    {
        LODSphere =
            transform.FindChild("LODSphere").gameObject.GetComponent<SphereCollider>();
    }

    void Update()
    {
        LODSphere.radius = distanceThreshold;

        if (Input.GetAxis("Mouse ScrollWheel") < 0)
            distanceThreshold += 5;
        else if (Input.GetAxis("Mouse ScrollWheel") > 0)
            distanceThreshold -= 5;

        UpdateLOD();
    }

    public static void UpdateLOD()
    {
        for (int i = 0; i < Spawner.Get().npcs.Count; i++)
        {
            NPC npc = Spawner.Get().npcs[i];

            float d = Vector3.Distance(npc.transform.position,
                Get().transform.position);

            npc.observed = (d <= Get().distanceThreshold);

            if (npc.observed)
            {
                if (npc.LOD == LOD.Proxy)
                    npc.SetLOD(LOD.Full);
            }
            else
            {
                if (npc.LOD == LOD.Full && d > Get().distanceThreshold + Get().overspill)
                    npc.SetLOD(LOD.Proxy);
            }
        }
    }
}
