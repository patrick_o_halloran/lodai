﻿using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// This class is used to configure the algorithms in the inspector
/// </summary>
public class AlgorithmSettings : MonoBehaviour {

    [Header("ANN")]
    //[Tooltip("Protip: Hidden layer neurons mean of the neurons in the input and output layers.")]
    [HideInInspector]
    public List<int> hiddenLayers = new List<int>()
    {
        10,15
    };
    public Emgu.CV.ML.MlEnum.ANN_MLP_ACTIVATION_FUNCTION activationFunction
        = Emgu.CV.ML.MlEnum.ANN_MLP_ACTIVATION_FUNCTION.SIGMOID_SYM;
    public int ANNMaxIterations = 500;
    //[HideInInspector]
    public Emgu.CV.ML.MlEnum.ANN_MLP_TRAIN_METHOD trainMethod
        = Emgu.CV.ML.MlEnum.ANN_MLP_TRAIN_METHOD.BACKPROP;
    [Tooltip("'Free parameter of the activation function, alpha.'")]
    public double alpha = 1.0f;
    [Tooltip("'Free parameter of the activation function, beta.'")]
    public double beta = 1.0f;

    [Tooltip("'Strength of the weight gradient term. The recommended value is about 0.1.'")]
    public double dwScale = 0.05f;
    [Tooltip("'Strength of the momentum term (the difference between"
        +"weights on the 2 previous iterations). This parameter provides some inertia to smooth"
        +"the random fluctuations of the weights. It can vary from 0 (the feature is disabled)"
        +"to 1 and beyond. The value 0.1 or so is good enough'")]
    public double momentScale = 0.05f;

    [Header("Trees")]

    [Tooltip("Only applies to classification, not in regression.")]
    public int maxCategories = 10;
    public int crossValidationFolds = 10;
    [Tooltip("'If true then a pruning will be harsher."
        +"This will make a tree more compact and more resistant to the training data noise but a bit less accurate.'")]
    public bool use1seRule = true;
    [HideInInspector]
    public bool useSurrogates = false;
    [Tooltip("'If true then pruned branches are physically"
        +"removed from the tree. Otherwise they are retained and it is possible to get" 
        +"results from the original unpruned (or pruned less aggressively) tree.'")]
    public bool truncatePrunedTree = true;

    [Header("(RTrees / ExtremeRTrees)")]
    public int maxNumberOfTreesInForest = 1000;   
    [Header("(Boost / GradientBoost)")]
    public int weakCount = 100;
    [Header("(Boost)")]
    public double weightTrimRate = 0.95f;
    
    //[Header("SVM TODO")]

    private static AlgorithmSettings Instance = null;
    public static AlgorithmSettings Get()
    {
        if (Instance == null)
            Instance = (AlgorithmSettings)FindObjectOfType(typeof(AlgorithmSettings));
        return Instance;
    }
}
