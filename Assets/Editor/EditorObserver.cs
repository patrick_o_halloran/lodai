﻿using UnityEngine;
using System.Collections;
using UnityEditor;

/// <summary>
/// This script allows the editor camera to act as an observer
/// </summary>
public class EditorObserver : MonoBehaviour {

    private GameObject editorObserver;

    void Start()
    {     
        editorObserver = GameObject.FindGameObjectWithTag("EditorObserver");
    }

    // Update is called once per frame
    void Update () {
        SceneView view = SceneView.currentDrawingSceneView;
        if (view != null && editorObserver)
            editorObserver.transform.position = view.camera.transform.position;
    }
}
