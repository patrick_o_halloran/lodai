﻿using UnityEngine;
using System.Collections;
using UnityEditor;
using System;

public class CustomGizmos : Editor
{
    /// <summary>
    /// Draws the prediction results etc. in the editor window
    /// https://docs.unity3d.com/ScriptReference/DrawGizmo.html
    /// </summary>
    /// <param name="objectTransform"></param>
    /// <param name="gizmoType"></param>
    [DrawGizmo(GizmoType.NotInSelectionHierarchy |
        GizmoType.InSelectionHierarchy)]
    public static void NPCDetailsGizmo(Transform objectTransform, GizmoType gizmoType)
    {
        if (!objectTransform.GetComponent<NPC>()
            || !EditorApplication.isPlaying)
            return;

        if (Settings.Get().showNPCLabels)
        {
            if (Vector3.Distance(objectTransform.position,
                SceneView.currentDrawingSceneView.camera.transform.position)
                > Settings.Get().showLabelMaxDistance)
                return;

            NPC npc = objectTransform.gameObject.GetComponent<NPC>();

            GUIStyle guiStyle = new GUIStyle(GUI.skin.box);
            guiStyle.normal.textColor = Color.white;

            if (npc.prediction != null)
            {
                string labelStr = "";
                if (npc.prediction.winner == 1)
                    labelStr = "Live";
                else if (npc.prediction.winner == 0)
                    labelStr = "Die";

                Handles.Label(npc.transform.position + Vector3.up * 5,
                labelStr +
                "\n Level " + npc.statController.level
                + "\n Time: " + (int)npc.encounterTimer +
                    " Predict: " + (int)npc.prediction.duration +
                    "\n HP: " + npc.statController.Health +
                    "/" + npc.statController.MaxHealth + " Predict: "
                    + npc.prediction.remainingHp + "/" + npc.statController.MaxHealth,
                guiStyle);
            }
            else
            {
                Handles.Label(npc.transform.position + Vector3.up * 5,
                    "\n Level " + npc.statController.level +
                    "\n HP: " + npc.statController.Health +
                    "/" + npc.statController.MaxHealth,
                guiStyle);
            }
        }
    }    
}
